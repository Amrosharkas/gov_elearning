<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('faq_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('stuff_order')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
        Schema::create('faqs', function(Blueprint $table) {
            $table->increments('id');
            $table->text('question');
            $table->text('answer')->nullable();
            $table->integer('category_id')->nullable();
            $table->boolean('approved')->default(false);
            $table->integer('approved_by')->nullable();
            //Saved - Pending - Published
            $table->enum('status', ['saved', 'published', 'pending', 'modification'])->default('saved');
            $table->integer('stuff_order')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('modification_to')->nullable(); //faqs.id
            $table->boolean('modified')->default(false); //faqs.id
            $table->timestamps();
        });
        Schema::create('user_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->text('question');
            $table->text('answer')->nullable();
            $table->enum('status', ['new', 'reported', 'answered'])->default('new');
            $table->boolean('copied')->default(false); //faqs.id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('faq_categories');
        Schema::drop('faqs');
        Schema::drop('user_questions');
    }

}
