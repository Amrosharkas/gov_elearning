<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Account;
use App\Shift;
use App\Section;
use App\OneCourse;
use App\BlendedCourse;
use App\BlendedTemplate;
use App\BlendedSchedule;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Super Admin
        $user = new User();
        $user->email = 'admin@healthgovernanceunit.com';
        $user->password = bcrypt('governancePass123');
        $user->type = 'superAdmin';
        $user->save();
        $account = new Account();
        $account->name = 'Super Admin';
        $account->user_id = $user->id;
        $account->type = 'superAdmin';
        $account->backend = true;
        $account->save();
        
        
        
        //Courses
        $course = new BlendedCourse;
        $course->title = 'Course 1';
        $course->save();
        
        $course = new BlendedCourse;
        $course->title = 'Course 2';
        $course->save();
        
    }

}
