<?php

return [
    'index' => function () {
        Route::get("/", ['uses' => 'ModuleController@index', 'as' => "index"]);
    },
    'create' => function () {
        Route::get("create", ['uses' => 'ModuleController@create', 'as' => "create"]);
    },
    'store' => function () {
        Route::post("/", ['uses' => 'ModuleController@store', 'as' => "store"]);
    },
    'edit' => function (){
        Route::get("{id}/edit", ['uses' => 'ModuleController@edit', 'as' => "edit"]);
    },
    'reorder' => function (){
        Route::post('reorder', ['uses' => 'ModuleController@reOrder', 'as' => "reorder"]);
    },
    'update' => function (){
        Route::patch("{id}/update", ['uses' => 'ModuleController@update', 'as' => "update"]);
    },
    'delete_multiple' => function (){
        Route::delete("delete_multiple", ['uses' => 'ModuleController@deleteMultiple', 'as' => "delete_multiple"]);

    },
    'delete' => function (){
        Route::delete("{id}/delete", ['uses' => 'ModuleController@delete', 'as' => "delete"]);
    },
    'approve' => function (){
        Route::patch("{id}/approve",['uses'=>'ModuleController@approve', 'as' => 'approve']);
    },
//    'store_publish' => function (){
//        Route::post("publish",['uses'=>'ModuleController@publish','as'=>'store_publish']);
//    },
//    'update_publish' => function (){
//        Route::patch("publish",['uses'=>'ModuleController@publish','as'=>'update_publish']);
//    }
];
        