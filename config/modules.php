<?php

// default actions ['index','create','store','edit','update','delete','delete_multiple']
// default view_dir same as module name
// default repository name : Shift => ShiftRepository, faq_category => FaqCategoryRepository
// default form request name : Shift => ShiftFormRequest, faq_category=> FaqCategoryFormRequest
// All possible keys are : actions, repository, form_request, view_dir, validator, approvable
return [
    'shift' => ['validator' => 'ShiftValidator'],
    'user' => [
        'repository' => 'BackendUsersRepository',
        'validator' => 'BackendUserValidator'
    ],
    'faq_category' => [
        'actions' => ['index', 'edit', 'store', 'update', 'delete', 'delete_multiple', 'create', 'reorder'],
    ],
    'faq' => [
        'approvable' => true,
        'actions' => ['delete', 'delete_multiple', 'approve'],
    ]
];
