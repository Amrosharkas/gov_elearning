<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Form Actions On Top & Bottom </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="javascript:;" class="reload"> </a>
            <a href="javascript:;" class="remove"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        @if($item)
        <form action="{{route('admin.role.update',['id'=>$item->id])}}" method="post" class="form-horizontal ajaxForm">
            {!! method_field('patch') !!}
        @else
        <form action="{{route('admin.role.store')}}" method="post" class="form-horizontal ajaxForm">
        @endif
            {!! csrf_field() !!}
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-4">
                        @if(old('name'))
                        <input type="text" class="form-control" name="name" value="{{old('name')}}">
                        @elseif($item)
                        <input type="text" class="form-control" name="name" value="{{$item->name}}">
                        @else
                        <input type="text" class="form-control" name="name">
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Abilities</label>
                    <div class="col-md-9">
                        <select multiple="multiple" class="multi-select" id="my_multi_select2" name="abilities[]">
                            @foreach($formData->entities as $entity=>$actions)
                            <optgroup label="{{$entity}}">
                                @foreach($actions as $action)
                                @if(old('abilities') && in_array($entity.'.'.$action,old('abilities')))
                                <option selected value="{{$entity}}.{{$action}}">{{$action}}</option>
                                @elseif($item && in_array($entity . '.' . $action,$item->getAbilityNames()))
                                <option selected value="{{$entity}}.{{$action}}">{{$action}}</option>
                                @else
                                <option value="{{$entity}}.{{$action}}">{{$action}}</option>
                                @endif
                                @endforeach
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-lg green">Save</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
