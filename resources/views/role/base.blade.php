@extends('admin.master')
@section('add_css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/ext/multi-select.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/ext/toastr.min.css')}}"/>
<style>
    .error-help-block{
        color: red;
    }
</style>
@stop

@section('add_js_plugins')
<script type="text/javascript" src="{{asset('assets/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/ext/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/ext/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/ext/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/ext/components-multi-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/ext/toastr.min.js')}}"></script>
@stop

@section('add_js_scripts')
<script type="text/javascript" src="{{asset('vendor/jsvalidation.min.js')}}"></script>
{!!JsValidator::formRequest('App\Http\Requests\RoleFormRequest')!!}

@stop

@section('add_inits')

@stop

@section('title')
Roles
@stop

@section('page_title')
Roles
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop
