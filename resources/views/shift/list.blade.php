<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Shifts
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{route('admin.shift.create')}}" id="sample_editable_1_new" class="btn green pjax-link">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                        <button data-form="delete_mul_1" type="button" class="btn red delete_multiple" disabled="disabled" style="float:none;" data-model="shift" ><i class="fa fa-remove"></i> Delete</button>

                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <form id="delete_mul_1" method="post" action="{{route('admin.shift.delete_multiple')}}">
            <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
                <thead>
                    <tr class="tr-head">
                        <th class="no-sorting table-checkbox-col">
                            <input type="checkbox" class="group-checkable">
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            From
                        </th>
                        <th>
                            To
                        </th>
                        <th class="no-sorting">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($shifts as $shift)
                    <tr class="odd gradeX" id="data-row-{{$shift->id}}">
                        <td valign="middle">
                            <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$shift->id}}" >
                        </td>
                        <td valign="middle">
                            {{$shift->name}}
                        </td>
                        <td valign="middle">
                            {{$shift->start_time}}
                        </td>
                        <td valign="middle">
                            {{$shift->end_time}}
                        </td>
                        <td valign="middle">
                            <a href="{{route('admin.shift.edit',['id'=>$shift->id])}}" class="ajax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                            <button type="button" data-action="{{route('admin.shift.delete',['id'=>$shift->id])}}"  class="btn red remove-shift"><i class="fa fa-remove"></i> Delete</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>