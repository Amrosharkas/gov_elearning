<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($shift)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" id="main-form" role="form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-4">
                        <input class="form-control" type="text" value="{{$shift?$shift->name:''}}" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Start time</label>
                    <div class="col-md-4">
                        <input class="form-control timepicker" type="text" value="{{$shift?$shift->start_time:''}}" name="start_time">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">End time</label>
                    <div class="col-md-4">
                        <input class="form-control timepicker" type="text" value="{{$shift?$shift->end_time:''}}" name="end_time">
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all"
                          @if($shift)
                          data-action="{{route('admin.shift.update',['id'=>$shift->id])}}"
                          data-method="patch"
                          @else
                          data-action="{{route('admin.shift.store')}}"
                          data-method="post"
                          @endif
                            >Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
