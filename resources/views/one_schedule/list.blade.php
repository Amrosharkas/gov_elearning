<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>One Schedules
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_schedule" data-action="{{route('admin.one_schedule.init')}}"
                                >
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Course
                    </th>
                    <th valign="middle">
                        Shift
                    </th>
                    <th valign="middle">
                        Teacher
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($schedules as $schedule)
                <tr class="odd gradeX" id="data-row-{{$schedule->id}}">
                    <td valign="middle">
                        @if($schedule->course)
                        {{$schedule->course->title}}
                        @endif
                    </td>
                    <td valign="middle">
                        @if($schedule->shift)
                        {{$schedule->shift->name}}
                        @endif
                    </td>
                    <td valign="middle">
                        @if($schedule->teacher)
                        {{$schedule->teacher->name}}
                        @endif
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.one_schedule.edit',['id'=>$schedule->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="{{route('admin.one_schedule.delete',['id'=>$schedule->id])}}" class="btn red remove-schedule" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>