<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Schedule </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form" id="main-form"> 
            <div class="form-body">
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Frequency </div>
                    <p class="ribbon-content">
                    <div class="form-group">
                        <label>Frequency</label>
                        <select name="frequency" class="form-control val" >
                            <option value="" @if(! $schedule->frequency) selected @endif></option>
                            @foreach(range(1,7) as $i)
                            <option @if($schedule->frequency == $i) selected @endif value="{{$i}}">{{$i}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Start Date</label>
                        <input name="start_date" class="form-control datepicker val" value ="{{$schedule->start_date}}" >
                    </div>

                    <div id="sessions">
                        @foreach($schedule->daytimes as $daytime)
                        <div class="portlet box blue-hoki session">
                            <div class="portlet-title">
                                <div class="caption">
                                    {{$daytime->day}}
                                </div>
                                <div class="actions tools">

                                    <a href="javascript:;" class="expand"> </a>
                                </div>
                            </div>
                            <div class="portlet-body  display-hide">
                                <div class="form-group">
                                    <label>
                                        Day
                                    </label>
                                    <select name="day" class="form-control">
                                        <option @if(! $daytime->day) selected @endif value=""></option>
                                        @foreach($weekdays as $day)
                                        <option @if($daytime->day == $day) selected @endif value="{{$day}}">{{$day}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Time
                                    </label>
                                    <input name="session_time" class="form-control timepicker" value="{{$daytime->session_time}}">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    </p>

                </div>

                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Course details </div>
                    <p class="ribbon-content">
                    <div class="form-group">
                        <label>Course</label>
                        <select name="course" class="form-control val" >
                            <option @if(! $schedule->course) selected @endif></option>
                            @foreach($courses as $course)
                            <option data-vc="{{(int) $course->has_vc}}" 
                                    @if($schedule->course_id == $course->id) selected @endif
                                    value="{{$course->id}}">{{$course->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group {{($schedule->course && $schedule->course->has_vc)?'':'hidden'}}" id="vc_link">
                        <label>Virtual Class Room Link</label>
                        <input class="form-control val" name="vc_link" value="{{$schedule->vc_link}}">
                    </div>
                    <div class="form-group">
                        <label>Shift</label>
                        <select name="shift" class="form-control val" >
                            <option @if(! $schedule->shift) selected @endif></option>
                            @foreach($shifts as $shift)
                            <option value="{{$shift->id}}" 
                                    @if($schedule->shift_id == $shift->id) selected @endif>
                                    {{$shift->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" >
                        <label>Number of sessions</label>
                        <input class="form-control val" name="n_sessions" value="{{$schedule->n_sessions}}">
                    </div>
                    <div class="form-group {{$schedule->n_sessions?'':'hidden'}}" id="n_sessions_price">
                        <label>Price of {{$schedule->n_sessions}} sessions</label>
                        <input class="form-control val" name="n_sessions_price" value="{{$schedule->n_sessions_price}}">
                    </div>
                    </p>

                </div>
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Participants </div>
                    <p class="ribbon-content" >
                    <div class="form-group participants" >
                        <h3>Teacher</h3>
                        <input class="form-control typehead" name="teacher" data-action="{{route('admin.one_schedule.teachers')}}" placeholder="Search by email">
                        <span class="displayname" data-id="{{$schedule->teacher?$schedule->teacher->id:''}}" data-type ="Teacher">
                            @if($schedule->teacher)


                            <div class="mt-element-list">
                                <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                                    <div class="list-head-title-container">
                                        <div class="list-date"></div>

                                    </div>
                                </div>
                                <div class="mt-list-container list-simple ext-1">
                                    <ul>
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <button>&times;</button>
                                            </div>
                                            <div class="list-datetime">  </div>
                                            <div class="list-item-content">
                                                <h3>

                                                    {{$schedule->teacher->name}} ({{$schedule->teacher->user->email}})



                                                </h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </span>
                    </div>
                    <div class="form-group participants">
                        <h3>Student</h3>
                        <input class="form-control typehead" name="student" data-action="{{route('admin.one_schedule.students')}}" placeholder="Search by email">
                        <span class="displayname" data-id="{{$schedule->student?$schedule->student->id:''}}" data-type="Student">
                            @if($schedule->student)
                            <div class="mt-element-list">
                                <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                                    <div class="list-head-title-container">
                                        <div class="list-date"></div>

                                    </div>
                                </div>
                                <div class="mt-list-container list-simple ext-1">
                                    <ul>
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <button>&times;</button>
                                            </div>
                                            <div class="list-datetime">  </div>
                                            <div class="list-item-content">
                                                <h3>

                                                    {{$schedule->student->name}} ({{$schedule->student->user->email}})


                                                </h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif

                        </span>
                    </div>
                    </p>

                </div>



            </div>

            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all" 
                            data-action="{{route('admin.one_schedule.update',['id'=>$schedule->id])}}">
                        Save</button>
                    <button type="button" class="btn btn-lg green" id="publish" 
                            data-action="{{route('admin.one_schedule.publish',['id'=>$schedule->id])}}">
                        Publish</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
<div id="hidden" class="hidden">
    <div class="mt-element-list students">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <div class="list-date"></div>
                <h3 class="list-title"></h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1">
            <ul>
                <li class="mt-list-item done">
                    <div class="list-icon-container">
                        <button>&times;</button>
                    </div>
                    <div class="list-datetime">  </div>
                    <div class="list-item-content">
                        <h3 class="append_name">

                        </h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="portlet box blue-hoki session">
        <div class="portlet-title">
            <div class="caption">
            </div>
            <div class="actions tools">

                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-group">
                <label>
                    Day
                </label>
                <select name="day" class="form-control">
                    <option selected value=""></option>
                    @foreach($weekdays as $day)
                    <option value="{{$day}}">{{$day}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>
                    Time
                </label>
                <input name="session_time" class="form-control timepicker">
            </div>
        </div>
    </div>
</div>