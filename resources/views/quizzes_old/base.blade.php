@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/dragula/dragula.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
<style>
    .bordered{
        margin-bottom: 5px;
        border-style: solid;
        border-width: 3px;
        padding: 30px;
        border-color: black;
    }
    .handle-hidden{
        visibility: hidden;
        height: 0;
        margin:0;
        padding:0;
    }
</style>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@endSection

@section('page_js')
<script>
var pageAttributes = {
    indexUrl: "{{route('admin.one_on_one.index')}}",
}
</script>
<script type="text/javascript" src="{{asset('assets/ajaxForms.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script>
$(document).on('click', '#add_new_course', function () {
    swal({title: "Title", text: "Please enter the title of the Quiz:", type: "input", showCancelButton: true, closeOnConfirm: false, animation: "slide-from-top", showLoaderOnConfirm: true}, function (inputValue) {
        if (inputValue === false)
            return false;
        if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false;
        }
        $.post("{{route('admin.one_on_one.init')}}", {title: inputValue}, function (data) {
            swal.close();
            pjaxPage(data.url);
        })
    });

});
$(document).on('ready pjax:success', function () {
    dragula([$('#levels')[0]]);
//    dragula([$('#objectives')[0]]);
    $('#objectives.dd').nestable();
});
$(document).on('change', 'input[name=objective-content]', function () {
    $(this).closest('.objective').data('content', $(this).val());
});
$(document).on('click', '#add_level', function () {
    $.post($(this).data('url'), function (data) {
        var $level = $('#templates').find('.level').clone();
        $('#levels').append($level);
        $level.find('input[name=id]').val(data.id);
        $level.find('.ig .fileinput').attr('data-action', data.upload_ig_action);
        $level.find('.material .fileinput').attr('data-action', data.upload_material_action);
    });
});
$(document).on('click', '.remove_level', function () {
    var $level = $(this).closest('.level');
    var id = $level.find('input[name=id]').val();
    $.ajax({
        url: "{{route('admin.one_on_one.delete_level')}}",
        method: "delete",
        data: {id: id},
        success: function (data) {
            $level.remove();
            toastr['success']('Level deleted successfully', "Done");
        }
    });
});
$(document).on('click', '#save-all', function () {
    var data = {};
    var url = $('#main-form').attr('action');
    data['section'] = $('#main-form').find('select[name=section]').val();
    data['title'] = $('#main-form').find('input[name=course-title]').val();
    data['max_frequency'] = $('#main-form').find('input[name=max_frequency]').val();
    data['session_duration'] = $('#main-form').find('input[name=session_duration]').val();
    data['max_start_date'] = $('#main-form').find('input[name=max_start_date]').val();
    data['description_overall'] = $('#main-form').find('textarea[name=description_overall]').val();
    data['description_section'] = $('#main-form').find('textarea[name=description_section]').val();
    data['description_course'] = $('#main-form').find('textarea[name=description_course]').val();
    data['has_vc'] = $('#main-form').find('input[name=has_vc]').is(':checked') + 0;
    var shifts = [];
    $('#shifts').find('input[type=checkbox]').each(function () {
        if ($(this).is(':checked')) {
            shifts.push($(this).val());
        }
    });
    data['shifts'] = shifts;
    var levels = [];
    $('#levels').find('.level').each(function () {
        var level = {};
        level['id'] = $(this).find('input[name=id]').val();
        level['title'] = $(this).find('input[name=title]').val();
        levels.push(level);
    });
    data['levels'] = levels;
    data['objectives'] = $('#objectives').nestable('serialize');
//    parseObjectives(objectives);
//    console.log(objectives);
//    return false;
//    $('#objectives').find('.objective').each(function () {
//        var objective = {};
//        objective['id'] = $(this).find('input[name=id]').val();
//        objective['content'] = $(this).find('input[name=objective-content]').val();
//        objectives.push(objective);
//    });
//    data['objectives'] = objectives;
    var pricings = [];
    $('#pricings').find('.pricing').each(function () {
        var pricing = {};
        pricing['id'] = $(this).find('input[name=id]').val();
        pricing['count'] = $(this).find('input[name=count]').val();
        pricing['basic_basic'] = $(this).find('input[name=basic_basic]').val();
        pricing['basic_premium'] = $(this).find('input[name=basic_premium]').val();
        pricing['premium_basic'] = $(this).find('input[name=premium_basic]').val();
        pricing['premium_premium'] = $(this).find('input[name=premium_premium]').val();
        pricings.push(pricing);
    });
    data['pricings'] = pricings;
    $.ajax({
        method: 'patch',
        data: data,
        url: url,
        success: function (data) {
            toastr['success']('Course successfuly saved', "Done");
            pjaxPage(data.url);
        }
    });
});
$(document).on('click', '#add_objective', function () {
//    $.post($(this).data('url'), function (data) {
    var $objective = $('#templates').find('.objective').clone();
    $('#objectives').first().children('ol').append($objective);
//    $objective.data('id', data.id);
//    $objective.find('input[name=id]').val(data.id);
//    });
});
$(document).on('click', '#add_pricing', function () {
    $.post($(this).data('url'), function (data) {
        var $pricing = $('#templates').find('.pricing').clone();
        $('#pricings').append($pricing);
        $pricing.find('input[name=id]').val(data.id);
    });
});
$(document).on('click', '.remove_objective', function () {
    var $objective = $(this).closest('.objective');
//    var id = $objective.find('input[name=id]').val();
//    $.ajax({
//        url: "{{route('admin.one_on_one.delete_objective')}}",
//        method: "delete",
//        data: {id: id},
//        success: function (data) {
    $objective.remove();
//    toastr['success']('Objective deleted successfully', "Done");
//        }
//    });
});
$(document).on('click', '.remove_pricing', function () {
    var $pricing = $(this).closest('.pricing');
    var id = $pricing.find('input[name=id]').val();
    $.ajax({
        url: "{{route('admin.one_on_one.delete_pricing')}}",
        method: "delete",
        data: {id: id},
        success: function (data) {
            $pricing.remove();
            toastr['success']('Session pricing deleted successfully', "Done");
        }
    });
});
$(document).on('click', '.filepicker', function () {
    $(this).closest('.form-group').find('.fileinput').click();
});
$(document).on('change', '.fileinput', function () {
    var $container = $(this).closest('.form-group');
    $container.find('.filepicker').addClass('hidden');
    $container.find('.filenameplaceholder').addClass('hidden');
    var $progressBar = $container.find('.progress-bar');
    $progressBar.removeClass('hidden').find('span').removeClass('hidden');
    $container.find('.cancel').removeClass('hidden');
    var data = new FormData();
    var $file = $(this);
    var url = $file.data('action');
    data.append('file', $file[0].files[0]);
    $.ajax({
        method: 'post',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        beforeSend: function (xhr) {
            $container.find('.cancel').click(function () {
                xhr.abort();
                $container.find('.cancel').addClass('hidden');
                $container.find('.filepicker').removeClass('hidden');
                $container.find('.filenameplaceholder').removeClass('hidden');
                $progressBar.addClass('hidden');
                $progressBar.width(0);
            })
        },
        xhr: function () {
            // get the native XmlHttpRequest object
            var xhr = $.ajaxSettings.xhr();
            // set the onprogress event handler
            xhr.upload.onprogress = function (evt) {
                var percent = Math.ceil(evt.loaded / evt.total * 100);
                $progressBar.width(percent + '%');
                $progressBar.find('span').html(percent + '%');
            };
            // return the customized object
            return xhr;
        },
        success: function (data) {
            $progressBar.width(0);
            $progressBar.addClass('hidden');
            $container.find('.cancel').addClass('hidden');
            link = document.createElement('a');
            link.href = data.download_action;
            link.innerHTML = data.file_name;
            $container.find('.filenameplaceholder ').html(link).removeClass('hidden');
            $container.find('.removefile')
                    .attr('data-action', data.remove_action)
                    .removeClass('hidden');
        }
    });

});
$(document).on('click', '.removefile', function (e) {
    var $container = $(this).closest('.form-group');
    e.preventDefault();
    $.ajax({
        method: 'delete',
        url: $(this).data('action'),
        success: function () {
            $container.find('.filenameplaceholder').html('No file selected');
            $container.find('.removefile').addClass('hidden');
            $container.find('.filepicker').removeClass('hidden');
        }
    });
});
$(document).on('click', '.mt-checkbox', function () {
    $input = $(this).find('input');
    if ($input.is(':checked')) {
        $input.prop('checked', false);
    } else {
        $input.prop('checked', true);
    }
});
$(document).on('click', '.remove-course', function (e) {
    e.preventDefault();
    $row = $(this).closest('tr');
    var action = $(this).attr('href');
    swal({title: "Are you sure?", text: "", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", closeOnConfirm: false}, function () {
        $.ajax({
            url: action,
            method: 'delete',
            success: function () {
                swal("Deleted!", 'Successfuly deleted', "success");
                table.row($row).remove().draw();
                return false;
            },
            error: function () {
                swal("Error", 'Something is wrong, Try again later', "error");
                return false;
            }
        });

    });
});
function parseObjectives(inObjs) {
    inObjs = {};
}
</script>
@endSection

@section('add_inits')

@stop

@section('title')
Quizzes
@stop

@section('page_title')
Quizzes
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

