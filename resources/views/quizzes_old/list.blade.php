<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Quizzes
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_course">
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($quizzes as $quiz)
                <tr class="odd gradeX" id="data-row-{{$quiz->id}}">
                    <td valign="middle">
                        {{$quiz->title}}
                    </td>
                    <td valign="middle">
                        {{$quiz->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.quiz.edit',['id'=>$quiz->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="{{route('admin.quiz.delete',['id'=>$quiz->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>