<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($user)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" role="form" id="main-form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control val" name="name" value="{{$user->name or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control val" name="email" value="{{$user->user->email or ''}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Type</label>
                    <div class="col-md-4">
                        <select class="form-control val" name="type">
                            <option {{$user && $user->type? '':'selected'}} value=""></option>
                            @foreach($types as $type)
                            <option @if($user && $user->type == $type) selected @endif value="{{$type}}">{{$type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green" id="save-all"
                            @if($user)
                            data-action="{{route('admin.backend_user.update',['id'=>$user->id])}}"
                            data-method="patch"
                            @else
                            data-action="{{route('admin.backend_user.store')}}"
                            data-method="post"
                            @endif
                            >Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
