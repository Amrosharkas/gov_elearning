<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Users
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{route('admin.backend_user.create')}}" id="sample_editable_1_new" class="btn green pjax-link">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                        <button data-form="delete_mul_1" type="button" class="btn red delete_multiple" disabled="disabled" style="float:none;" data-model="shift" ><i class="fa fa-remove"></i> Delete</button>

                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <form id="delete_mul_1" method="post" action="{{route('admin.backend_user.delete_multiple')}}">
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
                <thead>
                    <tr class="tr-head">
                        <th class="no-sorting table-checkbox-col">
                            <input type="checkbox" class="group-checkable">
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            Created at
                        </th>
                        <th class="no-sorting">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr class="odd gradeX" id="data-row-{{$user->id}}">
                        <td valign="middle">
                            <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$user->id}}" >
                        </td>
                        <td valign="middle">
                            {{$user->name}}
                        </td>
                        <td valign="middle">
                            {{$user->user->email}}
                        </td>
                        <td valign="middle">
                            {{$user->type}}
                        </td>
                        <td valign="middle">
                            {{$user->created_at}}
                        </td>
                        <td valign="middle">
                            <a href="{{route('admin.backend_user.edit',['id'=>$user->id])}}" class="pjax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                            <button type="button" data-action="{{route('admin.backend_user.delete',['id'=>$user->id])}}"  class="btn red remove-user"><i class="fa fa-remove"></i> Delete</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>