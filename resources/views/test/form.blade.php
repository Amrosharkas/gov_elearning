<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($item)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        @if($item)  				
        {!! Form::model($item,['method'=>'patch','route'=>['admin.test.update','id'=>$item->id],'class'=>'form-horizontal larajsval ajaxForm','novalidate']) !!}
        @else
        {!! Form::open(['method'=>'post','route'=>'admin.test.store','class'=>'form-horizontal larajsval ajaxForm','novalidate']) !!}
        @endif
        {!! csrf_field() !!}
        <div class="form-body">
        </div>
        <div class="form-actions">
            <div class="btn-set pull-right">
                <button type="submit" class="btn btn-lg green">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
        <!-- END FORM-->
    </div>
</div>
