<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>User Questions
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <div class="form-group">
                            <div class="md-radio-inline" id="status-filter">
                            <div class="md-radio">
                                                    <input type="radio" id="radio14" name="status" class="status-fitler" @if($status == 'new') checked @endif value="{{route('admin.user_question.index',['status'=>'new'])}}">
                                                    <label for="radio14">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> New </label>
                                                </div>
                            <div class="md-radio">
                                                    <input type="radio" id="radio142" name="status" class="status-fitler" @if($status == 'answered') checked @endif  value="{{route('admin.user_question.index',['status'=>'answered'])}}">
                                                    <label for="radio142">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Answered </label>
                                                </div>
                            
                                                
                                
                                
                                @if(LoggedAccount::get()->isSuperAdmin())
                                <div class="md-radio">
                                                    <input type="radio" id="radio14xx" name="status" class="status-fitler" @if($status == 'reported') checked @endif  value="{{route('admin.user_question.index',['status'=>'reported'])}}">
                                                    <label for="radio14xx">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Reported </label>
                                                </div>
                               
                                @endif
                            </div>
                            
                            
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th class="no-sorting table-checkbox-col">
                        <input type="checkbox" class="group-checkable">
                    </th>
                    <th valign="middle">
                        Question
                    </th>
                    <th class="no-sorting">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                <tr class="odd gradeX" id="data-row-{{$question->id}}">
                    <td valign="middle">
                        <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$question->id}}" >
                        <input type="hidden" value="{{$question->id}}" class="reorder-vals">
                    </td>
                    <td valign="middle">
                        {{$question->question}}
                    </td>
                    <td valign="middle">
                        @if($status == 'answered')
                        <a href="{{route('admin.user_question.view',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn yellow"><i class="fa fa-edit"></i> View</button></a>
                        @else
                        <a href="{{route('admin.user_question.answer',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> Answer</button></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>