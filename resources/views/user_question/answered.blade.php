<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>User Questions
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th class="no-sorting table-checkbox-col">
                        <input type="checkbox" class="group-checkable">
                    </th>
                    <th valign="middle">
                        Question
                    </th>
                    <th class="no-sorting">
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                <tr class="odd gradeX" id="data-row-{{$question->id}}">
                    <td valign="middle">
                        <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$question->id}}" >
                        <input type="hidden" value="{{$question->id}}" class="reorder-vals">
                    </td>
                    <td valign="middle">
                        {{$question->question}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.user_question.answer',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> View</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>