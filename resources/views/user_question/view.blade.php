<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Question </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" role="form" id="main-form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
                    <div class="col-md-4">
                        <p>{{$question->question}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Answer</label>
                    <div class="col-md-4">
                        <p>{{$question->answer}}</p>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    @if(!$question->copied)
                    <button type="submit" class="btn btn-lg blue" id="copy"
                            data-action="{{route('admin.user_question.copy',['id'=>$question->id])}}"
                            data-method="patch"
                            >Copy to FAQs</button>
                    @endif
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
