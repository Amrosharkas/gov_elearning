<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Question </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" role="form" id="main-form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
                    <div class="col-md-4">
                        <p>{{$question->question}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Answer</label>
                    <div class="col-md-4">
                        <textarea name='answer' class="form-control val" rows="4">{{$question->answer or ''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    @if(LoggedAccount::get()->isCRM())
                    <button type="submit" class="btn btn-lg red" id="report"
                            data-action="{{route('admin.user_question.report',['id'=>$question->id])}}"
                            data-method="patch"
                            >Pass to admin</button>
                    @endif
                    @if(!$question->copied)
                    <button type="submit" class="btn btn-lg blue" id="copy"
                            data-action="{{route('admin.user_question.copy',['id'=>$question->id])}}"
                            data-method="post"
                            >Copy to FAQs</button>
                    @endif
                    <button type="submit" class="btn btn-lg green" id="send"
                            data-action="{{route('admin.user_question.send',['id'=>$question->id])}}"
                            data-method="patch"
                            >Send</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
