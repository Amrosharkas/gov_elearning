<?php
$i = 'Students';
$j = false;
?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
@endSection

@section('page_js')
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script type="application/javascript">
$('#users-table').DataTable({
	processing: true,
	serverSide: true,
	ajax: '/admin/students/getAllStudents',
	columns: [
		{data: 'id', name: 'users.id'},
		{data: 'email', name: 'users.email'},
		{data: 'name', name: 'accounts.name'},
		{data: 'created_at', name: 'users.created_at'},
		{data: 'updated_at', name: 'users.updated_at'}
	]
});

</script>
@endSection

@section('add_inits')

@stop

@section('title')
Backend 
@stop

@section('page_title')
Users
@stop

@section('page_title_small')

@stop

@section('content')
<table class="table table-condensed" id="users-table">
        <thead>
            <tr>
                
              <th>id</th>
                <th>Email</th>
              <th>Name</th>
                <th>Created at</th>
                <th>Updated at</th>
                
            </tr>
        </thead>
    </table>
@stop
