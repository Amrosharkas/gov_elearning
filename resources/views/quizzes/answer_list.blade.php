<div class="dd" id="answers">
	<ol class="dd-list">
    @foreach($answers as $answer)
    <li class="dd-item answer shadow"  data-content="{{$answer['content']}}" data-id = "{{$answer['id']}}" data-correct = "{{$answer['correct']}}">
    
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
 </div>
        <div class="question_body" >
        <a class="btn red remove_answer" ><i class="fa fa-remove"></i> </a>
            <input type="hidden" name="id">
            
            	
                    <div class="form-group">
                        <input type="text" class="form-control" name="objective-content" value="{{$answer['content']}}" placeholder="Type the answer here" />
                        
                    </div>
                
                    
                
            
            <div class="form-group">
                    <h5>Attach File</h5>
                    <input type="file" class="hidden fileinput"
                           data-action="{{route('admin.quizzes.upload_answer_file',['id'=>$answer['id']])}}" 
                           >
                    
                        <div class="pull-left">
                            <span class="filenameplaceholder">
                                @if($answer['file'])
                                <a  href="{{route('admin.quizzes.answer_file',['id'=>$answer['id']])}}" class="btn btn-primary filename">
                                    Download file
                                </a>
                                @else
                                No file selected
                                @endif
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary filepicker pull-right {{$answer['file']?'hidden':''}}">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile {{$answer['file']?'':'hidden'}}" 
                                    data-action="{{route('admin.quizzes.delete_answer_file',['id'=>$answer['id']])}}"
                                    >
                                Detach file
                            </button>
                        </div>
                  
                </div>
                <div class="row"></div>
                <div class="form-group form-md-radios">
                                                    
                                                    <div class="md-radio-list">
                                                        <div class="md-radio">
                                                            <input id="radio{{$answer->id}}" @if($answer->correct == 1) checked @endif name="correct" class="md-radiobtn" type="radio">
                                                            <label for="radio{{$answer->id}}">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> Correct ? </label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
        </div>
        <div class="dd-handle handle-hidden" ></div>
        
    </li>
    @endforeach()
    </ol>
</div>

<button class="btn btn-primary " id="add_answer" data-question_id ="{{$question_id}}" >Add answer</button>