<div id="templates" class="hidden" hidden>
    <li class="dd-item question shadow"  data-content=""  data-id = "" data-weight = "" >
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
 </div>
        <div class="question_body" >
                                    <a href="" class="btn btn-primary answers_links popup" ><i class="fa fa-edit"></i> Answers </a>
                        <a class="btn red remove_question" ><i class="fa fa-remove"></i></a>
            <input type="hidden" name="id">
            <div class="row">
            	<div class="col-md-9">
                    <div class="form-group">
                        <input type="text" class="form-control" name="objective-content" placeholder="Type the Question here" />
                        
        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="question-weight" value="" placeholder="Weight" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                    <label >Attach File</label>
                    <input type="file" class="hidden fileinput"
                           data-action="" 
                           >
                    <div class="row">
                        <div class="col-md-8">
                            <span class="filenameplaceholder">
                                
                                No file selected
                                
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary filepicker pull-right">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile hidden" 
                                    data-action=""
                                    >
                                Remove
                            </button>

                        </div>
                    </div>
                </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
    </li>
    
    
    
    <li class="dd-item answer shadow"  data-content=""  data-id = "" data-correct = "0">
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
 </div>
        <div class="question_body" >
        <a class="btn red remove_answer" ><i class="fa fa-remove"></i></a>
            <input type="hidden" name="id">
            <div class="form-group">
                <input type="text" class="form-control" name="objective-content" placeholder="Type the answer here" />
                
                
            </div>
            
            <div class="form-group">
                    <h5>Attach File</h5>
                    <input type="file" class="hidden fileinput"
                           data-action="" 
                           >
                        <div class="pull-left">
                            <span class="filenameplaceholder">
                                
                                No file selected
                                
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary filepicker pull-right">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile hidden" 
                                    data-action=""
                                    >
                                Remove
                            </button>
                            
                        </div>
                </div>
                <div class="row"></div>
                <div class="form-group form-md-radios">
                                            
                                            <div class="md-radio-list">
                                                <div class="md-radio">
                                                    <input id="" name="correct" class="md-radiobtn" type="radio">
                                                    <label class="rLabel" for="">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Correct ? </label>
                                                </div>
                                                
                                            </div>
                                        </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
    </li>
</div>
