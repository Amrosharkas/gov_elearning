<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($quiz)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Title</label>
                    <input name="quiz-title" class="form-control" value="{{$quiz->title}}">
                </div>
                </p>
                
                                            </div>
                                            <div class="form-group">
                                                  <div class="form-group">
                    <label>Weight</label>
                    <input name="quiz-weight" class="form-control" value="{{$quiz->weight}}">
                </div>
                </p>
                
                                            </div>
                                            </div>
            <div class="mt-element-ribbon bg-grey-steel">
                                            <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Questions </div>
                                                <p class="ribbon-content">
                                                <!-- LEARNING OBJECTIVES -->
                <div class="dd" id="questions">
                    @include('quizzes.questions',$questions)
              </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary" id="add_question" data-url="{{route('admin.quizzes.init_question',['id'=>$quiz->id])}}" data-quiz_id="{{$quiz->id}}"><i class="fa fa-plus"></i> Add </button>
                </div>
                <!-- END LEARNING OBJECTIVES -->
                                                  
                     </p>
                
                                            </div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg green" id="save-all" data-action="{{route('admin.quizzes.update',['id'=>$quiz->id])}}">Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')