<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>FAQ Category</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" id="main-form" role="form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-4">
                        <input name="name" type="text" value="{{$category->name or ''}}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green" id="save"
                            @if($category)
                            data-action="{{route('admin.faq_category.update',['id'=>$category->id])}}"
                            data-method="patch"
                            @else
                            data-action="{{route('admin.faq_category.store')}}"
                            data-method="post"
                            @endif
                            >Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
