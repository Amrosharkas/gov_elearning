<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>FAQ Categories
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="{{route('admin.faq_category.create')}}" id="sample_editable_1_new" class="btn green pjax-link">
                            Add New <i class="fa fa-plus"></i>
                        </a>
                        <button disabled class="btn yellow reorder" data-action="{{route('admin.faq_category.reorder')}}">Re-order</button>
                        <button data-form="delete_mul_1" type="button" class="btn red delete_multiple" disabled="disabled" data-model="shift" ><i class="fa fa-remove"></i> Delete</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <form id="delete_mul_1" method="post" action="{{route('admin.faq_category.delete_multiple')}}">
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
                <thead>
                    <tr class="tr-head">
                        <th valign="middle" class="no-sorting table-checkbox-col">
                            <input type="checkbox" class="group-checkable">
                        </th>
                        <th valign="middle">
                            Name
                        </th>
                        <th class="no-sorting">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr class="odd gradeX" id="data-row-{{$category->id}}">
                        <td valign="middle">
                            <input type="hidden" value="{{$category->id}}" class="reorder-vals">
                            <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$category->id}}" >
                        </td>
                        <td valign="middle">
                            {{$category->name}}
                        </td>
                        <td valign="middle">
                            <a href="{{route('admin.faq_category.edit',['id'=>$category->id])}}" class="pjax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                            <button type="button" data-action="{{route('admin.faq_category.delete',['id'=>$category->id])}}"  class="btn red remove-category"><i class="fa fa-remove"></i> Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
