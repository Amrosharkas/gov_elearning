<div class="portlet box blue-hoki pricing session" data-id="{{$session?$session->id:''}}">
    <div class="portlet-title">
        <div class="caption">
            Sessions </div>
        <div class="actions tools">
            <button class="btn btn-default btn-sm remove_session" data-action="{{$session?route('admin.blended_template.delete_session',['id'=>$session->id]):''}}" type="button"><i class="fa fa-remove"></i> Remove</button>
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div style="padding:20px;" >
            <input type="hidden" value="{{$session ? $session->id: ''}}" name="id">
            <div class="form-group">
                <label>Title</label>
                <input class="form-control" name="title" type="text" value="{{$session ? $session->title : ''}}">
            </div>
            <div class="form-group">
                <label>Duration</label>
                <input class="form-control" name="duration" type="text" value="{{$session ? $session->duration : ''}}">
            </div>
            <!--- CONTENT -->
            <div class="form-group content">
                <label >Content</label>
                <input type="file" class="hidden fileinput"
                       data-action="{{$session? route('admin.blended_template.upload_session_content',['id'=>$session->id]):''}}" 
                       >
                <div class="row">
                    <div class="col-md-8">
                        <span class="filenameplaceholder">
                            @if($session && $session->content())
                            <a href="{{route('admin.blended_template.session_content',['id'=>$session->id])}}" class="filename">
                                {{$session->content()->name}}
                            </a>
                            @else
                            No file selected
                            @endif
                        </span>
                        <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                            <span>0%</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary filepicker pull-right {{$session && $session->content()?'hidden':''}}">
                            Select File
                        </button>
                        <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                        <button class="btn btn-danger pull-right removefile {{$session && $session->content()?'':'hidden'}}" 
                                data-action="{{$session ? route('admin.blended_template.delete_session_content',['id'=>$session->id]):''}}"
                                >
                            Remove
                        </button>
                    </div>
                </div>
            </div>
            <!-- end CONTENT !-->
            <!-- Material -->
            <div class="form-group material">
                <label >Extra Material</label>
                <input type="file" class="hidden fileinput"
                       data-action="{{$session?route('admin.blended_template.upload_session_material',['id'=>$session->id]):''}}" 
                       >
                <div class="row">
                    <div class="col-md-8">
                        <span class="filenameplaceholder">
                            @if($session && $session->material())
                            <a href="{{route('admin.blended_template.session_material',['id'=>$session->id])}}" class="filename">
                                {{$session->material()->name}}
                            </a>
                            @else
                            No file selected
                            @endif
                        </span>
                        <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                            <span>0%</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary filepicker pull-right {{$session && $session->material()?'hidden':''}}">
                            Select File
                        </button>
                        <button class="btn btn-danger hidden pull-right cancel">Cancel</button>
                        <button class="btn btn-danger pull-right removefile {{$session && $session->material()?'':'hidden'}}" 
                                data-action="{{$session ? route('admin.blended_template.delete_session_material',['id'=>$session->id]): ''}}"
                                >
                            Remove
                        </button>
                    </div>
                </div>
            </div>
            <!-- end Material -->
        </div>
    </div>
</div>
