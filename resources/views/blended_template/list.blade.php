<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Blended Templates
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_template" data-action="{{route('admin.blended_template.init')}}"
                                >
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">
                        Course
                    </th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($templates as $template)
                <tr class="odd gradeX" id="data-row-{{$template->id}}">
                    <td valign="middle">
                        {{$template->title}}
                    </td>
                    <td valign="middle">
                        @if($template->course)
                        {{$template->course->title}}
                        @endif
                    </td>
                    <td valign="middle">
                        {{$template->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.blended_template.edit',['id'=>$template->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="{{route('admin.blended_template.delete',['id'=>$template->id])}}" class="btn red remove-template" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>