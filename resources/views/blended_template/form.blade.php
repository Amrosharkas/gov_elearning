<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Template</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate class="form-horizontal  form"> 
            <div class="form-body">
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Template details </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <label>Course</label>
                            <select name="course" class="form-control val">
                                <option value="" @if(! $template->course) selected @endif></option>
                                @foreach($courses as $course)
                                <option @if($template->course && ($template->course->id == $course->id)) selected @endif value="{{$course->id}}">{{$course->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input name="template_title" class="form-control val" value="{{$template->title}}">
                        </div>
                        <!-- IG -->
                        <div class="form-group">
                            <label >IG</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.blended_template.upload_ig',['id'=>$template->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($template->ig())
                                        <a href="{{route('admin.blended_template.ig',['id'=>$template->id])}}" class="filename">
                                            {{$template->ig()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$template->ig()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$template->ig()?'':'hidden'}}" 
                                            data-action="{{route('admin.blended_template.delete_ig',['id'=>$template->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END IG -->
                        <div class="form-group">
                            <label>Number of H.Ws</label>
                            <input name="homework_count" class="form-control val" value="{{$template->homework_count}}">
                        </div>                
                    </div>
                </div>
                <!-- Sessions -->
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Sessions </div>
                    <div class="ribbon-content">
                        <div id="sessions">
                            @foreach($sessions as $session)
                            @include('blended_template.session',['session'=>$session])
                            @endforeach
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="add_session" data-url="{{route('admin.blended_template.init_session',['id'=>$template->id])}}">Add a session</button>
                        </div>                
                    </div>

                </div>

                <!-- END Sessions -->
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all" 
                            data-action="{{route('admin.blended_template.update',['id'=>$template->id])}}">
                        Save</button>
                    <button type="button" class="btn btn-lg green" id="publish" 
                            data-action="{{route('admin.blended_template.publish',['id'=>$template->id])}}">
                        Publish</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
<div id="hidden" class="hidden">
    @include('blended_template.session',['session'=>false])
</div>