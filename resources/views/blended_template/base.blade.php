<?php $i = 'blended'; $j='templates';?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/dragula/dragula.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
<style>
    .bordered{
        margin-bottom: 5px;
        border-style: solid;
        border-width: 3px;
        padding: 30px;
        border-color: black;
    }
    .handle-hidden{
        visibility: hidden;
        height: 0;
        margin:0;
        padding:0;
    }
</style>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@endSection

@section('page_js')
<script>
var pageAttributes = {
    indexUrl: "{{route('admin.one_on_one.index')}}",
}
</script>
<script type="text/javascript" src="{{asset('assets/ajaxForms.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/blended_template.js')}}"></script>
@endSection

@section('add_inits')

@stop

@section('title')
Blended Template
@stop

@section('page_title')
Blended Template
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

