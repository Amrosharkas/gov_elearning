<div class="session bordered" data-id="{{$session?$session->id:''}}">
    <div class="title">Title: {{$session?$session->title:''}}</div>
    <div class="duration">Duration: {{$session?$session->duration:''}}</div>
    <div class="form-group">
        <label>Start time</label>
        <input class="form-control datetimepicker" name="start_time" 
               value="{{$session && $session->pivot?$session->pivot->start_time:''}}" placeholder="Date and time"> 
    </div>
</div>

