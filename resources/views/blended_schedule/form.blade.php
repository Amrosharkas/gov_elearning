<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Schedule </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form" id="main-form"> 
            <div class="form-body">
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <div class="form-group">
                                <label>Course</label>
                                <select name="course" class="form-control val"
                                        data-action = "{{route('admin.blended_schedule.templates',['id'=>$schedule->id])}}"
                                        >
                                    <option @if(! $schedule->course) selected @endif value=""></option>
                                    @foreach($courses as $course)
                                    <option @if($schedule->course && ($schedule->course->id == $course->id)) selected @endif value="{{$course->id}}">{{$course->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Template</label>
                                <select name="template" class="form-control val" {{$schedule->course ? '' : 'disabled'}}
                                    data-action = "{{route('admin.blended_schedule.sessions',['id'=>$schedule->id])}}"
                                    >
                                    <option @if(! $schedule->template) selected @endif value=""></option>
                                    @if($schedule->course)
                                    @foreach($schedule->course->templates as $template)
                                    <option @if($schedule->template && ($schedule->template->id == $template->id)) selected @endif value="{{$template->id}}">{{$template->title}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Shift</label>
                                <select name="shift" class="form-control val">
                                    <option @if(! $schedule->shift) selected @endif value=""></option>
                                    @foreach($shifts as $shift)
                                    <option @if($schedule->shift && ($schedule->shift->id == $shift->id)) selected @endif value="{{$shift->id}}">{{$shift->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Virtual Classroom </label>
                                <input name="vc_link" class="form-control val" value="{{$schedule->vc_link}}">
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input name="price" class="form-control val" value="{{$schedule->price}}">
                            </div>

                            <div class="form-group">
                                <label>Teacher Payment</label>
                                <input name="teacher_payment" class="form-control val" value="{{$schedule->teacher_payment}}">
                            </div>

                            <div class="form-group">
                                <label class="mt-checkbox">
                                    <input type="checkbox" @if($schedule->closed_group) checked  @endif name="closed_group" value="{{$schedule->closed_group}}"> Closed Group 
                                           <span></span>
                                </label>
                            </div>                
                        </div>

                    </div>
                </div>



                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Participants </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <h3>Students</h3>

                            <div class="form-group">
                                <input type="search" name="q" id="add_student" class="form-control" placeholder="Search by email" autocomplete="off">
                                <span class="displayname" data-type="Students"> 
                                    <div id="students"
                                         data-delete="{{route('admin.blended_schedule.delete_student',['id'=>$schedule->id])}}"
                                         data-students="{{route('admin.blended_schedule.students',['id'=>$schedule->id])}}"
                                         data-add = "{{route('admin.blended_schedule.add_student',['id'=>$schedule->id])}}"
                                         >
                                        @foreach($schedule->students as $student)
                                        <div class="mt-element-list student"  data-id="{{$student->id}}">
                                            <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                                                <div class="list-head-title-container">
                                                    <div class="list-date"></div>

                                                </div>
                                            </div>
                                            <div class="mt-list-container list-simple ext-1">
                                                <ul>
                                                    <li class="mt-list-item done">
                                                        <div class="list-icon-container">
                                                            <button class="remove-student">&times;</button>
                                                        </div>
                                                        <div class="list-datetime">  </div>
                                                        <div class="list-item-content">
                                                            <h3>

                                                                {{$student->user->email}}({{$student->name}}) 



                                                            </h3>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </span>
                            </div>  

                            <h3>Teacher</h3>

                            <div class="form-group" id="teacher">
                                <input class="form-control typehead" name="teacher" data-action="{{route('admin.one_schedule.teachers')}}" placeholder="Search by email">
                                <span class="displayname" data-id="{{$schedule->teacher?$schedule->teacher->id:''}}">

                                    @if($schedule->teacher)
                                    <div class="mt-element-list teacher" data-id="{{$schedule->teacher->id}}">
                                        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
                                            <div class="list-head-title-container">
                                                <div class="list-date"></div>
                                                <h3 class="list-title"></h3>
                                            </div>
                                        </div>
                                        <div class="mt-list-container list-simple ext-1">
                                            <ul>
                                                <li class="mt-list-item done">
                                                    <div class="list-icon-container">
                                                        <button class="remove-teacher">&times;</button>
                                                    </div>
                                                    <div class="list-datetime">  </div>
                                                    <div class="list-item-content">
                                                        <h3 class="append_name">{{$schedule->teacher->name}} ({{$schedule->teacher->user->email}}) </h3>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    @endif
                                </span>
                            </div>             </div>

                    </div>
                </div>

                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Sessions </div>
                    <div class="ribbon-content"><div class="form-group">
                            <h3>Sessions</h3>
                            <div id="sessions">
                                @if($schedule->template)
                                @foreach($schedule->sessions as $session)
                                @include('blended_schedule.hidden.session',['session'=>$session])
                                @endforeach
                                @endif

                            </div>
                            <div class="form-group">
                                <label>Locked By</label>
                                <input name="locked_by" class="form-control datepicker val" value="{{$schedule->locked_by}}">
                            </div>
                            <div class="form-group">
                                <label>Hidden By</label>
                                <input name="hidden_by" class="form-control datepicker val" value="{{$schedule->hidden_by}}">
                            </div>
                            <div class="form-group">
                                <label>Archived By</label>
                                <input name="archived_by" class="form-control datepicker val" value="{{$schedule->archived_by}}">
                            </div>                
                        </div>

                    </div>
                </div>

            </div>

            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all" 
                            data-action="{{route('admin.blended_schedule.update',['id'=>$schedule->id])}}">
                        Save</button>
                    <button type="button" class="btn btn-lg green" id="publish" 
                            data-action="{{route('admin.blended_schedule.publish',['id'=>$schedule->id])}}">
                        Publish</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
<div id="hidden" class="hidden">
    @include('blended_schedule.hidden.session',['session'=>false])
    <div class="mt-element-list students student" data-id = "">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <div class="list-date"></div>
                <h3 class="list-title"></h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1">
            <ul>
                <li class="mt-list-item done">
                    <div class="list-icon-container">
                        <button>&times;</button>
                    </div>
                    <div class="list-datetime">  </div>
                    <div class="list-item-content">
                        <h3 class="append_name">

                        </h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="mt-element-list teachers" data-id = "">
        <div class="mt-list-head list-simple ext-1 font-white bg-green-sharp">
            <div class="list-head-title-container">
                <div class="list-date"></div>
                <h3 class="list-title"></h3>
            </div>
        </div>
        <div class="mt-list-container list-simple ext-1">
            <ul>
                <li class="mt-list-item done">
                    <div class="list-icon-container">
                        <button class="remove-teacher">&times;</button>
                    </div>
                    <div class="list-datetime">  </div>
                    <div class="list-item-content">
                        <h3 class="append_name">

                        </h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>



</div>