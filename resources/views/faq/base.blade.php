<?php
$i = 'faqs';
$j = 'index';
?>

@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/dragula/dragula.min.css')}}"/>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.min.js')}}"></script>

@endSection

@section('page_js')
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/faq.js')}}"></script>
<script>
//$(document).on('ready ajax-page-loaded', function () {
//    if ($('.larajsval').length) {
//        $.getScript($('.larajsval').data('validator'));
//    }
//});
$(document).on('click', '.report', function () {
    var url = $(this).data('action');
    var $button = $(this);
    var $tr = $button.closest('tr');
    $.ajax({
        url: url,
        method: 'PATCH',
        success: function (data) {
            $button.remove();
            table.row($tr).remove().draw();
            toastr['success']('Reported successfuly', "Done");
        }
    })
});
</script>
@endSection

@section('add_inits')

@stop

@section('title')
FAQs
@stop

@section('page_title')
FAQs
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

