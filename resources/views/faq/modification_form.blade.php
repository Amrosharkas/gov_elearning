<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Question </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" role="form" id="main-form">
            <div class="form-body">
                <h3>Original Question</h3>
                <p>Question: {{$question->question}}</p>
                <p>Answer: {{$question->answer}}</p>
                <p>Category: {{$question->category->name}}</p>
                <div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
                    <div class="col-md-4">
                        <textarea name='question' class="form-control val" rows="4">{{$question->modification->question}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Answer</label>
                    <div class="col-md-4">
                        <textarea name='answer' class="form-control val" rows="4">{{$question->modification->answer}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Category</label>
                    <div class="col-md-4">
                        <select class="form-control val" name="category">
                            <option value=""></option>
                            @foreach($categories as $category)
                            <option @if( $category->id == $question->modification->category_id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green" id="confirm_modification"
                            data-action="{{route('admin.faq.confirm_modification',['id'=>$question->id])}}"
                            data-method="patch"
                            >Confirm</button>
                    <button type="submit" class="btn btn-lg red" id="reject_modification"
                            data-action="{{route('admin.faq.reject_modification',['id'=>$question->id])}}"
                            data-method="delete"
                            >Reject</button>

                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
