<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Question </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form form-horizontal" role="form" id="main-form">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Question</label>
                    <div class="col-md-4">
                        <textarea name='question' class="form-control val" rows="4">{{$question->question or ''}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Answer</label>
                    <div class="col-md-4">
                        <textarea name='answer' class="form-control val" rows="4">{{$question->answer or ''}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Category</label>
                    <div class="col-md-4">
                        <select class="form-control val" name="category">
                            <option @if(!$question || ! $question->category) selected @endif value=""></option>
                            @foreach($categories as $category)
                            <option @if($question && $question->category && $category->name == $question->category->name) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    @if(! $question || ($question && $question->status == 'saved'))
                    <button type="submit" class="btn btn-lg blue" id="save"
                            @if($question)
                            data-action="{{route('admin.faq.update',['id'=>$question->id])}}"
                            data-method="patch"
                            @else
                            data-action="{{route('admin.faq.store')}}"
                            data-method="post"
                            @endif
                            >Save</button>
                    <button type="submit" class="btn btn-lg green" id="publish"
                            @if($question)
                            data-action="{{route('admin.faq.publish',['id'=>$question->id])}}"
                            data-method="patch"
                            @else
                            data-action="{{route('admin.faq.publish_new')}}"
                            data-method="post"
                            @endif
                            >Publish</button>
                    @elseif($question && $question->status == 'pending')
                    <button type="submit" class="btn btn-lg green" id="confirm"
                            data-action="{{route('admin.faq.confirm',['id'=>$question->id])}}"
                            data-method="patch"
                            >Confirm</button>
                    <button type="submit" class="btn btn-lg red" id="reject"
                            data-action="{{route('admin.faq.reject',['id'=>$question->id])}}"
                            data-method="delete"
                            >Reject</button>
                    @elseif($question && $question->status == 'published')
                    <button type="submit" class="btn btn-lg yellow" id="confirm"
                            data-action="{{route('admin.faq.modify',['id'=>$question->id])}}"
                            data-method="patch"
                            >Modify</button>
                    @endif

                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
