<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
            	<div class="col-md-3">
                
               	 <div class="from-group">
                 @if($status == 'saved')
                    <a href="{{route('admin.faq.create')}}" class="btn green pjax-link">
                        Add New <i class="fa fa-plus"></i>
                    </a>
                    @endif
                    @if($selectedCategory && $status=='published' && LoggedAccount::get()->isSuperAdmin())
                    <button disabled class="btn yellow reorder" data-action="{{route('admin.faq.reorder',['id'=>$selectedCategory->id])}}">Re-order</button>
                    @endif
                    <label>Category</label>
                    <select class="from-control" id="category-filter">
                        <option value="{{route('admin.faq.index',['status'=>$status])}}" @if(!$selectedCategory) selected @endif></option>
                        @foreach($categories as $category)
                        <option value="{{route('admin.faq.index',['status'=>$status,'category'=>$category->id])}}"
                                @if($selectedCategory && $selectedCategory->id == $category->id) selected @endif
                                >{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>     
                </div>     
                <div class="col-md-9">    
               	 <div class="form-group">
                    
                    
                    <div class="md-radio-inline" id="status-fitler">
                                                <div class="md-radio">
                                                    <input type="radio" id="radio14" name="status" @if($status == 'saved') checked @endif value="{{route('admin.faq.index',['status'=>'saved'])}}">
                                                    <label for="radio14">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Saved </label>
                                                </div>
                                                <div class="md-radio">
                                                    <input type="radio" id="radio142" name="status" @if($status == 'published') checked @endif value="{{route('admin.faq.index',['status'=>'published'])}}">
                                                    <label for="radio142">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Published </label>
                                                </div>
                                                @if(LoggedAccount::get()->isSuperAdmin())
                                                    <div class="md-radio">
                                                        <input type="radio" id="radio1422" name="status" @if($status == 'pending') checked @endif value="{{route('admin.faq.index',['status'=>'pending'])}}">
                                                        <label for="radio1422">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Pending </label>
                                                    </div>
                                                    <div class="md-radio">
                                                        <input type="radio" id="radio1421" name="status" @if($status == 'modification') checked @endif value="{{route('admin.faq.index',['status'=>'modification'])}}">
                                                        <label for="radio1421">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Modifications </label>
                                                    </div>
                                                @endif
                                                
                                            </div>
                </div>
                </div>
            </div>
        </div>
        <form id="delete_mul_1" method="post" action="{{route('admin.faq.delete_multiple')}}">
            {!! method_field('delete') !!}
            <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
                <thead>
                    <tr class="tr-head">
                        <th class="no-sorting table-checkbox-col">
                            <input type="checkbox" class="group-checkable">
                        </th>
                        <th valign="middle">
                            Question
                        </th>
                        <th valign="middle">
                            Answer
                        </th>
                        <th valign="middle">
                            Category
                        </th>
                        <th class="no-sorting">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($questions as $question)
                    <tr class="odd gradeX" id="data-row-{{$question->id}}">
                        <td valign="middle">
                            <input type="checkbox" name="items[]"  class="table-checkbox"  value="{{$question->id}}" >
                            <input type="hidden" value="{{$question->id}}" class="reorder-vals">
                        </td>
                        <td valign="middle">
                            {{$question->question}}
                        </td>
                        <td valign="middle">
                            {{$question->answer}}
                        </td>
                        <td valign="middle">
                            {{$question->category? $question->category->name:''}}
                        </td>
                        <td valign="middle">
                            @if($status == 'saved')
                            <a href="{{route('admin.faq.edit',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                            @elseif($status == 'pending')
                            <a href="{{route('admin.faq.review_pending',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn yellow"><i class="fa fa-edit"></i> Review</button></a>
                            @elseif($status == 'published' && $question && ! $question->modified)
                            <a href="{{route('admin.faq.get_modify',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn yellow"><i class="fa fa-edit"></i> Modify</button></a>
                            @elseif($status == 'modification')
                            <a href="{{route('admin.faq.review_modification',['id'=>$question->id])}}" class="pjax-link"><button type="button" class="btn yellow"><i class="fa fa-edit"></i> Review</button></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>