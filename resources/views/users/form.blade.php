<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Advance Validation
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title="">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
            </a>
            <a href="javascript:;" class="reload" data-original-title="" title="">
            </a>
            <a href="javascript:;" class="remove" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body form">

        @if($item)  				
        {!! Form::model($item,['method'=>'post','url'=>'/crud/'.$model_name.'/'.$item->id.'/update','class'=>'j-forms ajaxForm','id'=>'j-forms','novalidate']) !!}
        @else
        {!! Form::open(['method'=>'post','url'=>'/crud/'.$model_name.'','class'=>'j-forms ajaxForm','id'=>'j-forms','novalidate']) !!}
        @endif
        <div class="content">

            <!-- start title -->
            <div class="j-row">
                <div class="span4">
                    <label class="label label-center">Title</label>
                </div>
                <div class="span8 unit">
                    <div class="input">
                        <label class="icon-right" for="name">
                            <i class="fa fa-user"></i>
                        </label>
                        {!! Form::email('email',null,['id'=>'email']) !!}
                    </div>
                </div>
            </div>
            <!-- end name -->

            

            <!-- start login -->
            <!-- end login -->

            


            <!-- start response from server -->
            <div id="response" style="display:none;"></div>
            <!-- end response from server -->

        </div>
        <!-- end /.content -->

        <div class="footer">
            <button type="submit" class="primary-btn" id="enable-button" >Submit</button>
        </div>
        <!-- end /.footer -->
        {!! Form::close() !!}

    </div>
    <!-- END VALIDATION STATES-->
</div>
