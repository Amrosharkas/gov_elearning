@extends('admin.master')
@section('default-page'){{$model_name}}/list
@stop
@section('add_css')
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('add_js_plugins')
<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@stop

@section('add_js_scripts')
<script src="/assets/admin/pages/scripts/table-managed.js"></script>
<script type="text/javascript" src="/vendor/jsvalidation.min.js"></script>

<script type="text/javascript">
    ajaxPageHandler.init();
    taps.onajaxpageload = function (pageurl) {
        if (pageurl != "#") {

            window.history.pushState("stateObj", "Title", "#" + pageurl);
        }

        {!!JsValidator::formRequest('App\Http\Requests\\'.$model_name.'FormRequest')!!}
        $.getScript("/assets/admin/pages/scripts/ajax-forms.js");
        TableManaged.init();
        DeleteHandler.init();
        checkboxHandler.init();
        datetimepickersHandler.init();

    }
</script>
@stop

@section('add_inits')

@stop

@section('title')
Users
@stop

@section('page_title')
Users
@stop

@section('page_title_small')

@stop

@section('content')

<div id="countrytabs" class="shadetabs" style="display:none;" >

    <li>
        <a href="#" rel="#default" class="selected">Tab 1</a>
    </li>
</div>

@stop