
<div class="portlet box grey-cascade">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-globe"></i>Users
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			
			
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6">
					<div class="btn-group">
						<a href="javascript: taps.loadajaxpage('/crud/{{$model_name}}/create')" >
						<button id="sample_editable_1_new" class="btn green">
						Add New <i class="fa fa-plus"></i>
						</button></a>
                                                    <button type="button" class="btn red delete_multiple" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Hide</button>
					
					</div>
			  </div>
				<div class="col-md-6"></div>
			</div>
		</div>
            <form name="vpb_form_name" id="deleteMult" method="post" action="">
        {!! csrf_field() !!}
		<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr class="tr-head">
			<th class="table-checkbox">
				<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
			</th>
			<th>
				 Email
			</th>
			
			<th>
				 Actions
			</th>
		</tr>
		</thead>
		<tbody>
        @foreach($items as $item)
		<tr class="odd gradeX" id="data-row-{{$item->id}}">
			<td valign="middle">
				<input type="checkbox" name="users[]"  class="checkboxes"  value="{{$item->id}}" >
			</td>
			<td valign="middle">
				 {{$item->email}}
		  </td>
			
			<td valign="middle">
                            <a href="javascript: taps.loadajaxpage('/crud/{{$model_name}}/{{$item->id}}/edit')"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
				<button type="button" id="{{$item->id}}" data-model="{{$model_name}}" class="btn red delete"><i class="fa fa-remove"></i> Hide</button></td>
		</tr>
        @endforeach
		</tbody>
		</table>
            </form>
	</div>
</div>