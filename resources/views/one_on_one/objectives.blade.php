<ol class="dd-list">
    @foreach($objectives as $objective)
    <li class="dd-item objective"  data-content="{{$objective['content']}}" data-id="{{$objective['id']}}">

        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
        </div>
        <div >
            <input type="hidden" name="id">
            <div class="form-group">
                <input type="text" class="form-control" name="objective-content" value="{{$objective['content']}}" placeholder="Type the objective here" />
                <button class="btn btn-default btn-sm remove_objective" type="button"><i class="fa fa-remove"></i> Remove</button>
            </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
        @if(isset($objective['children']))
        @include('one_on_one.objectives',['objectives'=>$objective['children']])
        @endif
    </li>
    @endforeach
</ol>

