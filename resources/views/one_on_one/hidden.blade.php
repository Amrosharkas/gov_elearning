<div id="hidden" class="hidden" hidden>
    @include('one_on_one.hidden.pricing',['pricing'=>false])
    @include('one_on_one.hidden.level',['level'=>false])
    <li class="dd-item objective"  data-content="" data-id="">
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
        </div>
        <div >
            <input type="hidden" name="id">
            <div class="form-group">
                <input type="text" class="form-control" name="objective-content" placeholder="Type the objective here" />
                <button class="btn btn-default btn-sm remove_objective remove_objective" type="button"><i class="fa fa-remove"></i> Remove</button>
            </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
    </li>
</div>
