<div class="portlet box blue-hoki pricing" data-id="{{$pricing?$pricing->id:''}}">
    <div class="portlet-title">
        <div class="caption">
            {{$pricing ? $pricing->count : ''}} Sessions </div>
        <div class="actions tools">
            <button class="btn btn-default btn-sm remove_pricing"
                    data-action="{{$pricing?route('admin.one_on_one.delete_pricing',['id'=>$pricing->id]):''}}"
                    type="button"><i class="fa fa-remove"></i> Remove</button>
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-inline">
            <input type="hidden"  name="id" value="{{$pricing? $pricing->id : ''}}">
            <div class="form-group">
                <input type="text" name="count" class="form-control" placeholder="Count" value="{{$pricing? $pricing->count: ''}}">
            </div>
            <div class="form-group">
                <input type="text" name="basic_basic" class="form-control" placeholder="B Teacher-B Time" value="{{$pricing ? $pricing->basic_basic: ''}}">
            </div>
            <div class="form-group">
                <input type="text" name="basic_premium" class="form-control" placeholder="B Teacher-P Time" value="{{$pricing? $pricing->basic_premium: ''}}">
            </div>
            <div class="form-group">
                <input type="text" name="premium_basic" class="form-control" placeholder="P Teacher-B Time" value="{{$pricing? $pricing->premium_basic:''}}">
            </div>
            <div class="form-group">
                <input type="text" name="premium_premium" class="form-control" placeholder="P Teacher-P Time" value="{{$pricing? $pricing->premium_premium:''}}">
            </div>
        </div>
    </div>
</div>
