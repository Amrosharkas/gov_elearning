<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>One on One
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_course" data-action="{{route('admin.one_on_one.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">
                        Section
                    </th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($courses as $course)
                <tr class="odd gradeX" id="data-row-{{$course->id}}">
                    <td valign="middle">
                        {{$course->title}}
                    </td>
                    <td valign="middle">
                        @if($course->section)
                        {{$course->section->title}}
                        @endif
                    </td>
                    <td valign="middle">
                        {{$course->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.one_on_one.edit',['id'=>$course->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="{{route('admin.one_on_one.delete',['id'=>$course->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>