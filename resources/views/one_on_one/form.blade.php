<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Course</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div class="form" id="main-form" novalidate  class="form-horizontal"> 
            <div class="global-errors alert alert-danger hidden"></div>
            <div class="form-body">
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <label>Section</label>
                            <select name="section" class="form-control val">
                                <option value="" @if(! $course->section) selected @endif></option>
                                @foreach($sections as $section)
                                <option @if($course->section && ($course->section->id == $section->id)) selected @endif value="{{$section->id}}">{{$section->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input name="course_title" class="form-control val" value="{{$course->title}}">
                        </div>
                        <div class="form-group">
                            <label class="mt-checkbox">
                                <input type="checkbox" @if($course->has_vc) checked  @endif name="has_vc" value="{{$course->has_vc}}"> Requires Virtual Classroom
                                       <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Descriptions </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <label>Description of overall courses page</label>
                            <textarea name="description_overall" class="form-control val">{{$course->description_overall}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Description in section page</label>
                            <textarea name="description_section" class="form-control val">{{$course->description_section}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Description in course page</label>
                            <textarea name="description_course" class="form-control val">{{$course->description_course}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Course details </div>
                    <div class="ribbon-content">
                        <div class="form-group">
                            <label>Maximum allowed Frequency/week</label>
                            <select name="max_frequency" class="form-control val" >
                                @for($i = 1; $i < 8; $i++)
                                <option value="{{$i}}" @if($course->max_frequency == $i) selected @endif>{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Session Duration</label>
                            <input name="session_duration" class="form-control val" value="{{$course->session_duration}}">
                        </div>
                        <div class="form-group">
                            <label>Maximum start date</label>
                            <input name="max_start_date" class="form-control val" value="{{$course->max_start_date}}">
                        </div>
                    </div>
                </div>
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Media </div>
                    <div class="ribbon-content">
                        <!-- SECTION IMAGE -->
                        <div class="form-group">
                            <label >Section Image</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.one_on_one.upload_section_image',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->sectionImage())
                                        <a href="{{route('admin.one_on_one.section_image',['id'=>$course->id])}}" class="filename">
                                            {{$course->sectionImage()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->sectionImage()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->sectionImage()?'':'hidden'}}" 
                                            data-action="{{route('admin.one_on_one.delete_section_image',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Course Image</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.one_on_one.upload_course_image',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->courseImage())
                                        <a href="{{route('admin.one_on_one.course_image',['id'=>$course->id])}}" class="filename">
                                            {{$course->courseImage()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->courseImage()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->courseImage()?'':'hidden'}}" 
                                            data-action="{{route('admin.one_on_one.delete_section_image',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Promo</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.one_on_one.upload_promo',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->promo())
                                        <a href="{{route('admin.one_on_one.promo',['id'=>$course->id])}}" class="filename">
                                            {{$course->promo()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->promo()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->promo()?'':'hidden'}}" 
                                            data-action="{{route('admin.one_on_one.delete_promo',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline" id="shifts">
                            <label>Choose Shifts</label>
                            <div class="mt-checkbox-inline">
                                @foreach($shifts as $shift)
                                <label class="mt-checkbox">
                                    <input type="checkbox" @if($course->shifts->where('id',$shift->id)->count()) checked  @endif name="shifts" value="{{$shift->id}}"> {{$shift->name}} 
                                           <span></span>
                                </label>
                                @endforeach
                            </div>     
                        </div>

                    </div>

                </div>
                <!-- PRICINGS -->
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Session Pricing </div>
                    <div class="ribbon-content">

                        <div id="pricings">
                            @foreach($pricings as $pricing)
                            @include('one_on_one.hidden.pricing',['pricing'=>$pricing])
                            @endforeach
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="add_pricing" data-url="{{route('admin.one_on_one.init_pricing',['id'=>$course->id])}}"><i class="fa fa-plus"></i> Add </button>
                        </div>
                    </div>
                </div>
                <!-- END PRICINGS -->
                <!-- LERNING OBJECTIVES -->
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Learning Objectives </div>
                    <div class="ribbon-content">
                        <!-- LEARNING OBJECTIVES -->
                        <div class="dd" id="objectives">
                            @include('one_on_one.objectives',$objectives)
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="add_objective" data-url="{{route('admin.one_on_one.init_objective',['id'=>$course->id])}}"><i class="fa fa-plus"></i> Add </button>
                        </div>
                    </div>
                </div>
                <!-- END LERNING OBJECTIVES -->
                <!-- LEVELS -->
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Levels </div>
                    <div class="ribbon-content">
                        <!-- LEVELS -->
                        <div id="levels">
                            @foreach($levels as $level)
                            @include('one_on_one.hidden.level',['level'=>$level])
                            @endforeach
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="add_level" data-url="{{route('admin.one_on_one.init_level',['id'=>$course->id])}}"><i class="fa fa-plus"></i> Add </button>
                        </div>                       
                    </div>                       
                </div>
                <!-- END LEVELS -->

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all"
                            data-action="{{route('admin.one_on_one.update',['id'=>$course->id])}}"
                            >Save</button>
                    <button type="button" class="btn btn-lg green" id="publish"
                            data-action="{{route('admin.one_on_one.publish',['id'=>$course->id])}}"
                            >Publish</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>


@include('one_on_one.hidden')
