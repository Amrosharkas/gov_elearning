@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        {!! Form::model($model,['route' =>  'exp.validation.update','method' => 'patch']) !!}
            @include('experiments.validation.form')
        {!! Form::close() !!}

    </div>
</div>
@endsection
