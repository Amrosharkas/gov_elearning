@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        {!! Form::open(['route' =>  'exp.validation.store','method' => 'post']) !!}
        @include('experiments.validation.form')
        {!! Form::close() !!}
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('vendor/jsvalidation.min.js')}}"></script>
<script>
jQuery(document).ready(function () {

    $("form").validate({
        errorElement: 'span',
        errorClass: 'help-block error-help-block',
        errorPlacement: function (error, element) {
            console.log(error);
            if (element.parent('.input-group').length ||
                    element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                error.insertAfter(element.parent());
                // else just place the validation message immediatly after the input
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error'); // add the Bootstrap error class to the control group
        },
        /*
         // Uncomment this to mark as validated non required fields
         unhighlight: function(element) {
         $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
         },
         */
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // remove the Boostrap error class from the control group
        },
        focusInvalid: false, // do not focus the last invalid input

        rules: {"title": {"laravelValidation": [["Required", [], "The title field is required.", true]]}}})
})
</script>
@endSection
