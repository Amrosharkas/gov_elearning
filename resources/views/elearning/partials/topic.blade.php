<div class="topic bordered unit" data-id="{{$topic?$topic->id :''}}">
    <div class="form-group">
        <label>Title</label>
        <input name="title" class="form-control" type="text" value="{{$topic?$topic->title:''}}">
    </div>
    <div class="form-group">
        <label >Content</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$topic?route('admin.elearning.module.upload_content',['id'=>$topic->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($topic && $topic->content)
                    <a href="{{$topic->contentPath()}}" class="filename">
                        View
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$topic && $topic->content?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$topic && $topic->content?'':'hidden'}}" 
                        data-action="{{$topic && $topic->content ? route('admin.elearning.module.delete_content',['id'=>$topic->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label >Material</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$topic ?route('admin.elearning.module.upload_material',['id'=>$topic->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($topic && $topic->material())
                    <a href="{{route('admin.elearning.module.material',['id'=>$topic->id])}}" class="filename">
                        {{$topic->material()->name}}
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$topic && $topic->material()?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$topic && $topic->material()?'':'hidden'}}" 
                        data-action="{{$topic && $topic->material()?route('admin.elearning.module.delete_material',['id'=>$topic->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-danger remove_unit"
                data-action="{{$topic ? route('admin.elearning.topic.delete',['id'=>$topic->id]):''}}"
                >Remove</button>
    </div> 
</div>
