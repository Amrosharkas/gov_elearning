<div class="module bordered unit" data-id="{{$module?$module->id:''}}">
    <div class="form-group">
        <label>Title</label>
        <input name="title" class="form-control" type="text" value="{{$module?$module->title:''}}">
    </div>
    <div class="form-group">
        <label >Content</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$module?route('admin.elearning.module.upload_content',['id'=>$module->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($module && $module->content)
                    <a href="{{$module->contentPath()}}" class="filename">
                        View
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$module && $module->content?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$module && $module->content?'':'hidden'}}" 
                        data-action="{{$module && $module->content ? route('admin.elearning.module.delete_content',['id'=>$module->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label >Material</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$module ?route('admin.elearning.module.upload_material',['id'=>$module->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($module && $module->material())
                    <a href="{{route('admin.elearning.module.material',['id'=>$module->id])}}" class="filename">
                        {{$module->material()->name}}
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$module && $module->material()?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$module && $module->material()?'':'hidden'}}" 
                        data-action="{{$module && $module->material()?route('admin.elearning.module.delete_material',['id'=>$module->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-danger remove_unit"
                data-action="{{$module ? route('admin.elearning.module.delete',['id'=>$module->id]):''}}"
                >Remove</button>
    </div> 
    @if(isset($isMap))
    <div class="bordered lessons-area">
        @foreach($mdlLessons as $mdlLesson)
        @include('elearning.partials.lesson',['lesson'=>$mdlLesson,'isMap'=>true,'lsnTopics'=> $mdlLesson->topics ])
        @endforeach
    </div>
    @endif
</div>
