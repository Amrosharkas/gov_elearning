<div class="lesson bordered unit" data-id="{{$lesson?$lesson->id:''}}">
    <div class="form-group">
        <label>Title</label>
        <input name="title" class="form-control" type="text" value="{{$lesson?$lesson->title:''}}">
    </div>
    <div class="form-group">
        <label >Content</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$lesson?route('admin.elearning.module.upload_content',['id'=>$lesson->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($lesson && $lesson->content)
                    <a href="{{$lesson->contentPath()}}" class="filename">
                        View
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$lesson && $lesson->content?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$lesson && $lesson->content?'':'hidden'}}" 
                        data-action="{{$lesson && $lesson->content ? route('admin.elearning.module.delete_content',['id'=>$lesson->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label >Material</label>
        <input type="file" class="hidden fileinput"
               data-action="{{$lesson ?route('admin.elearning.module.upload_material',['id'=>$lesson->id]):''}}" 
               >
        <div class="row">
            <div class="col-md-8">
                <span class="filenameplaceholder">
                    @if($lesson && $lesson->material())
                    <a href="{{route('admin.elearning.module.material',['id'=>$lesson->id])}}" class="filename">
                        {{$lesson->material()->name}}
                    </a>
                    @else
                    No file selected
                    @endif
                </span>
                <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                    <span>0%</span>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-primary filepicker pull-right {{$lesson && $lesson->material()?'hidden':''}}">
                    Select File
                </button>
                <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                <button class="btn btn-danger pull-right removefile {{$lesson && $lesson->material()?'':'hidden'}}" 
                        data-action="{{$lesson && $lesson->material()?route('admin.elearning.module.delete_material',['id'=>$lesson->id]):''}}"
                        >
                    Remove
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-danger remove_unit"
                data-action="{{$lesson ? route('admin.elearning.lesson.delete',['id'=>$lesson->id]):''}}"
                >Remove</button>
    </div> 
    @if(isset($isMap))
    <div class="bordered topics-area">
        @foreach($lsnTopics as $lsnTopic)
        @include('elearning.partials.topic',['topic'=>$lsnTopic])
        @endforeach
    </div>
    @endif

</div>
