<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Course</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <div class="panel panel-primary">
                <div class="panel-heading">Main info</div>
                <div class="panel-body">
                    <div class="form" id="main-data">

                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Learning Objectives
                </div>
                <div class="panel-body">
                    <div class="dd" id="objectives">
                        @include('elearning.partials.objectives',$objectives)
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" id="add_objective" ><i class="fa fa-plus"></i> Add </button>
                    </div>

                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Modules</div>
                <div class="panel-body">
                    <div id="modules" class="bordered">
                        @foreach($modules as $module)
                        @include('elearning.partials.module',['module'=>$module])
                        @endforeach
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" id="add_module"
                                data-action="{{route('admin.elearning.module.init',['course_id'=>$course->id])}}"
                                >Add module</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Lessons</div>
                <div class="panel-body">
                    <div id="lessons" class="bordered">
                        @foreach($lessons as $lesson)
                        @include('elearning.partials.lesson',['lesson'=>$lesson])
                        @endforeach
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" id="add_lesson"
                                data-action="{{route('admin.elearning.lesson.init',['course_id'=>$course->id])}}"
                                >Add lesson</button>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Topics</div>
                <div class="panel-body">
                    <div id="topics" class="bordered">
                        @foreach($topics as $topic)
                        @include('elearning.partials.topic',['topic'=>$topic])
                        @endforeach
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" id="add_topic" 
                                data-action="{{route('admin.elearning.topic.init',['course_id'=>$course->id])}}"
                                >Add topic</button>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">Map</div>
                <div class="panel-body" id="map">
                    <div class="bordered" id="modules-area">
                        @foreach($map as $mdl)
                        @include('elearning.partials.module',['module'=>$mdl,'isMap'=>true,'mdlLessons'=>$mdl->lessons])
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-set pull-right">
                <button type="button" class="btn btn-lg blue" id="save" 
                        data-action="{{route('admin.elearning.course.update',['id'=>$course->id])}}">
                    Save</button>
                <button type="button" class="btn btn-lg green" id="publish" >
                    Publish</button>
            </div>
        </div>
    </div>
</div>
<div class="hidden" id="hidden">
    @include('elearning.partials.module',['module'=>false])
    @include('elearning.partials.lesson',['lesson'=>false])
    @include('elearning.partials.topic',['topic'=>false])
    <li class="dd-item objective"  data-content="" data-id="">
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
        </div>
        <div >
            <input type="hidden" name="id">
            <div class="form-group">
                <input type="text" class="form-control" name="objective-content" placeholder="Type the objective here" />
                <button class="btn btn-default btn-sm remove_objective remove_objective" type="button"><i class="fa fa-remove"></i> Remove</button>
                <button class="btn btn-success">Covers</button>
            </div>

        </div>
        <div class="dd-handle handle-hidden" ></div>
    </li>
</div>