<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($course)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate method="POST" class="form"> 
            <div class="form-body">
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                    <p class="ribbon-content"><div class="form-group">
                        <div class="hide form-group">
                            <label>Section</label>
                            <select name="section" class="form-control val">
                                <option value="1" @if(! $course->section) selected @endif></option>
                                @foreach($sections as $section)
                                <option @if($course->section && ($course->section->id == $section->id)) selected @endif value="{{$section->id}}">{{$section->title}}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="form-group">
                            <label>Title</label>
                            <input name="title" class="form-control val" value="{{$course->title}}">
                        </div>
                        </p>

                    </div>
                </div>

                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Descriptions </div>
                    <p class="ribbon-content"><div class="form-group">
                        <div class="form-group val">
                            <label>Description of overall courses page</label>
                            <textarea name="description_overall" class="form-control">{{$course->description_overall}}</textarea>
                        </div>
                        <div class="form-group val">
                            <label>Description in section page</label>
                            <textarea name="description_section" class="form-control">{{$course->description_section}}</textarea>
                        </div>
                        <div class="form-group val">
                            <label>Description in course page</label>
                            <textarea name="description_course" class="form-control">{{$course->description_course}}</textarea>
                        </div>
                        </p>

                    </div>
                </div>


                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div>Course details </div>
                    <p class="ribbon-content"><div class="form-group">
                        <div class="form-group val">
                            <label>Maximum Number of Students</label>
                            <input name="max_students" class="form-control" value="{{$course->max_students}}">
                        </div>
                        <div class="form-group val">
                            <label>Estimated time</label>
                            <input name="estimated_time" class="form-control" value="{{$course->estimated_time}}">
                        </div>                </p>

                    </div>
                </div>

                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Dependency </div>
                    <p class="ribbon-content"><div class="form-group">
                        <div class="form-group">
                            <label>Test your self</label>
                            <select name="test_your_self" class="form-control val">
                                <option value=""></option>
                                @foreach($quizzes as $quiz)
                                <option @if($course->test_your_self_id == $quiz->id) selected @endif value="{{$quiz->id}}">{{$quiz->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Dependent Course</label>
                            <select name="dependent_course" class="form-control val">
                                <option @if(! $course->prerequisite_id ) selected @endif value=""></option>
                                @foreach($courses as $coursez)
                                <option @if($course->prerequisite_id == $coursez->id) selected @endif value="{{$coursez->id}}">{{$coursez->title}}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="form-group @if(!$course->prerequisite_id) hidden @endif" id="level-access-test">
                            <label>Level access test</label>
                            <select name="level_access" class="form-control val">
                                <option value=""></option>
                                @foreach($quizzess as $quiz)
                                <option @if($course->level_access_id == $quiz->id) selected @endif value="{{$quiz->id}}">{{$quiz->title}}</option>
                                @endforeach
                            </select>
                        </div>               </p>

                    </div>
                </div>

                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Media </div>
                    <p class="ribbon-content"><div class="form-group">
                        <!-- SECTION IMAGE -->
                        <div class="form-group">
                            <label >Section Image</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.blended.upload_section_image',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->sectionImage())
                                        <a href="{{route('admin.blended.section_image',['id'=>$course->id])}}" class="filename">
                                            {{$course->sectionImage()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->sectionImage()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->sectionImage()?'':'hidden'}}" 
                                            data-action="{{route('admin.blended.delete_section_image',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END SECTION IMAGE -->
                        <!-- COURSE IMAGE -->
                        <div class="form-group">
                            <label >Course Image</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.blended.upload_course_image',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->courseImage())
                                        <a href="{{route('admin.blended.course_image',['id'=>$course->id])}}" class="filename">
                                            {{$course->courseImage()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->courseImage()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->courseImage()?'':'hidden'}}" 
                                            data-action="{{route('admin.blended.delete_course_image',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END COURSE IMAGE -->
                        <!-- END SECTION IMAGE -->
                        <!-- PROMO -->
                        <div class="form-group">
                            <label >Promo</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.blended.upload_promo',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->promo())
                                        <a href="{{route('admin.blended.promo',['id'=>$course->id])}}" class="filename">
                                            {{$course->promo()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->promo()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->promo()?'':'hidden'}}" 
                                            data-action="{{route('admin.blended.delete_promo',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END PROMO -->                </p>

                    </div>
                </div>
                
                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Course Material </div>
                    <p class="ribbon-content"><div class="form-group">
                        <!-- SECTION IMAGE -->
                        <div class="form-group">
                            <label >Upload Material</label>
                            <input type="file" class="hidden fileinput"
                                   data-action="{{route('admin.blended.upload_course_material',['id'=>$course->id])}}" 
                                   >
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="filenameplaceholder">
                                        @if($course->courseMaterial())
                                        <a href="{{route('admin.blended.course_material',['id'=>$course->id])}}" class="filename">
                                            {{$course->courseMaterial()->name}}
                                        </a>
                                        @else
                                        No file selected
                                        @endif
                                    </span>
                                    <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                        <span>0%</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary filepicker pull-right {{$course->courseMaterial()?'hidden':''}}">
                                        Select File
                                    </button>
                                    <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                                    <button class="btn btn-danger pull-right removefile {{$course->courseMaterial()?'':'hidden'}}" 
                                            data-action="{{route('admin.blended.delete_course_material',['id'=>$course->id])}}"
                                            >
                                        Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- END SECTION IMAGE -->

                    </div>
                </div>


                <div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Learning Objectives </div>
                    <p class="ribbon-content">
                        <!-- LEARNING OBJECTIVES -->
                    <div class="dd" id="objectives">
                        @include('one_on_one.objectives',$objectives)
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" id="add_objective" data-url="{{route('admin.one_on_one.init_objective',['id'=>$course->id])}}"><i class="fa fa-plus"></i> Add </button>
                    </div>
                    <!-- END LEARNING OBJECTIVES -->

                    </p>

                </div>

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg blue" id="save-all" 
                            data-action="{{route('admin.blended.update',['id'=>$course->id])}}">
                        Save
                    </button>
                    <button type="button" class="btn btn-lg green" id="save-all" 
                            data-action="{{route('admin.blended.publish',['id'=>$course->id])}}">
                        Publish
                    </button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
@include('blended.hidden')
