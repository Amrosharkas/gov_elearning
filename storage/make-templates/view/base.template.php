@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
@endSection

@section('page_js')
<script>
var pageAttributes = {
    indexUrl: "{{route('admin.%%%%routeNamePrefix%%%%.index')}}",
}
</script>
<script type="text/javascript" src="{{asset('assets/ajaxForms.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
{!!JsValidator::formRequest('App\Http\Requests\%%%%FormRequest%%%%')!!}
<script>
$(document).on('ready ajax-page-loaded', function () {
    jsValidate();
});
$(document).on('submit', '.larajsval', function () {
    jsValidate();
});
$(document).on('change blur', '.larajsval :input', function () {
    jsValidate();
});
</script>
@endSection

@section('add_inits')

@stop

@section('title')
%%%%tabTitle%%%%
@stop

@section('page_title')
%%%%pageTitle%%%%
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

