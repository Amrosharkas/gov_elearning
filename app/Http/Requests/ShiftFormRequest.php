<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class ShiftFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {


        $rules = [
            'name' => ['required'],
            'start_time' => ['bail', 'required', 'regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/', 'different:end_time'],
            'end_time' => ['bail', 'required', 'regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/', 'different:start_time']
        ];
        if ($id = $this->route('id')) {
            $rules['name'][] = 'unique:shifts,name,' . $id;
        } else {
            $rules['name'][] = 'unique:shifts,name';
        }
        return $rules;
    }

}
