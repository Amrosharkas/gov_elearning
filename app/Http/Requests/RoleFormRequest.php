<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RoleFormRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'name' => ['required'],
            'abilities' => ['required']
        ];
        if ($id = $this->route('id')) {
            $rules['name'][] = 'unique:roles,name,' . $id;
        } else {
            $rules['name'][] = 'unique:roles,name';
        }
        return $rules;
    }

}
