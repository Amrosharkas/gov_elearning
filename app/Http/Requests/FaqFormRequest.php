<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FaqFormRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = [
            'question' => ['required'],
            'category' => ['required_with:published', 'exists:faq_categories,id'],
            'published' => ['boolean'],
            'answer' => ['required_with:published']
        ];
        return $rules;
    }

    public function attributes() {
        return ['category_id' => 'category'];
    }

    public function messages() {
        return [
            'category_id.required_with' => 'You can\'t publish without a category',
            'answer.required_with' => 'You can\'t publish without an answer',
        ];
    }

}
