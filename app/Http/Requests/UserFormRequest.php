<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;
use App\User;

class UserFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $types = implode(',', User::getBackendUserTypes());
        $rules = [
            'email' => ['required', 'email'],
            'type' => ['required', 'in:' . $types],
            'name' => ['required']
        ];
        if ($id = $this->route('id')) {
            $rules['email'][] = 'unique:users,email,' . $id;
        } else {
            $rules['email'][] = 'unique:users,email';
        }
        return $rules;
    }

}
