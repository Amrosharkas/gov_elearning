<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlendedCourse;
use App\Section;
use App\File;
use Validator;
use App\quiz;

class BlendedController extends Controller {

    public function index() {
        $data = [];
        $data['partialView'] = 'blended.list';
        $data['courses'] = BlendedCourse::with('section')->orderBy('created_at', 'desc')->get();
        return view('blended.base', $data);
    }

    public function init() {
        $course = new BlendedCourse();
        $course->save();
        return response(['url' => route('admin.blended.edit', ['id' => $course->id])]);
    }

    public function edit($id) {
        $course = BlendedCourse::findOrFail($id);
        $data = [];
        $data['partialView'] = 'blended.form';
        $data['course'] = $course;
        $data['courses'] = BlendedCourse::where('id', '!=', $id)->get();
        $data['sections'] = Section::all();
        $data['quizzes'] = quiz::all();
        $data['quizzess'] = quiz::all();
        $data['objectives'] = $course->objectives ? json_decode($course->objectives, 1) : [];
        return view('blended.base', $data);
    }

    public function uploadSectionImage($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/section_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->section_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_section_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.section_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadCourseImage($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/course_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->course_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_section_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.section_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadCourseMaterial($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/course_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->course_material_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_course_material', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.course_material', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadPromo($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/promo');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->promo_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_promo', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.promo', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function sectionImage($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->sectionImage()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function courseImage($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->courseImage()->path, $course->sectionImage()->name);
        }
        abort(404);
    }
    public function courseMaterial($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->courseMaterial()) {
            return response()->download($course->courseMaterial()->path, $course->courseMaterial()->name);
        }
        abort(404);
    }

    public function promo($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->promo()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function deleteSectionImage($id) {
        BlendedCourse::findOrFail($id)->deleteSectionImage();
    }

    public function deleteCourseImage($id) {
        BlendedCourse::findOrFail($id)->deleteCourseImage();
    }
    public function deleteCourseMaterial($id) {
        BlendedCourse::findOrFail($id)->deleteCourseMaterial();
    }

    public function deletePromo($id) {
        BlendedCourse::findOrFail($id)->deletePromo();
    }

    public function update(Request $request, $id) {
        // VALIDATION
        $errors = [];
        $hasErrors = false;
        $course = BlendedCourse::findOrFail($id);
        $rules = [
            'section' => ['integer', 'exists:sections,id'],
            'dependent_course' => ['integer', 'exists:blended_courses,id'],
            'title' => ['required'],
            'objectives' => ['array'],
            'max_students' => ['integer'],
            'estimated_time' => ['integer'],
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        /*if ($request->dependent_course > 0 && $request->dependent_course == $id) {
            $hasErrors = true;
        }*/
        $errors['objectives'] = [];
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        // END VALIDATION
        if (is_array($request->objectives)) {
            $course->objectives = json_encode($request->objectives);
        }
        $course->title = $request->title;
        $course->section_id = $request->section;
        $course->description_overall = $request->description_overall;
        $course->description_section = $request->description_section;
        $course->description_course = $request->description_course;
        $course->max_students = $request->max_students;
        $course->estimated_time = $request->estimated_time;
        $course->prerequisite_id = $request->dependent_course;
        $course->level_access_id = $request->level_access;
        $course->test_your_self_id = $request->test_your_self;
        $course->saved = true;
        $course->save();
        $responseData = [];
        $responseData['url'] = route('admin.blended.index');
        return response()->json($responseData);
    }

    public function publish($id, Request $request) {
        // VALIDATION
        $errors = [];
        $hasErrors = false;
        $course = BlendedCourse::findOrFail($id);
        $rules = [
            'section' => ['required', 'integer', 'exists:sections,id'],
            'dependent_course' => ['integer', 'exists:blended_courses,id'],
            'title' => ['required'],
            'description_overall' => ['required'],
            'description_section' => ['required'],
            'description_course' => ['required'],
            'objectives' => ['array'],
            'max_students' => ['required', 'integer'],
            'estimated_time' => ['required', 'integer'],
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        if ($request->dependent_course > 0 && $request->dependent_course == $id) {
            $hasErrors = true;
        }
        $objectives = $request->objectives;
        $errors['objectives'] = [];
        if (is_array($objectives)) {
            $this->validateObjectives($objectives, $errors['objectives']);
            if (count($errors['objectives'])) {
                $hasErrors = true;
            }
        }
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        // END VALIDATION
        if (is_array($request->objectives)) {
            $course->objectives = json_encode($request->objectives);
        }
        $course->title = $request->title;
        $course->section_id = $request->section;
        $course->description_overall = $request->description_overall;
        $course->description_section = $request->description_section;
        $course->description_course = $request->description_course;
        $course->max_students = $request->max_students;
        $course->estimated_time = $request->estimated_time;
        $course->prerequisite_id = $request->dependent_course;
        $course->level_access_id = $request->level_access;
        $course->test_your_self_id = $request->test_your_self;
        $course->saved = true;
        $course->published = true;
        $course->save();
        $responseData = [];
        $responseData['url'] = route('admin.blended.index');
        return response()->json($responseData);
    }

    public function delete($id) {
        BlendedCourse::destroy($id) or abort(404);
    }

    protected function validateObjectives($objectives, &$errors) {
        foreach ($objectives as $objective) {
            if (!trim($objective['content'])) {
                $errors[$objective['id']] = true;
            }
            if (isset($objective['children']) && is_array($objective['children'])) {
                $this->validateObjectives($objective['children'], $errors);
            }
        }
    }

}
