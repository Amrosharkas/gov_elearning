<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shift;
use Validator;
use Carbon\Carbon;

class ShiftController extends Controller {

    private $rules = [
        'name' => ['required'],
        'start_time' => ['bail', 'required', 'regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/', 'different:end_time'],
        'end_time' => ['bail', 'required', 'regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/', 'different:start_time']
    ];

    public function index() {
        $shifts = Shift::orderBy('updated_at')->get();
        $data = [];
        $data['shifts'] = $shifts;
        $data['partialView'] = 'shift.list';
        return view('shift.base', $data);
    }

    public function create() {
        $data = [];
        $data['shift'] = false;
        $data['partialView'] = 'shift.form';
        return view('shift.base', $data);
    }

    public function edit($id) {
        $shift = Shift::findOrFail($id);
        $data = [];
        $data['shift'] = $shift;
        $data['partialView'] = 'shift.form';
        return view('shift.base', $data);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), $this->publishStoreRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        if ($this->causesInterference($request->start_time, $request->end_time)) {
            return response()->json(['interference' => true]);
        }
        $shift = new Shift();
        $shift->name = $request->name;
        $shift->start_time = $request->start_time;
        $shift->end_time = $request->end_time;
        $shift->save();
        return response()->json(['url' => route('admin.shift.index')]);
    }

    public function update(Request $request, $id) {
        $shift = Shift::findOrFail($id);
        $validator = Validator::make($request->all(), $this->publishUpdateRules($id));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $shift->name = $request->name;
        $shift->start_time = $request->start_time;
        $shift->end_time = $request->end_time;
        $shift->save();
        return response()->json(['url' => route('admin.shift.index')]);
    }

//    public function publishStore(Request $request) {
//        $validator = Validator::make($request->all(), $this->publishStoreRules());
//        if ($validator->fails()) {
//            return response()->json(['errors' => $validator->errors()->messages()]);
//        }
//        $shift = new Shift();
//        $shift->name = $request->name;
//        $shift->start_time = $request->start_time;
//        $shift->end_time = $request->end_time;
//        $shift->save();
//        return response()->json(['url' => route('admin.shift.index')]);
//    }
//
//    public function publishUpdate($id, Request $request) {
//        $shift = Shift::findOrFail($id);
//        $validator = Validator::make($request->all(), $this->publishUpdateRules($id));
//        if ($validator->fails()) {
//            return response()->json(['errors' => $validator->errors()->messages()]);
//        }
//        $shift->name = $request->name;
//        $shift->start_time = $request->start_time;
//        $shift->end_time = $request->end_time;
//        $shift->save();
//        return response()->json(['url' => route('admin.shift.index')]);
//    }

    public function delete($id) {
        $status = Shift::findOrFail($id)->delete();
        return response()->json(compact('status'));
    }

    public function deleteMultiple(Request $request) {
        Shift::whereIn('id', $request->items)->delete();
    }

//    public function updateRules($id) {
//        $rules = $this->rules;
//        unset($rules['name'][0]);
//        unset($rules['start_time'][1]);
//        unset($rules['end_time'][1]);
//        $rules['name'][] = 'unique:shifts,name,' . $id;
//        return $rules;
//    }

    public function publishUpdateRules($id) {
        $rules = $this->rules;
        $rules['name'][] = 'unique:shifts,name,' . $id;
        return $rules;
    }

//    public function storeRules() {
//        $rules = $this->rules;
//        unset($rules['name'][0]);
//        unset($rules['start_time'][1]);
//        unset($rules['end_time'][1]);
//        $rules['name'][] = 'unique:shifts,name';
//        return $rules;
//    }

    public function publishStoreRules() {
        $rules = $this->rules;
        $rules['name'][] = 'unique:shifts,name';
        return $rules;
    }

    public function causesInterference($start, $end) {
        list($startHour, $startMinute) = explode(':', $start);
        list($endHour, $endMinute) = explode(':', $end);
        $startTime = Carbon::createFromTime($startHour, $startMinute);
        $endTime = Carbon::createFromTime($endHour, $endMinute);
        $shifts = Shift::all();
        foreach ($shifts as $shift) {
            list($sHour, $sMinute) = explode(':', $shift->start_time);
            list($eHour, $eMinute) = explode(':', $shift->end_time);
            $sTime = Carbon::createFromTime($sHour, $sMinute);
            $eTime = Carbon::createFromTime($eHour, $eMinute);
            if (($eTime > $sTime && $startTime >= $sTime && $startTime < $eTime) ||
                    ($eTime > $sTime && $endTime > $sTime && $endTime <= $eTime) ||
                    ($eTime < $sTime && ($startTime >= $sTime || $startTime < $eTime)) ||
                    ($eTime < $sTime && ($endTime > $sTime || $endTime <= $eTime)) ||
                    ($eTime > $sTime && $endTime > $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime < $sTime && $endTime < $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime > $sTime && $endTime < $startTime && (($sTime >= $startTime && $eTime > $startTime) || ($sTime < $endTime && $eTime <= $endTime)))) {
                return true;
            }
        }
        return false;
    }

}
