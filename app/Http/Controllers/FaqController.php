<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Faq;
use App\FaqCategory;
use LoggedAccount;

class FaqController extends Controller {

    public function index($status, Request $request) {
        if (!in_array($status, ['saved', 'pending', 'published', 'modification', 'reported'])) {
            abort(404);
        }
        if (($status == 'pending' || $status == 'modification') && !LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        return $this->faqs($request->category, $status);
    }

    protected function faqs($category, $status) {
        if ($status == 'modification') {
            $query = Faq::where('modified', '=', true);
        } else {
            $query = Faq::where('status', '=', $status);
        }
        if (is_null($category)) {
            $data['selectedCategory'] = false;
        } else {
            $query->where('category_id', '=', $category);
            $data['selectedCategory'] = FaqCategory::find($category);
        }
        $data['questions'] = $query->orderBy('stuff_order')->get();
        $data['categories'] = FaqCategory::orderBy('stuff_order')->get();
        $data['partialView'] = 'faq.list';
        $data['status'] = $status;
        return view('faq.base', $data);
    }

    public function create() {
        $data['question'] = false;
        $data['partialView'] = 'faq.form';
        $data['categories'] = FaqCategory::all();
        return view('faq.base', $data);
    }

    public function store(Request $request) {
        $rules = [
            'question' => ['required'],
            'category' => ['exists:faq_categories,id']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question = new Faq();
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        $question->status = 'saved';
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'saved'])]);
    }

    public function edit($id) {
        $question = Faq::findOrFail($id);
        $data = [];
        $data['question'] = $question;
        $data['partialView'] = 'faq.form';
        $data['categories'] = FaqCategory::all();
        return view('faq.base', $data);
    }

    public function update($id, Request $request) {
        $question = Faq::findOrFail($id);
        $rules = [
            'question' => ['required'],
            'category' => ['exists:faq_categories,id']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'saved'])]);
    }

    public function publishNew(Request $request) {
        $rules = [
            'question' => ['required'],
            'category' => ['required', 'exists:faq_categories,id'],
            'answer' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question = new Faq();
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        if (LoggedAccount::get()->isSuperAdmin()) {
            $question->status = 'published';
        } else {
            $question->status = 'pending';
        }
        $question->stuff_order = Faq::where('category_id', '=', $request->category)->max('stuff_order') + 1;
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'saved'])]);
    }

    public function publish($id, Request $request) {
        $question = Faq::findOrFail($id);
        $rules = [
            'question' => ['required'],
            'category' => ['required', 'exists:faq_categories,id'],
            'answer' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        if (LoggedAccount::get()->isSuperAdmin()) {
            $question->status = 'published';
        } else {
            $question->status = 'pending';
        }
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'saved', 'category' => $question->category_id])]);
    }

    public function reorder($id, Request $request) {
        $newSequence = $request->orderList;
        if (!is_array($newSequence)) {
            abort(400);
        }
        if (!LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $allCount = Faq::where('category_id', '=', $id)->where('status', '=', 'published')->count();
        $foundCount = Faq::where('category_id', '=', $id)->where('status', '=', 'published')->whereIn('id', $newSequence)->count();
        $newSequenceCount = count($newSequence);
        if ($newSequenceCount != $allCount || $newSequenceCount != $foundCount) {
            abort(400);
        }
        $order = 1;
        foreach ($newSequence as $id) {
            Faq::where('id', '=', $id)->update(['stuff_order' => $order++]);
        }
    }

    public function confirm($id, Request $request) {
        $question = Faq::findOrFail($id);
        if ($question->status != 'pending' && !LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $rules = [
            'question' => ['required'],
            'category' => ['required', 'exists:faq_categories,id'],
            'answer' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        $question->status = 'published';
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'pending'])]);
    }

    public function reject($id) {
        $faq = Faq::findOrFail($id);
        if ($question->status != 'pending' && !LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $faq->delete();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'pending'])]);
    }

    public function reviewPending($id) {
        $question = Faq::findOrFail($id);
        if ($question->status != 'pending' && !LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $data = [];
        $data['question'] = $question;
        $data['partialView'] = 'faq.form';
        $data['categories'] = FaqCategory::all();
        return view('faq.base', $data);
    }

    public function getModify($id) {
        $question = Faq::findOrFail($id);
        if ($question->modified || $question->status != 'published') {
            abort(404);
        }
        $data = [];
        $data['question'] = $question;
        $data['partialView'] = 'faq.form';
        $data['categories'] = FaqCategory::all();
        return view('faq.base', $data);
    }

    public function modify($id, Request $request) {
        $question = Faq::findOrFail($id);
        if ($question->status != 'published' || $question->modified) {
            abort(404);
        }
        $rules = [
            'question' => ['required'],
            'category' => ['required', 'exists:faq_categories,id'],
            'answer' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        if (LoggedAccount::get()->isSuperAdmin()) {
            $question->question = $request->question;
            $question->answer = $request->answer;
            $question->category_id = $request->category;
            $question->status = 'published';
        } else {
            $modified = new Faq();
            $modified->question = $request->question;
            $modified->answer = $request->answer;
            $modified->category_id = $request->category;
            $modified->status = 'modification';
            $modified->modification_to = $question->id;
            $question->modified = true;
            $modified->save();
        }
        $question->save();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'published'])]);
    }

    public function reviewModification($id) {
        $question = Faq::findOrFail($id);
        if (!$question->modified && LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $data = [];
        $data['question'] = $question;
        $data['partialView'] = 'faq.modification_form';
        $data['categories'] = FaqCategory::all();
        return view('faq.base', $data);
    }

    public function confirmModification($id, Request $request) {
        $question = Faq::findOrFail($id);
        if (!$question->modified && LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $rules = [
            'question' => ['required'],
            'category' => ['required', 'exists:faq_categories,id'],
            'answer' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $question->question = $request->question;
        $question->answer = $request->answer;
        $question->category_id = $request->category;
        $question->status = 'published';
        $question->modified = false;
        $question->save();
        $question->modification()->delete();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'modification'])]);
    }

    public function rejectModification($id) {
        $question = Faq::findOrFail($id);
        if (!$question->modified && LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        $question->modified = false;
        $question->save();
        $question->modification()->delete();
        return response()->json(['url' => route('admin.faq.index', ['status' => 'modification'])]);
    }

}
