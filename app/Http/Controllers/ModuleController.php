<?php

namespace App\Http\Controllers;

use App\Repositories\Repository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Route;

class ModuleController extends Controller {

    private $repo;
    private $request;

    public function __construct(Repository $repo, Request $request) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public function index() {
        $items = $this->repo->getAll();
        $baseView = current_module()->getViewDirName() . '.base';
        $partialView = current_module()->getViewDirName() . '.list';
        return view($baseView, compact('items', 'partialView'));
    }

    public function create() {
        return $this->renderForm(false, $this->request);
    }

    public function edit($id) {
        $item = $this->repo->find($id);
        return $this->renderForm($this->repo->find($id), $this->request);
    }

    public function store(FormRequest $request) {
        if ($response = current_module()->validator()->inValid($this->request->all())) {
            return response()->json($response);
        }

        return response()->json($this->repo->create($request->input()));
    }

    public function update(FormRequest $request, $id) {
        if ($response = current_module()->inValid($this->request->all())) {
            return response()->json($response);
        }
        return response()->json($this->repo->update($request->input(), $id));
    }

    public function delete($id) {
        return response()->json($this->repo->delete($id));
    }

    public function deleteMultiple() {
        return response()->json($this->repo->deleteMultiple($this->request->input('items')));
    }

    public function reOrder() {
        return response()->json($this->repo->reOrder($this->request->input('orderList')));
    }

    public function approve($id) {
        return response()->json($this->repo->approve($this->request->input('id'), Auth::user()->id));
    }

    private function renderForm($item, $request) {
        $formData = $this->repo->getFormData();
        $baseView = current_module()->getViewDirName() . '.base';
        $partialView = current_module()->getViewDirName() . '.form';
        return view($baseView, compact('item', 'formData', 'partialView'));
    }

}
