<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\quiz;

use App\Question;

use App\File;

use App\Answer;

class quizController extends Controller
{
   public function index() {
        $data = [];
        $data['partialView'] = 'quizzes.list';
        $data['quizzes'] = quiz::orderBy('created_at', 'desc')->get();
        return view('quizzes.base', $data);
    }

    public function init() {
        $quiz = new quiz();
        $quiz->save();
        return response(['url' => route('admin.quizzes.edit', ['id' => $quiz->id])]);
    }

    public function edit($id) {
        $quiz = quiz::findOrFail($id);
        $data = [];
        $data['partialView'] = 'quizzes.form';
        $data['quiz'] = $quiz;
        $questions = Question::where('quiz_id',$quiz->id)->orderBy('question_order','ASC')->get();
        $data['questions'] = $questions ? json_decode($questions, 1) : [];
        return view('quizzes.base', $data);
    }
    public function addQuestion($quiz_id){
        $question = new Question();
        $question->quiz_id = $quiz_id;
        $question->save();
        return $question->id;
    }
    public function addAnswer($question_id){
        $answer = new Answer();
        $answer->question_id = $question_id;
        $answer->save();
        return $answer->id;
    }

    public function uploadQuestionFile($id, Request $request) {
        $question = Question::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/quizzes/question_file');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $question->file = $file->id;
        $question->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.quizzes.delete_question_file', ['id' => $question->id]);
        $responseData['download_action'] = route('admin.quizzes.question_file', ['id' => $question]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }
    public function initQuestion($id, Request $request) {
        $quiz = quiz::findOrFail($id);
        $question = new question();
        $question->question_order = $quiz->getQuestions()->max('question_order') + 1;
        $quiz->getQuestions()->save($question);
        return response()->json(['id' => $question->id]);
    }

    public function uploadCourseImage($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/course_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->course_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_section_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.section_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadPromo($id, Request $request) {
        $course = BlendedCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended/promo');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->promo_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended.delete_promo', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.blended.promo', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function questionFile($id) {
        $question = Question::findOrFail($id);
        if ($question->questionFile()) {
            return response()->download($question->questionFile()->path, $question->questionFile()->name);
        }
        abort(404);
    }

    public function courseImage($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->courseImage()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function promo($id) {
        $course = BlendedCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->promo()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function deleteQuestionFile($id) {
        Question::findOrFail($id)->deleteQuestionFile();
    }

    public function deleteCourseImage($id) {
        BlendedCourse::findOrFail($id)->deleteCourseImage();
    }

    public function deletePromo($id) {
        BlendedCourse::findOrFail($id)->deletePromo();
    }

    public function update(Request $request, $id) {
        $quiz = quiz::findOrFail($id);
        $questions = $quiz->getQuestions;
        foreach ($questions as $question) {
            
        }
        $ids = array();
        if (is_array($request->questions)) {
            $count = count($request->questions);

            for ($i =0; $i<$count;$i++) {
                $check_question = Question::where('id',$request->questions[$i]['id'])->first();
                array_push($ids, $request->questions[$i]['id']);
                if($check_question){
                    $check_question->content = $request->questions[$i]['content'];
                    $check_question->weight = $request->questions[$i]['weight'];
                    $check_question->question_order = $i;
                    $check_question->save();
                }else{
                    $question = new Question();
                    $question->quiz_id = $id;
                    $question->content = $request->questions[$i]['content'];
                    $question->weight = $request->questions[$i]['weight'];
                    $question->question_order = $i;
                    $question->save();
                     array_push($ids, $question->id);
                }
                
            }
        }
        Question::whereNotIn('id',$ids)->where('quiz_id',$id)->delete();
        
        $quiz->title = $request->title;
        $quiz->weight = $request->weight;
        $quiz->save();
        $responseData = [];
        $responseData['url'] = route('admin.quizzes.index');
        return response()->json($responseData);
    }

    public function delete($id) {
        BlendedCourse::findOrFail($id)->delete();
    }
    // Answers ---------------------------------------------------------

    public function answers($question_id) {
        $answers = Answer::where('question_id',$question_id)->get();
        $data = [];
        $data['partialView'] = '';
        $data['answers'] = $answers;
        $data['question_id'] = $question_id;
        return view('quizzes.answers', $data);
    }

    public function uploadAnswerFile($id, Request $request) {
        $answer = Answer::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/quizzes/answer_file');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $answer->file = $file->id;
        $answer->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.quizzes.delete_answer_file', ['id' => $answer->id]);
        $responseData['download_action'] = route('admin.quizzes.answer_file', ['id' => $answer]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function answerFile($id) {
        $answer = Answer::findOrFail($id);
        if ($answer->answerFile()) {
            return response()->download($answer->answerFile()->path, $answer->answerFile()->name);
        }
        abort(404);
    }

    public function deleteAnswerFile($id) {
        Answer::findOrFail($id)->deleteAnswerFile();
    }

    public function updateAnswers(Request $request, $id) {
        $question = Question::findOrFail($id);
        $answers = $question->getAnswers;
        
        $ids = array();
        if (is_array($request->answers)) {
            $count = count($request->answers);

            for ($i =0; $i<$count;$i++) {
                $answer = Answer::where('id',$request->answers[$i]['id'])->first();
                array_push($ids, $request->answers[$i]['id']);
                if($answer){
                    $answer->content = $request->answers[$i]['content'];
                    $answer->correct = $request->answers[$i]['correct'];
                    $answer->stuff_order = $i;
                    $answer->save();
                }else{
                    $answer = new Question();
                    $answer->question_id = $id;
                    $answer->content = $request->answers[$i]['content'];
                    $answer->correct = $request->answers[$i]['correct'];
                    $answer->stuff_order = $i;
                    $answer->save();
                     array_push($ids, $answer->id);
                }
                if($answer->correct == 1){
                   $question->correct_choice_id = $answer->id; 
                   $question->save();
                }
                
            }
        }
        Answer::whereNotIn('id',$ids)->where('question_id',$id)->delete();
        
        
        $responseData = [];
        //$responseData['url'] = route('admin.quizzes.answers');
        return response()->json($responseData);
    }

    


}
