<?php

namespace App\Http\Controllers\students;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use App\User;
use App\Account;
use App\Student;

class studentController extends Controller
{
    public function getIndex()
    {
        $model_name = 'User';
        return view('students.allStudents',compact('model_name'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllStudents(Request $request)
    {
        $users = User::
        	join('accounts', 'accounts.user_id', '=', 'users.id')
        	->join('students' , 'students.user_id' ,'=', 'users.id')
        	->where('accounts.type','student')
            ->select(['users.*','students.user_id','accounts.name' , 'users.id as group'])
            ->groupBy('group');

        return Datatables::of($users)
        /*->addColumn('Test', function ($user) {
                return '<a href="/admin/backend/'.$user->user_id.'/results" target="_blank" ><button type="button" class="btn green-seagreen">Test</button></a>';
            })
        ->addColumn('Checked', function ($user) {
            $test = Test::where('user_id',$user->user_id)->first();
            if($test){
                if($test->checked ==1){
                    return '<input class="update-checked" data-user="'.$user->user_id.'" type="checkbox" checked>';
                }else{
                    return '<input class="update-checked" data-user="'.$user->user_id.'" type="checkbox">';
                }
            }else{
                return '';
            }
            })
        ->addColumn('Checkbox', function ($user) {
                return '<input name="users[]" class="checkboxes" value="'.$user->user_id.'" type="checkbox">';
            })
        ->addColumn('Details', function ($user) {
                return '<a href="/admin/backend/'.$user->user_id.'/details" target="_blank" ><button type="button" class="btn green-seagreen">Details</button></a>';
            })
        ->filter(function ($query) use ($request) {
            if ($request->has('name')) {
                $name = trim($request->get('name'));
                $query->where('name', 'like', "%{$name}%");
            }

            if ($request->has('email')) {
                $query->where('email', 'like', "%{$request->get('email')}%");
            }

            if ($request->has('date_start')) {
                $query->where('start_time', '>', "{$request->get('date_start')}");
            }
            if ($request->has('date_end')) {
                $query->Where('end_time', '<', "{$request->get('date_end')}");
            }
            if ($request->has('date_from')) {
                $query->where('users.created_at', '>', "{$request->get('date_from')}");
            }
            if ($request->has('date_to')) {
                $query->Where('users.created_at', '<', "{$request->get('date_to')}");
            }
            if ($request->has('score_from')) {
                $query->Where('test_score', '>=', "{$request->get('score_from')}");
            }
            if ($request->has('score_to')) {
                $query->Where('test_score', '<=', "{$request->get('score_to')}");
            }
            if ($request->has('checked') && $request->get('checked')!=2 ) {
                $query->Where('tests.checked', '=', "{$request->get('checked')}");
            }
            
            

            
        })*/
        ->setRowID(function ($user) {
                return "data-row-".$user->id ;
        })
        ->make(true);
        
            
    }
}
