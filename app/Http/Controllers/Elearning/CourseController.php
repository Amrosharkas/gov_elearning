<?php

namespace App\Http\Controllers\Elearning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ElearningCourse;
use App\ElearningModule;
use App\ElearningLesson;
use App\ElearningTopic;

class CourseController extends Controller {

    public function edit($id) {
        $course = ElearningCourse::findOrFail($id);
        $data = [];
        $data['partialView'] = 'elearning.form';
        $data['course'] = $course;
        $data['modules'] = $course->modules()->whereNull('stuff_order')->get();
        $data['lessons'] = $course->lessons()->whereNull('module_id')->get();
        $data['topics'] = $course->topics()->whereNull('lesson_id')->get();
        $data['map'] = $course->modules()->with('lessons.topics')->get();
        $data['objectives'] = $course->objectives ? json_decode($course->objectives, 1) : [];
        return view('elearning.base', $data);
    }

    public function uploadSectionImage($id, Request $request) {
        $course = ElearningCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/elearning/section_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->section_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.elearning.course.delete_section_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.elearning.course.section_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadCourseImage($id, Request $request) {
        $course = ElearningCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/elearning/course_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->course_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.elearning.course.delete_course_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.elearning.course.course_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadPromo($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/elearning/promo');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->promo_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.elearning.course.delete_promo', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.elearning.course.promo', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function sectionImage($id) {
        $course = ElearningCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->sectionImage()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function courseImage($id) {
        $course = ElearningCourse::findOrFail($id);
        if ($course->courseImage()) {
            return response()->download($course->courseImage()->path, $course->courseImage()->name);
        }
        abort(404);
    }

    public function promo($id) {
        $course = Elearning::findOrFail($id);
        if ($course->promo()) {
            return response()->download($course->promo()->path, $course->promo()->name);
        }
        abort(404);
    }

    public function deleteSectionImage($id) {
        ElearningCourse::findOrFail($id)->deleteSectionImage();
    }

    public function deleteCourseImage($id) {
        ElearningCourse::findOrFail($id)->deleteCourseImage();
    }

    public function deletePromo($id) {
        ElearningCourse::findOrFail($id)->deletePromo();
    }

    public function update($id, Request $request) {
//        if ($errors = $this->validateForUpdate()) {
//            return response()->json(compact('errors'));
//        }
        $course = ElearningCourse::findOrFail($id);
        if (!is_array($request->lists) || !is_array($request->map)) {
            abort(400);
        }
        $hasErrors = false;
        $errors = [];
        $errors['units'] = [];
        if (!isset($request->lists['modules']) && !is_array($request->lists['modules'])) {
            
        }
        $result = $this->validateUnitsForUpdate();
        if ($result['hasErrors']) {
            $hasErrors = true;
            $errors['units'] = $result['errors'];
        }
        // Saving Data
        if (isset($request->lists['modules'])) {
            foreach ($request->lists['modules'] as $mdl) {
                $module = $course->modules->where('id', (int) $mdl['id'])->first();
                if ($module) {
                    $module->title = $mdl['title'];
                    $module->save();
                }
            }
        }
        if (isset($request->lists['lessons'])) {
            foreach ($request->lists['lessons'] as $lsn) {
                $lesson = $course->lessons->where('id', (int) $lsn['id'])->first();
                if ($lesson) {
                    $lesson->title = $lsn['title'];
                    $lesson->save();
                }
            }
        }
        if (isset($request->lists['topics'])) {
            foreach ($request->lists['topics'] as $tpc) {
                $topic = $course->topics->where('id', (int) $tpc['id'])->first();
                if ($topic) {
                    $topic->title = $tpc['title'];
                    $topic->save();
                }
            }
        }
        if (isset($request->map['modules'])) {
            foreach ($request->map['modules'] as $mdl) {
                $module = $course->modules->where('id', (int) $mdl['id'])->first();
                if ($module) {
                    $module->title = $mdl['title'];
                    $module->stuff_order = $mdl['stuff_order'];
                    $module->save();
                }
            }
        }
        if (isset($request->map['lessons'])) {
            foreach ($request->map['lessons'] as $lsn) {
                $lesson = $course->lessons->where('id', (int) $lsn['id'])->first();
                if ($lesson && $course->modules->where('id', (int) $lsn['module'])->first()) {
                    $lesson->title = $lsn['title'];
                    $lesson->stuff_order = $lsn['stuff_order'];
                    $lesson->module_id = $lsn['module'];
                    $lesson->save();
                }
            }
        }
        if (isset($request->map['topics'])) {
            foreach ($request->map['topics'] as $tpc) {
                $topic = $course->topics->where('id', (int) $tpc['id'])->first();
                if ($topic && $course->lessons->where('id', (int) $tpc['lesson'])->first()) {
                    $topic->title = $tpc['title'];
                    $topic->stuff_order = $tpc['stuff_order'];
                    $topic->lesson_id = $tpc['lesson'];
                    $topic->save();
                }
            }
        }
    }

    protected function validateMainForUpdate($data) {
        $rules = [
            'section' => ['integer', 'exists:sections,id'],
            'title' => ['required', 'string'],
            'description_overall' => ['string'],
            'description_section' => ['string'],
            'description_course' => ['string'],
            'estimated_time' => ['integer'],
            'dependent_course' => ['integer', 'exists:elearning_courses,id']
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return $v->errors()->messages();
        }
    }

    protected function validateMainForPublish($data) {
        $rules = [
            'section' => ['required', 'integer', 'exists:sections,id'],
            'title' => ['required', 'string'],
            'description_overall' => ['required', 'string'],
            'description_section' => ['required', 'string'],
            'description_course' => ['required', 'string'],
            'estimated_time' => ['required', 'integer'],
            'dependent_course' => ['required', 'integer', 'exists:elearning_courses,id']
        ];
        $v = Validator::make($data, $rules);
        if ($v->fails()) {
            return $v->errors()->messages();
        }
    }

    protected function validateUnitsForUpdate($lists) {
        $errors = [];
        $hasErrors = false;
        $rules = [
            'title' => 'string'
        ];
        $errors['modules'] = [];
        if (isset($lists['modules']) && is_array($lists['modules'])) {
            foreach ($lists['modules'] as $module) {
                $v = Validator::make($module, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['modules'][$module['id']] = $v->errors()->messages();
                }
            }
        }
        $errors['lessons'] = [];
        if (isset($lists['lessons']) && is_array($lists['lessons'])) {
            foreach ($lists['lessons'] as $lesson) {
                $v = Validator::make($lesson, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['lessons'][$lessons['id']] = $v->errors()->messages();
                }
            }
        }
        $errors['topics'] = [];
        if (isset($lists['topics']) && is_array($lists['topics'])) {
            foreach ($lists['topics'] as $topic) {
                $v = Validator::make($topic, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['topics'][$topic['id']] = $v->errors()->messages();
                }
            }
        }
        return compact('hasErrors', 'errors');
    }

    protected function validateUnitsForPublish($lists) {
        $errors = [];
        $hasErrors = false;
        $rules = [
            'title' => 'required,string'
        ];
        $errors['modules'] = [];
        if (isset($lists['modules']) && is_array($lists['modules'])) {
            foreach ($lists['modules'] as $module) {
                $v = Validator::make($module, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['modules'][$module['id']] = $v->errors()->messages();
                }
            }
        }
        $errors['lessons'] = [];
        if (isset($lists['lessons']) && is_array($lists['lessons'])) {
            foreach ($lists['lessons'] as $lesson) {
                $v = Validator::make($lesson, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['lessons'][$lessons['id']] = $v->errors()->messages();
                }
            }
        }
        $errors['topics'] = [];
        if (isset($lists['topics']) && is_array($lists['topics'])) {
            foreach ($lists['topics'] as $topic) {
                $v = Validator::make($topic, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['topics'][$topic['id']] = $v->errors()->messages();
                }
            }
        }
        return compact('hasErrors', 'errors');
    }

    protected function updateMap() {
        
    }

}
