<?php

namespace App\Http\Controllers\Elearning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use ZipArchive;
use App\ElearningCourse;
use App\ElearningModule;

class ModuleController extends Controller {

    public function init($course_id) {
        $course = ElearningCourse::findOrFail($course_id);
        $module = ElearningModule::create();
        $course->modules()->save($module);
        $data = [];
        $data['id'] = $module->id;
        $data['remove_action'] = route('admin.elearning.module.delete', ['id' => $module->id]);
        return response()->json($data);
    }

    public function delete($id) {
        ElearningModule::destroy($id) or abort(404);
    }

    public function uploadContent($id, Request $request) {
        $rules = [
            'file' => ['file', 'mimetypes:application/zip']
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return response()->json(['error' => 'Invalid file type']);
        }
        $uploadedFile = $request->file;
        $dirPath = public_path('courses/elearning/module/content/' . $id);
        if (file_exists($dirPath)) {
            $this->rrmdir($dirPath);
        }
        mkdir($dirPath);
        $uploadedFile->move($dirPath, 'material.zip');
        $zip = new ZipArchive;
        if ($zip->open($dirPath . '/material.zip') === TRUE) {
            $zip->extractTo($dirPath);
            $zip->close();
            unlink($dirPath . '/material.zip');
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function uploadMaterial(Request $request) {
        
    }

    protected function rrmdir($path) {
        $dir = opendir($path);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $path . '/' . $file;
                if (is_dir($full)) {
                    $this->rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($path);
    }

}
