<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OneCourse;
use App\OneLevel;
use App\LearningObjective;
use App\OneSessionPricing;
use App\Shift;
use App\Section;
use App\File;
use Validator;

class OneOnOneController extends Controller {

    protected $pricingRules = [
        'count' => ['integer'],
        'basic_basic' => ['integer'],
        'basic_premium' => ['integer'],
        'premium_basic' => ['integer'],
        'premium_premium' => ['integer']
    ];

    public function index(Request $request) {
        $data = [];
        $data['partialView'] = 'one_on_one.list';
        $data['courses'] = OneCourse::with('section')->where('saved', '=', true)->orderBy('created_at', 'desc')->get();
        return view('one_on_one.base', $data);
    }

    public function init() {
        $course = new OneCourse();
        $course->save();
        return response()->json([
                    'url' => route('admin.one_on_one.edit', ['id' => $course->id])
        ]);
    }

    public function edit($id) {
        $course = OneCourse::findOrFail($id);
//        if (!$course->saved) {
//            abort(404);
//        }
        $data = [];
        $data['partialView'] = 'one_on_one.form';
        $data['course'] = $course;
        $data['shifts'] = Shift::all();
        $data['sections'] = Section::all();
        $data['levels'] = $course->levels()->where('saved', '=', true)->orderBy('stuff_order')->get();
        $data['pricings'] = $course->pricings()->where('saved', '=', true)->orderBy('created_at')->get();
//        $data['objectives'] = $course->objectives()->orderBy('objective_order')->get();
        $data['objectives'] = $course->objectives ? json_decode($course->objectives, 1) : [];
        return view('one_on_one.base', $data);
    }

    public function initLevel($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $level = new OneLevel();
        $level->stuff_order = $course->levels()->max('stuff_order') + 1;
        $course->levels()->save($level);
        $responseData = [];
        $responseData['id'] = $level->id;
        $responseData['upload_ig_action'] = route('admin.one_on_one.upload_ig', ['id' => $level->id]);
        $responseData['upload_material_action'] = route('admin.one_on_one.upload_material', ['id' => $level->id]);
        $responseData['remove_level_action'] = route('admin.one_on_one.delete_level', ['id' => $level->id]);
        return response()->json($responseData);
    }

    public function initObjective($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $objective = new LearningObjective();
        $objective->objective_order = $course->objectives()->max('objective_order') + 1;
        $course->objectives()->save($objective);
        return response()->json(['id' => $objective->id]);
    }

    public function deleteLevel($id) {
        OneLevel::destroy($id) or abort(404);
    }

    public function initPricing($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $pricing = new OneSessionPricing();
        $course->pricings()->save($pricing);
        $data = [];
        $data['id'] = $pricing->id;
        $data['remove_pricing_action'] = route('admin.one_on_one.delete_pricing', ['id' => $pricing->id]);
        return response()->json($data);
    }

    public function deletePricing($id) {
        OneSessionPricing::destroy($id) or abort(404);
    }

//    public function deleteObjective(Request $request) {
//        $status = LearningObjective::findOrFail($request->id)->delete();
//        return response()->json(['status' => $status]);
//    }

    public function uploadIg($id, Request $request) {
        $level = OneLevel::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/one_on_one/ig');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $level->ig_id = $file->id;
        $level->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.one_on_one.delete_ig', ['id' => $level->id]);
        $responseData['download_action'] = route('admin.one_on_one.ig', ['id' => $level]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadMaterial($id, Request $request) {
        $level = OneLevel::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/one_on_one/material');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $level->material_id = $file->id;
        $level->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.one_on_one.delete_material', ['id' => $level->id]);
        $responseData['download_action'] = route('admin.one_on_one.material', ['id' => $level]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadSectionImage($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/one_on_one/section_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->section_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.one_on_one.delete_section_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.one_on_one.section_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadCourseImage($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/one_on_one/course_image');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->course_image_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.one_on_one.delete_course_image', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.one_on_one.course_image', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function uploadPromo($id, Request $request) {
        $course = OneCourse::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/one_on_one/promo');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $course->promo_id = $file->id;
        $course->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.one_on_one.delete_promo', ['id' => $course->id]);
        $responseData['download_action'] = route('admin.one_on_one.promo', ['id' => $course]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function sectionImage($id) {
        $course = OneCourse::findOrFail($id);
        if ($course->sectionImage()) {
            return response()->download($course->sectionImage()->path, $course->sectionImage()->name);
        }
        abort(404);
    }

    public function courseImage($id) {
        $course = OneCourse::findOrFail($id);
        if ($course->courseImage()) {
            return response()->download($course->courseImage()->path, $course->courseImage()->name);
        }
        abort(404);
    }

    public function promo($id) {
        $course = OneCourse::findOrFail($id);
        if ($course->promo()) {
            return response()->download($course->promo()->path, $course->promo()->name);
        }
        abort(404);
    }

    public function deleteSectionImage($id) {
        OneCourse::findOrFail($id)->deleteSectionImage();
    }

    public function deleteCourseImage($id) {
        OneCourse::findOrFail($id)->deleteCourseImage();
    }

    public function deletePromo($id) {
        OneCourse::findOrFail($id)->deletePromo();
    }

    public function ig($id) {
        $level = OneLevel::findOrFail($id);
        if ($level->ig()) {
            return response()->download($level->ig()->path, $level->ig()->name);
        }
        abort(404);
    }

    public function material($id) {
        $level = OneLevel::findOrFail($id);
        if ($level->material()) {
            return response()->download($level->material()->path, $level->material()->name);
        }
        abort(404);
    }

    public function deleteIg($id) {
        OneLevel::findOrFail($id)->deleteIg();
    }

    public function deleteMaterial($id) {
        OneLevel::findOrFail($id)->deleteMaterial();
    }

    public function publish(Request $request, $id) {
        $course = OneCourse::findOrFail($id);
        // Validation
        $errors = [];
        $hasErrors = false;
        $rules = [
            'max_start_date' => ['required', 'integer'],
            'section' => ['required', 'integer', 'exists:sections,id'],
            'session_duration' => ['required', 'integer'],
            'has_vc' => ['boolean'],
            'max_frequency' => ['required', 'integer', 'min:1', 'max:7'],
            'description_overall' => ['required'],
            'description_section' => ['required'],
            'description_course' => ['required'],
            'course_title' => ['required'],
        ];
        $attributes = [
            'course_title' => 'title'
        ];
        $errors['main'] = [];
        $v = Validator::make($request->all(), $rules, [], $attributes);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        // rules and messages for levels
        $rules = [
            'levels' => ['required', 'array', 'min:1'],
            'shifts' => ['required', 'array', 'min:1'],
            'objectives' => ['array'],
            'pricings' => ['required', 'array', 'min:1'],
        ];
        $messages = [
            'levels.*' => 'You have to enter course levels',
            'shifts.*' => 'You have to enter course shifts',
            'pricings.*' => 'You have to enter course pricings',
        ];
        $errors['arrays'] = [];
        $v = Validator::make($request->all(), $rules, $messages);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['arrays'] = $v->errors()->messages();
        }
        $levels = $request->levels;
        $errors['levels'] = [];
        if (is_array($levels)) {
            foreach ($levels as $level) {
                if (!trim($level['title'])) {
                    $hasErrors = true;
                    $errors['levels'][$level['id']] = "The title field is required";
                }
            }
        }
        $objectives = $request->objectives;
        $errors['objectives'] = [];
        if (is_array($objectives)) {
            $this->validateObjectives($objectives, $errors['objectives']);
            if (count($errors['objectives'])) {
                $hasErrors = true;
            }
        }
        $pricings = $request->pricings;
        $errors['pricings'] = [];
        if (is_array($pricings)) {
            $rules = [
                'count' => ['required', 'integer'],
                'basic_basic' => ['required', 'integer'],
                'basic_premium' => ['required', 'integer'],
                'premium_basic' => ['required', 'integer'],
                'premium_premium' => ['required', 'integer']
            ];
            $attributes = [
                'basic_basic' => 'Basic teacher - Basic timing',
                'basic_premium' => 'Basic teacher - Premium timing',
                'premium_premium' => 'Premium teacher - Premium timing',
                'premium_basic' => 'Premium teacher - Basic timing',
            ];
            foreach ($pricings as $pricing) {
                foreach ($pricing as $key => $value) {
                    $v = Validator::make($pricing, $rules, [], $attributes);
                    if ($v->fails()) {
                        $hasErrors = true;
                        $errors['pricings'][$pricing['id']] = $v->errors()->messages();
                    }
                }
            }
        }

        if ($hasErrors) {
            return response()->json(['errors' => $errors]);
        }
        //End Validation
        $shifts = $request->shifts;
        if (is_array($shifts)) {
            $course->shifts()->detach();
            $course->shifts()->attach($shifts);
        }
        if (is_array($levels)) {
            $order = 1;
            foreach ($levels as $level) {
                $update = [];
                $update['title'] = $level['title'];
                $update['stuff_order'] = $order++;
                $update['saved'] = true;
                OneLevel::where('id', '=', $level['id'])->update($update);
            }
        }
        if (is_array($pricings)) {
            $errors['pricings'] = [];
            foreach ($pricings as $pricing) {
                $id = $pricing['id'];
                unset($pricing['id']);
                $pricing['saved'] = true;
                OneSessionPricing::where('id', '=', $id)->update($pricing);
            }
        }
        $course->description_overall = $request->description_overall;
        $course->description_section = $request->description_section;
        $course->description_course = $request->description_course;
        $course->section_id = $request->section;
        $course->title = $request->course_title;
        $course->max_frequency = $request->max_frequency;
        $course->session_duration = $request->session_duration;
        $course->max_start_date = $request->max_start_date;
        $course->has_vc = $request->has_vc;
        $course->saved = true;
        $course->published = true;
        if (is_array($objectives)) {
            $objectives = json_encode($request->objectives);
        }
        $course->save();
        return response()->json(['url' => route('admin.one_on_one.index')]);
    }

    public function update(Request $request, $id) {
        $course = OneCourse::findOrFail($id);
        // Validation
        $errors = [];
        $hasErrors = false;
        $rules = [
            'max_start_date' => ['integer'],
            'section' => ['integer', 'exists:sections,id'],
            'session_duration' => ['integer'],
            'has_vc' => ['boolean'],
            'max_frequency' => ['integer', 'min:1', 'max:7'],
            'course_title' => ['required']
        ];
        $attributes = [
            'course_title' => 'title'
        ];
        $errors['main'] = [];
        $v = Validator::make($request->all(), $rules,[],$attributes);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        $rules = [
            'levels' => ['array', 'min:1'],
            'shifts' => [ 'array', 'min:1'],
            'objectives' => ['array'],
            'pricings' => ['array', 'min:1'],
        ];

        $errors['arrays'] = [];

        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            $hasErrors = true;
            $errors['arrays'] = $v->errors()->messages();
        }

        $levels = $request->levels;
        $errors['levels'] = [];
        $objectives = $request->objectives;
        $errors['objectives'] = [];
        $pricings = $request->pricings;
        $errors['pricings'] = [];
        if (is_array($pricings)) {
            $rules = [
                'count' => ['integer'],
                'basic_basic' => ['integer'],
                'basic_premium' => ['integer'],
                'premium_basic' => ['integer'],
                'premium_premium' => ['integer']
            ];
            $attributes = [
                'basic_basic' => 'Basic teacher - Basic timing',
                'basic_premium' => 'Basic teacher - Premium timing',
                'premium_premium' => 'Premium teacher - Premium timing',
                'premium_basic' => 'Premium teacher - Basic timing',
            ];
            foreach ($pricings as $pricing) {
                foreach ($pricing as $key => $value) {
                    $v = Validator::make($pricing, $rules, [], $attributes);
                    if ($v->fails()) {
                        $hasErrors = true;
                        $errors['pricings'][$pricing['id']] = $v->errors()->messages();
                    }
                }
            }
        }

        if ($hasErrors) {
            return response()->json(['errors' => $errors]);
        }
        //End Validation
        $shifts = $request->shifts;
        if (is_array($shifts)) {
            $course->shifts()->detach();
            $course->shifts()->attach($shifts);
        }
        if (is_array($levels)) {
            $order = 1;
            foreach ($levels as $level) {
                $update = [];
                $update['title'] = $level['title'];
                $update['stuff_order'] = $order++;
                $update['saved'] = true;
                OneLevel::where('id', '=', $level['id'])->update($update);
            }
        }
        if (is_array($pricings)) {
            $errors['pricings'] = [];
            foreach ($pricings as $pricing) {
                $id = $pricing['id'];
                unset($pricing['id']);
                $pricing['saved'] = true;
                OneSessionPricing::where('id', '=', $id)->update($pricing);
            }
        }
        $course->description_overall = $request->description_overall;
        $course->description_section = $request->description_section;
        $course->description_course = $request->description_course;
        $course->section_id = $request->section;
        $course->title = $request->course_title;
        $course->max_frequency = $request->max_frequency;
        $course->session_duration = $request->session_duration;
        $course->max_start_date = $request->max_start_date;
        $course->has_vc = $request->has_vc;
        $course->saved = true;
        if (is_array($objectives)) {
            $objectives = json_encode($request->objectives);
        }
        $course->save();
        return response()->json(['url' => route('admin.one_on_one.index')]);
    }

    public function delete($id) {
        OneCourse::findOrFail($id)->delete();
    }

    protected function validateObjectives($objectives, &$errors) {
        foreach ($objectives as $objective) {
            if (!trim($objective['content'])) {
                $errors[$objective['id']] = true;
            }
            if (isset($objective['children']) && is_array($objective['children'])) {
                $this->validateObjectives($objective['children'], $errors);
            }
        }
    }

}
