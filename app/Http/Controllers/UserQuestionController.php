<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserQuestion;
use App\Faq;
use LoggedAccount;

class UserQuestionController extends Controller {

    public function index($status) {
        $data = [];
        if (!in_array($status, ['new', 'reported', 'answered'])) {
            abort(404);
        }
        if (!LoggedAccount::get()->isSuperAdmin() && $status == 'reported') {
            abort(404);
        }
        $data['questions'] = UserQuestion::where('status', '=', $status)->orderBy('updated_at')->get();
        $data['partialView'] = 'user_question.list';
        $data['status'] = $status;
        return view('user_question.base', $data);
    }

    public function answer($id) {
        $question = UserQuestion::findOrFail($id);
        if ($question->status == 'answered') {
            abort(404);
        }
        $data['question'] = $question;
        $data['partialView'] = 'user_question.form';
        return view('user_question.base', $data);
    }

    public function send($id, Request $request) {
        $question = UserQuestion::findOrFail($id);
        if ($question->status == 'answered') {
            abort(404);
        }
        if (!trim($request->answer)) {
            return response()->json(['errors' => ['answer' => ['Answer must be filled']]]);
        }
        $question->answer = $request->answer;
        $question->status = 'answered';
        $question->save();
        return response()->json(['url' => route('admin.user_question.index', ['status' => 'new'])]);
    }

    public function view($id) {
        $question = UserQuestion::findOrFail($id);
        $data['question'] = $question;
        $data['partialView'] = 'user_question.view';
        return view('user_question.base', $data);
    }

    public function copy($id, Request $request) {
        $question = UserQuestion::findOrFail($id);
        if ($question->copied) {
            abort(404);
        }
        $faq = new Faq();
        $faq->question = $question->question;
        if ($question->answer) {
            $faq->answer = $question->answer;
        } else {
            $faq->answer = $request->answer;
        }
        $question->copied = true;
        $question->save();
        $faq->save();
    }

    public function report($id) {
        $question = UserQuestion::findOrFail($id);
        if ($question->status != 'new' || !LoggedAccount::get()->isCRM()) {
            abort(404);
        }
        $question->status = 'reported';
        $question->save();
        return response()->json(['url' => route('admin.user_question.index', ['status' => 'new'])]);
    }

}
