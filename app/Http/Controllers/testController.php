<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Test;

class testController extends Controller
{
    public function index(Request $request) {
        $data = [];
        $data['partialView'] = 'test.list';
        $data['courses'] = OneCourse::query()->orderBy('created_at', 'desc')->get();
        return view('one_on_one.base', $data);
    }
}
