<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OneSchedule;
use App\Account;
use App\Shift;
use App\OneCourse;
use App\OneScheduleDaytime;
use DB;
use Validator;

class OneScheduleController extends Controller {

    protected $weekdays = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

    public function index() {
        $data = [];
        $data['partialView'] = 'one_schedule.list';
        $data['schedules'] = OneSchedule::with('course')->orderBy('created_at', 'desc')->get();
        return view('one_schedule.base', $data);
    }

    public function init() {
        $schedule = OneSchedule::create();
        return response(['url' => route('admin.one_schedule.edit', ['id' => $schedule->id])]);
    }

    public function edit($id) {
        $schedule = OneSchedule::findOrFail($id);
        $data = [];
        $data['partialView'] = 'one_schedule.form';
        $data['schedule'] = $schedule;
        $data['courses'] = OneCourse::all();
        $data['teachers'] = Account::with('user')->where('type', '=', 'teacher')->get();
        $data['students'] = Account::with('user')->where('type', '=', 'student')->get();
        $data['shifts'] = Shift::whereNotNull('name')->get();
        $data['weekdays'] = $this->weekdays;
        return view('one_schedule.base', $data);
    }

    public function students(Request $request) {
        return Account::where('accounts.type', '=', 'student')
                        ->select('accounts.name', 'accounts.user_id', DB::raw('accounts.id as acc_id'))
                        ->search($request->get('q'))->with('user')
                        ->get();
    }

    public function teachers(Request $request) {
        return Account::where('accounts.type', '=', 'teacher')
                        ->select('accounts.name', 'accounts.user_id', DB::raw('accounts.id as acc_id'))
                        ->search($request->get('q'))->with('user')
                        ->get();
    }

    public function update($id, Request $request) {
        $schedule = OneSchedule::findOrFail($id);
        /** VALIDACTION */
        $errors = [];
        $hasErrors = false;
        $rules = [
            'frequency' => ['integer', 'min:1', 'max:7'],
            'start_date' => ['date'],
            'course' => ['integer', 'exists:one_courses,id'],
            'n_sessions' => ['integer'],
            'n_sessions_price' => ['integer'],
            'shift' => ['integer', 'exists:shifts,id'],
            'teacher' => ['integer', 'exists:accounts,id,type,teacher'],
            'student' => ['integer', 'exists:accounts,id,type,student'],
            'sessions' => ['required_with:frequency', 'array', 'min:1', 'max:7']
        ];
        $v = Validator::make($request->all(), $rules);
        $errors['main'] = [];
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        if (count($request->sessions) != $request->frequency) {
            $hasErrors = true;
        }
        $sessions = $request->sessions;
        $errors['sessions'] = [];
        //Sessions rules
        $days = implode(',', $this->weekdays);
        $rules = [
            'day' => ['in:' . $days],
            'session_time' => ['regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/'],
        ];
        if (is_array($sessions)) {
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['sessions'][$session['index']] = $v->errors()->messages();
                }
            }
        }
        $course = OneCourse::find($request->course);
        if ($request->course > 0 && $course && $course->has_vc) {
            $rules = [
                'vc_link' => ['url']
            ];
            $v = Validator::make($request->all(), $rules);
            if ($v->fails()) {
                $errors['main']['vc_link'] = $v->errors()->messages()['vc_link'];
                $hasErrors = true;
            }
        }

        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        /** END VALIDACTION */
        $schedule->vc_link = $request->vc_link;
        $schedule->course_id = $request->course;
        $schedule->shift_id = $request->shift;
        $schedule->teacher_id = $request->teacher;
        $schedule->student_id = $request->student;
        $schedule->n_sessions = $request->n_sessions;
        $schedule->n_sessions_price = $request->n_sessions_price;
        $schedule->frequency = $request->frequency;
        $schedule->saved = true;
        $schedule->save();
        if (is_array($request->sessions)) {
            $schedule->daytimes()->delete();
            $sessions = $request->sessions;
            foreach ($sessions as $session) {
                $daytime = new OneScheduleDaytime;
                $daytime->day = $session['day'];
                $daytime->session_time = $session['session_time'];
                $schedule->daytimes()->save($daytime);
            }
        }
        return response()->json(['url' => route('admin.one_schedule.index')]);
    }

    public function publish($id, Request $request) {
        $schedule = OneSchedule::findOrFail($id);
        /** VALIDACTION */
        $errors = [];
        $hasErrors = false;
        $rules = [
            'frequency' => ['required', 'integer', 'min:1', 'max:7'],
            'start_date' => ['required', 'date'],
            'course' => ['required', 'integer', 'exists:one_courses,id'],
            'n_sessions' => ['required', 'integer'],
            'n_sessions_price' => ['required', 'integer'],
            'shift' => ['required', 'integer', 'exists:shifts,id'],
            'teacher' => ['required', 'integer', 'exists:accounts,id,type,teacher'],
            'student' => ['required', 'integer', 'exists:accounts,id,type,student'],
            'sessions' => ['required', 'required_with:frequency', 'array', 'min:1', 'max:7']
        ];
        $v = Validator::make($request->all(), $rules);
        $errors['main'] = [];
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        if (count($request->sessions) != $request->frequency) {
            $hasErrors = true;
        }
        $sessions = $request->sessions;
        $errors['sessions'] = [];
        //Sessions rules
        $days = implode(',', $this->weekdays);
        $rules = [
            'day' => ['required', 'in:' . $days],
            'session_time' => ['required', 'regex:/^([0-1]\d|(2[0-3])):[0-5]\d$/'],
        ];
        if (is_array($sessions)) {
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['sessions'][$session['index']] = $v->errors()->messages();
                }
            }
        }
        $course = OneCourse::find($request->course);
        if ($request->course > 0 && $course && $course->has_vc) {
            $rules = [
                'vc_link' => ['required', 'url']
            ];
            $v = Validator::make($request->all(), $rules);
            if ($v->fails()) {
                $errors['main']['vc_link'] = $v->errors()->messages()['vc_link'];
                $hasErrors = true;
            }
        }

        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        /** END VALIDACTION */
        $schedule->vc_link = $request->vc_link;
        $schedule->course_id = $request->course;
        $schedule->shift_id = $request->shift;
        $schedule->teacher_id = $request->teacher;
        $schedule->student_id = $request->student;
        $schedule->n_sessions = $request->n_sessions;
        $schedule->n_sessions_price = $request->n_sessions_price;
        $schedule->frequency = $request->frequency;
        $schedule->saved = true;
        $schedule->published = true;
        $schedule->save();
        if (is_array($request->sessions)) {
            $schedule->daytimes()->delete();
            $sessions = $request->sessions;
            foreach ($sessions as $session) {
                $daytime = new OneScheduleDaytime;
                $daytime->day = $session['day'];
                $daytime->session_time = $session['session_time'];
                $schedule->daytimes()->save($daytime);
            }
        }
        return response()->json(['url' => route('admin.one_schedule.index')]);
    }

    public function delete($id) {
        OneSchedule::findOrFail($id)->delete();
    }

}
