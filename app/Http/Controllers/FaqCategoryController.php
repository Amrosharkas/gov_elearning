<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FaqCategory;
use Validator;

class FaqCategoryController extends Controller {

    public function index() {
        $categories = FaqCategory::orderBy('stuff_order')->get();
        $data = [];
        $data['categories'] = $categories;
        $data['partialView'] = 'faq_category.list';
        return view('faq_category.base', $data);
    }

    public function create() {
        $data = [];
        $data['category'] = false;
        $data['partialView'] = 'faq_category.form';
        return view('faq_category.base', $data);
    }

    public function edit($id) {
        $category = FaqCategory::findOrFail($id);
        $data = [];
        $data['category'] = $category;
        $data['partialView'] = 'faq_category.form';
        return view('faq_category.base', $data);
    }

    public function store(Request $request) {
        $rules = ['name' => 'required|unique:faq_categories,name'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $category = new FaqCategory();
        $category->name = $request->name;
        $category->save();
        return response()->json(['url' => route('admin.faq_category.index')]);
    }

    public function update($id, Request $request) {
        $category = FaqCategory::findOrFail($id);
        $rules = ['name' => 'required|unique:faq_categories,name,' . $id];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $category->name = $request->name;
        $category->save();

        return response()->json(['url' => route('admin.faq_category.index')]);
    }

    public function delete($id) {
        FaqCategory::destroy($id);
    }

    public function deleteMultiple(Request $request) {
        FaqCategory::destroy($request->items);
    }

    public function reorder(Request $request) {
        $newSequence = $request->orderList;
        if (!is_array($newSequence)) {
            abort(400);
        }
        $allCount = FaqCategory::count();
        $foundCount = FaqCategory::whereIn('id', $newSequence)->count();
        $newSequenceCount = count($newSequence);
        if ($newSequenceCount != $allCount || $newSequenceCount != $foundCount) {
            abort(400);
        }
        $order = 1;
        foreach ($newSequence as $id) {
            FaqCategory::where('id', '=', $id)->update(['stuff_order' => $order++]);
        }
    }

}
