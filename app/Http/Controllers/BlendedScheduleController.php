<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlendedSchedule;
use App\BlendedCourse;
use App\BlendedTemplate;
use App\Shift;
use App\Account;
use DB;
use App\BlendedScheduleSession;
use Validator;

class BlendedScheduleController extends Controller {

    public function index() {
        $data = [];
        $data['partialView'] = 'blended_schedule.list';
        $data['schedules'] = BlendedSchedule::with(['teacher', 'template.course'])->orderBy('created_at', 'desc')->get();
        return view('blended_schedule.base', $data);
    }

    public function init() {
        $schedule = BlendedSchedule::create();
        return response(['url' => route('admin.blended_schedule.edit', ['id' => $schedule->id])]);
    }

    public function edit($id) {
        $schedule = BlendedSchedule::findOrFail($id);
        $data = [];
        $data['partialView'] = 'blended_schedule.form';
        $data['schedule'] = $schedule;
        $data['courses'] = BlendedCourse::whereNotNull('title')
                        ->with(['templates' => function($query) {
                                $query->whereNotNull('title');
                            }])->get();
        $data['teachers'] = Account::where('type', '=', 'teacher')->get();
//        $data['templates'] = BlendedTemplate::whereNotNull('title')->get();
        $data['shifts'] = Shift::whereNotNull('name')->get();
        return view('blended_schedule.base', $data);
    }

    public function students($id, Request $request) {
        $schedule = BlendedSchedule::findOrFail($id);
        $studentIds = $schedule->students->pluck('id');
        return Account::where('accounts.type', '=', 'student')
                        ->whereNotIn('accounts.id', $studentIds)
                        ->select('accounts.name', 'accounts.user_id', DB::raw('accounts.id as acc_id'))
                        ->search($request->get('q'))->with('user')
                        ->get();
    }

    public function templates(Request $request) {
        $course = BlendedCourse::findOrFail($request->id);
        return response()->json(['templates' => $course->templates]);
    }

    public function sessions(Request $request) {
        $template = BlendedTemplate::findOrFail($request->id);
        return response()->json(['sessions' => $template->sessions]);
    }

    public function addStudent($id, Request $request) {
        $schedule = BlendedSchedule::findOrFail($id);
        $student = Account::findOrFail($request->id);
        if ($student->type != 'student' || $schedule->students()->where('student_id', '=', $request->id)->count()) {
            abort(404);
        }
        $schedule->students()->attach($student);
    }

    public function deleteStudent($id, Request $request) {
        $schedule = BlendedSchedule::findOrFail($id);
        $student = Account::findOrFail($request->id);
        $schedule->students()->detach($student->id);
    }

    public function update($id, Request $request) {
        $schedule = BlendedSchedule::findOrFail($id);
        // VALIDATION
        $hasErrors = false;
        $errors = [];
        $rules = [
            'course' => ['required_with:template', 'integer', 'exists:blended_courses,id'],
            'template' => ['integer', 'exists:blended_templates,id'],
            'shift' => ['integer', 'exists:shifts,id'],
            'closed_group' => ['boolean'],
            'vc_link' => ['url'],
            'price' => ['integer'],
            'teacher_payment' => ['numeric'],
            'locked_by' => ['date'],
            'hidden_by' => ['date'],
            'archived_by' => ['date'],
            'sessions' => ['array']
        ];
        $v = Validator::make($request->all(), $rules);
        $erros['main'] = [];
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        $errors['sessions'] = [];
        $sessions = $request->sessions;
        if (is_array($sessions)) {
            $rules = [
                'start_time' => 'date'
            ];
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['sessions'][$session['id']] = $v->errors()->messages();
                }
            }
        }
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        if ($request->course > 0 && $request->template > 0) {
            $course = BlendedCourse::find($request->course);
            $template = BlendedTemplate::find($request->template);
            if ($course && $template && $course->id != $template->course_id) {
                return response()->json(compact('errors'));
            }
        }
        // END VALIDATION
        $schedule->course_id = $request->course;
        $schedule->template_id = $request->template;
        $schedule->shift_id = $request->shift;
        $schedule->teacher_id = $request->teacher;
        $schedule->vc_link = $request->vc_link;
        $schedule->price = $request->price;
        $schedule->teacher_payment = $request->teacher_payment;
        $schedule->closed_group = $request->closed_group;
        $schedule->locked_by = $request->locked_by;
        $schedule->hidden_by = $request->hidden_by;
        $schedule->archived_by = $request->archived_by;
        $schedule->saved = true;
        $schedule->save();
        $sessions = $request->sessions;
        if (is_array($sessions) && $schedule->template_id) {
            foreach ($sessions as $session) {
                $id = $session['id'];
                $session['start_time'] = $session['start_time'] ? : null;
                unset($session['id']);
                $schedule->sessions()->updateExistingPivot($id, $session);
            }
        }
        return response()->json(['url' => route('admin.blended_schedule.index')]);
    }

    public function publish($id, Request $request) {
        $schedule = BlendedSchedule::findOrFail($id);
        // VALIDATION
        $hasErrors = false;
        $errors = [];
        $rules = [
            'course' => ['required', 'integer', 'exists:blended_courses,id'],
            'template' => ['required', 'integer', 'exists:blended_templates,id'],
            'shift' => ['required', 'integer', 'exists:shifts,id'],
            'closed_group' => ['boolean'],
            'vc_link' => ['required', 'url'],
            'price' => ['required', 'integer'],
            'teacher_payment' => ['required', 'numeric'],
            'locked_by' => ['required', 'date'],
            'hidden_by' => ['required', 'date'],
            'archived_by' => ['required', 'date'],
            'sessions' => ['required', 'array']
        ];
        $v = Validator::make($request->all(), $rules);
        $erros['main'] = [];
        if ($v->fails()) {
            $hasErrors = true;
            $errors['main'] = $v->errors()->messages();
        }
        $errors['sessions'] = [];
        $sessions = $request->sessions;
        if (is_array($sessions)) {
            $rules = [
                'start_time' => ['required', 'date']
            ];
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $hasErrors = true;
                    $errors['sessions'][$session['id']] = $v->errors()->messages();
                }
            }
        }
        if (!$schedule->teacher) {
            $hasErrors = true;
            $errors['teacher'] = 'The teacher field is required';
        }
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        if ($request->course > 0 && $request->template > 0) {
            $course = BlendedCourse::find($request->course);
            $template = BlendedTemplate::find($request->template);
            if ($course && $template && $course->id != $template->course_id) {
                return response()->json(compact('errors'));
            }
        }
        // END VALIDATION
        $schedule->course_id = $request->course;
        $schedule->template_id = $request->template;
        $schedule->shift_id = $request->shift;
        $schedule->teacher_id = $request->teacher;
        $schedule->vc_link = $request->vc_link;
        $schedule->price = $request->price;
        $schedule->teacher_payment = $request->teacher_payment;
        $schedule->closed_group = $request->closed_group;
        $schedule->locked_by = $request->locked_by;
        $schedule->hidden_by = $request->hidden_by;
        $schedule->archived_by = $request->archived_by;
        $schedule->saved = true;
        $schedule->published = true;
        $schedule->save();
        $sessions = $request->sessions;
        if (is_array($sessions) && $schedule->template_id) {
            foreach ($sessions as $session) {
                $id = $session['id'];
                $session['start_time'] = $session['start_time'] ? : null;
                unset($session['id']);
                $schedule->sessions()->updateExistingPivot($id, $session);
            }
        }
        return response()->json(['url' => route('admin.blended_schedule.index')]);
    }

    public function delete($id) {
        BlendedSchedule::findOrFail($id)->delete();
    }

}
