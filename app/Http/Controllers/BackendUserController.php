<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\User;
use Validator;

class BackendUserController extends Controller {

    public function index() {
        $data = [];
        $users = Account::where('backend', '=', true)->where('type', '!=', 'superAdmin')->with('user')->get();
        $data['users'] = $users;
        $data['partialView'] = 'backend_user.list';
        return view('backend_user.base', $data);
    }

    public function create() {
        $data = [];
        $data['partialView'] = 'backend_user.form';
        $data['user'] = false;
        $data['types'] = User::getBackendUserTypes();
        return view('backend_user.base', $data);
    }

    public function edit($id) {
        $user = Account::where('backend', '=', true)->findOrFail($id);
        $data = [];
        $data['partialView'] = 'backend_user.form';
        $data['user'] = $user;
        $data['types'] = User::getBackendUserTypes();
        return view('backend_user.base', $data);
    }

    public function store(Request $request) {
        $rules = $this->rules();
        $rules['email'][] = 'unique:users,email';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt('123456');
        $user->save();
        $account = new Account();
        $account->name = $request->name;
        $account->type = $request->type;
        $account->user_id = $user->id;
        $account->backend = true;
        $account->save();
        return response()->json(['url' => route('admin.backend_user.index')]);
    }

    public function update($id, Request $request) {
        $account = Account::findOrFail($id);
        $rules = $this->rules();
        $rules['email'][] = 'unique:users,email,' . $id;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        $account->name = $request->name;
        $account->type = $request->type;
        $account->save();
        $account->user()->update(['email' => $request->email]);
        return response()->json(['url' => route('admin.backend_user.index')]);
    }

    public function delete($id) {
        Account::findOrFail($id)->delete();
    }

    public function deleteMultiple(Request $request) {
        Account::whereIn('id', $request->items)->delete();
    }

    public function rules() {
        $types = implode(',', User::getBackendUserTypes());
        $rules = [
            'email' => ['required', 'email'],
            'type' => ['required', 'in:' . $types],
            'name' => ['required']
        ];
        return $rules;
    }

}
