<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlendedTemplate;
use App\BlendedCourse;
use App\File;
use App\BlendedSession;
use Validator;

class BlendedTemplateController extends Controller {

    public function index() {
        $data = [];
        $data['partialView'] = 'blended_template.list';
        $data['templates'] = BlendedTemplate::with('course')->orderBy('created_at', 'desc')->get();
        return view('blended_template.base', $data);
    }

    public function init() {
        $template = new BlendedTemplate();
        $template->save();
        return response(['url' => route('admin.blended_template.edit', ['id' => $template->id])]);
    }

    public function edit($id) {
        $template = BlendedTemplate::findOrFail($id);
        $data = [];
        $data['partialView'] = 'blended_template.form';
        $data['template'] = $template;
        $data['courses'] = BlendedCourse::whereNotNull('title')->get();
        $data['sessions'] = $template->sessions()->orderBy('stuff_order')->get();
        return view('blended_template.base', $data);
    }

    public function uploadIg($id, Request $request) {
        $template = BlendedTemplate::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended_template/ig');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $template->ig_id = $file->id;
        $template->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended_template.delete_ig', ['id' => $template->id]);
        $responseData['download_action'] = route('admin.blended_template.ig', ['id' => $template]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }

    public function ig($id) {
        $template = BlendedTemplate::findOrFail($id);
        if ($template->ig()) {
            return response()->download($template->ig()->path, $template->ig()->name);
        }
        abort(404);
    }

    public function deleteIg($id) {
        BlendedTemplate::findOrFail($id)->deleteIg();
    }

    public function update(Request $request, $id) {
        // VALIDATION
        $errors = [];
        $hasErrors = false;
        $rules = [
            'course' => ['integer', 'exists:blended_courses,id'],
            'template_title' => ['required'],
            'homework_count' => ['integer'],
            'sessions' => ['array']
        ];
        $v = Validator::make($request->all(), $rules);
        $errors['main'] = [];
        if ($v->fails()) {
            $errors['main'] = $v->errors()->messages();
            $hasErrors = true;
        }
        $sessions = $request->sessions;
        $errors['sessions'] = [];
        if (is_array($sessions)) {
            $rules = [
                'duration' => ['integer']
            ];
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $errors['sessions'][$session['id']] = $v->errors()->messages();
                    $hasErrors = true;
                }
            }
        }
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }
        // END VALIDATION
        $template = BlendedTemplate::findOrFail($id);
        $template->title = $request->template_title;
        $template->course_id = $request->course;
        $template->homework_count = $request->homework_count;
        if (is_array($request->sessions)) {
            $sessions = $request->sessions;
            $order = 1;
            foreach ($sessions as $session) {
                $session['stuff_order'] = $order++;
                $id = $session['id'];
                $session['duration'] = $session['duration'];
                unset($session['id']);
                BlendedSession::where('id', '=', $id)->update($session);
            }
        }
        $template->save();
        $url = route('admin.blended_template.index');
        return response()->json(
                        compact('url'));
    }

    public function publish(Request $request, $id) {
        // VALIDATION
        $errors = [];
        $hasErrors = false;
        $rules = [
            'course' => ['required', 'integer', 'exists:blended_courses,id'],
            'template_title' => ['required'],
            'homework_count' => ['integer'],
            'sessions' => ['required', 'array']
        ];
        $v = Validator::make($request->all(), $rules);
        $errors['main'] = [];
        if ($v->fails()) {
            $errors['main'] = $v->errors()->messages();
            $hasErrors = true;
        }
        $sessions = $request->sessions;
        $errors['sessions'] = [];
        if (is_array($sessions)) {
            $rules = [
                'title' => ['required'],
                'duration' => ['required', 'integer']
            ];
            foreach ($sessions as $session) {
                $v = Validator::make($session, $rules);
                if ($v->fails()) {
                    $errors['sessions'][$session['id']] = $v->errors()->messages();
                    $hasErrors = true;
                }
            }
        }
        if ($hasErrors) {
            return response()->json(compact('errors'));
        }

        // END VALIDATION
        $template = BlendedTemplate::findOrFail($id);
        $template->title = $request->template_title;
        $template->course_id = $request->course;
        $template->homework_count = $request->homework_count;
        if (is_array($request->sessions)) {
            $sessions = $request->sessions;
            $order = 1;
            foreach ($sessions as $session) {
                $session['stuff_order'] = $order++;
                $id = $session['id'];
                $session['duration'] = $session['duration'];
                unset($session['id']);
                BlendedSession::where('id', '=', $id)->update($session);
            }
        }
        $template->save();
        $url = route('admin.blended_template.index');
        return response()->json(
                        compact('url'));
    }

    public function initSession($id, Request $request) {
        $template = BlendedTemplate::findOrFail($id);
        $session = new BlendedSession();
        $session->stuff_order = $template->sessions()->max('stuff_order') + 1;
        $template->sessions()->save($session);
        $responseData = [];
        $responseData['id'] = $session->id;
        $responseData['remove_session_action'] = route('admin.blended_template.delete_session', ['id' => $session->id]);
        $responseData['upload_content_action'] = route('admin.blended_template.upload_session_content', ['id' => $session->id]);
        $responseData['upload_material_action'] = route('admin.blended_template.upload_session_material', ['id' => $session->id]);
        return response()->json(
                        $responseData);
    }

    public function deleteSession(Request $request) {
        BlendedSession::findOrFail($request->id
        )->delete();
    }

    public function uploadSessionMaterial($id, Request $request) {
        $session = BlendedSession::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended_template/session_material');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $session->material_id = $file->id;
        $session->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended_template.delete_session_material', ['id' => $session->id]);
        $responseData['download_action'] = route('admin.blended_template.session_material', ['id' => $session]);
        $responseData['file_name'] = $file->name;
        return response()->json(
                        $responseData);
    }

    public function uploadSessionContent($id, Request $request) {
        $session = BlendedSession::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = storage_path('uploads/blended_template/session_content');
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $session->content_id = $file->id;
        $session->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.blended_template.delete_session_content', ['id' => $session->id]);
        $responseData['download_action'] = route('admin.blended_template.session_content', ['id' => $session]);
        $responseData['file_name'] = $file->name;
        return response()->json(
                        $responseData);
    }

    public function sessionMaterial($id) {
        $session = BlendedSession::findOrFail($id);
        if ($session->material()) {
            return response()->download($session->material()->path, $session->material()->name);
        }

        abort(404);
    }

    public function sessionContent($id) {
        $session = BlendedSession::findOrFail($id);
        if ($session->content()) {
            return response()->download($session->content()->path, $session->content()->name);
        }

        abort(404);
    }

    public function deleteSessionMaterial($id) {
        BlendedSession::findOrFail($id)->
                deleteMaterial();
    }

    public function deleteSessionContent($id) {
        BlendedSession::findOrFail($id)->
                deleteContent();
    }

    public function delete($id) {
        BlendedTemplate::findOrFail($id)->delete();
    }

}
