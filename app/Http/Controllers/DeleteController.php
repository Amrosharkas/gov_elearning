<?php

namespace App\Http\Controllers;
use App\Shift;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeleteController extends Controller
{

    public function delete($model, $id)
    {
        //dd($id);
        $model_name = 'App\\' . $model;
        $object = new $model_name();
        $found = $object::where('id',$id)->delete();
        dd($found);
    }

    public function deleteMultiple($model, Request $request)
    {
        $data = $request->input();
        $selected_objects_id = count($data["users"]);
        $model_name = 'App\\' . $model;
        $object = new $model_name();
        $return = "";
        for ($i = 0; $i < $selected_objects_id; $i++)
        {

            $user_id = trim(strip_tags($_POST["users"][$i]));
            $return .= $user_id . ",";
            $object::where('id',$user_id)->delete();
        }

        return $return;
    }

}
