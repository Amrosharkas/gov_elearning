<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', function () {
    return redirect('/admin/blended');
});
//Route::get('session', function () {
//    dd(session()->all());
//});
Route::group(['prefix' => 'exp'], function () {
    Route::get('validation', 'ExperimentsController@create')->name('exp.validation.create');
    Route::get('validation/edit', 'ExperimentsController@edit')->name('exp.validation.edit');
    Route::post('validation', 'ExperimentsController@store')->name('exp.validation.store');
    Route::patch('validation/update', 'ExperimentsController@update')->name('exp.validation.update');
    Route::get('ajax', 'ExperimentsController@ajax')->name('exp.ajax');
    Route::get('auth', ['uses' => 'ExperimentsController@auth', 'middleware' => 'auth']);
    Route::get('instance', 'ExperimentsController@instanceTrial');
    Route::post('request', 'ExperimentsController@req');
    Route::get('chkbox', function () {
        return view('experiments.checkboxes');
    });
});
//$modules = modules();
//foreach ($modules as $module) {
//    Route::group(['middleware' => ['auth', 'admin', 'module'], 'prefix' => "admin/{$module}", 'as' => "admin.{$module}."], function () use ($module) {
//        foreach (module($module)->getActionsList() as $action) {
//            custom_action($action);
//        }
//    });
//}
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin/shift', 'as' => 'admin.shift.'], function () {
    Route::get('/', 'ShiftController@index')->name('index');
    Route::get('{id}/edit', 'ShiftController@edit')->name('edit');
    Route::delete('{id}/delete', 'ShiftController@delete')->name('delete');
    Route::get('create', 'ShiftController@create')->name('create');
    Route::delete('delete_multiple', 'ShiftController@deleteMultiple')->name('delete_multiple');
    Route::post('store', 'ShiftController@store')->name('store');
    Route::patch('{id}/update', 'ShiftController@update')->name('update');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin/backend_user', 'as' => 'admin.backend_user.'], function () {
    Route::get('/', 'BackendUserController@index')->name('index');
    Route::get('create', 'BackendUserController@create')->name('create');
    Route::get('{id}/edit', 'BackendUserController@edit')->name('edit');
    Route::post('store', 'BackendUserController@store')->name('store');
    Route::patch('{id}/update', 'BackendUserController@update')->name('update');
    Route::delete('{id}/delete', 'BackendUserController@delete')->name('delete');
    Route::delete('delete_multiple', 'BackendUserController@deleteMultiple')->name('delete_multiple');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin/faq_category', 'as' => 'admin.faq_category.'], function() {
    Route::get('/', 'FaqCategoryController@index')->name('index');
    Route::get('create', 'FaqCategoryController@create')->name('create');
    Route::get('{id}/edit', 'FaqCategoryController@edit')->name('edit');
    Route::post('reorder', 'FaqCategoryController@reorder')->name('reorder');
    Route::post('store', 'FaqCategoryController@store')->name('store');
    Route::patch('{id}/update', 'FaqCategoryController@update')->name('update');
    Route::delete('{id}/delete', 'FaqCategoryController@delete')->name('delete');
    Route::delete('delete_multiple', 'FaqCategoryController@deleteMultiple')->name('delete_multiple');
});
Route::group(['middleware' => ['auth', 'faqs'], 'prefix' => "admin/faq", 'as' => "admin.faq."], function () {
    Route::get('create', 'FaqController@create')->name('create');
    Route::get('{status}', 'FaqController@index')->name('index');
    Route::get('{id}/edit', 'FaqController@edit')->name('edit');
    Route::get('{id}/review_pending', 'FaqController@reviewPending')->name('review_pending');
    Route::delete('{id}/delete', 'FaqController@delete')->name('delete');
    Route::delete('delete_multiple', 'FaqController@deleteMultiple')->name('delete_multiple');
    Route::post('{id}/reorder', 'FaqController@reorder')->name('reorder');
    Route::post('store', 'FaqController@store')->name('store');
    Route::patch('{id}/update', 'FaqController@update')->name('update');
    Route::post('publish_new', 'FaqController@publishNew')->name('publish_new');
    Route::patch('{id}/publish', 'FaqController@publish')->name('publish');
    Route::get('{id}/modify', 'FaqController@getModify')->name('get_modify');
    Route::get('{id}/review_modification', 'FaqController@reviewModification')->name('review_modification');
    Route::patch('{id}/modify', 'FaqController@modify')->name('modify');
    Route::delete('{id}/reject', 'FaqController@reject')->name('reject');
    Route::patch('{id}/confirm', 'FaqController@confirm')->name('confirm');
    Route::patch('{id}/confirm_modification', 'FaqController@confirmModification')->name('confirm_modification');
    Route::delete('{id}/reject_modification', 'FaqController@rejectModification')->name('reject_modification');
});
Route::group(['middleware' => ['auth', 'faqs'], 'prefix' => "admin/user_question", 'as' => "admin.user_question."], function () {
    Route::get('{status}', 'UserQuestionController@index')->name('index');
    Route::get('{id}/answer', 'UserQuestionController@answer')->name('answer');
    Route::get('{id}/view', 'UserQuestionController@view')->name('view');
    Route::patch('{id}/send', 'UserQuestionController@send')->name('send');
    Route::post('{id}/copy', 'UserQuestionController@copy')->name('copy');
    Route::patch('{id}/report', 'UserQuestionController@report')->name('report');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/one_on_one", 'as' => "admin.one_on_one."], function () {
    Route::get('/', 'OneOnOneController@index')->name('index');
    Route::post('init', 'OneOnOneController@init')->name('init');
    Route::post('{id}/init_level', 'OneOnOneController@initLevel')->name('init_level');
    Route::delete('{id}/delete', 'OneOnOneController@delete')->name('delete');
    Route::delete('{id}/delete_level', 'OneOnOneController@deleteLevel')->name('delete_level');
    Route::post('{id}/init_objective', 'OneOnOneController@initObjective')->name('init_objective');
    Route::delete('delete_objective', 'OneOnOneController@deleteObjective')->name('delete_objective');
    Route::post('{id}/init_pricing', 'OneOnOneController@initPricing')->name('init_pricing');
    Route::delete('{id}delete_pricing', 'OneOnOneController@deletePricing')->name('delete_pricing');
    Route::patch('{id}/update', 'OneOnOneController@store')->name('update');
    Route::patch('{id}/publish', 'OneOnOneController@publish')->name('publish');
    Route::post('{id}/upload_ig', 'OneOnOneController@uploadIg')->name('upload_ig');
    Route::get('level/{id}/ig', 'OneOnOneController@ig')->name('ig');
    Route::delete('level/{id}/delete_ig', 'OneOnOneController@deleteIg')->name('delete_ig');
    Route::post('{id}/upload_material', 'OneOnOneController@uploadMaterial')->name('upload_material');
    Route::get('level/{id}/material', 'OneOnOneController@material')->name('material');
    Route::delete('level/{id}/delete_material', 'OneOnOneController@deleteMaterial')->name('delete_material');
    Route::post('{id}/upload_section_image', 'OneOnOneController@uploadSectionImage')->name('upload_section_image');
    Route::get('level/{id}/section_image', 'OneOnOneController@sectionImage')->name('section_image');
    Route::delete('level/{id}/delete_section_image', 'OneOnOneController@deleteSectionImage')->name('delete_section_image');
    Route::post('{id}upload_course_image', 'OneOnOneController@uploadCourseImage')->name('upload_course_image');
    Route::get('level/{id}/course_image', 'OneOnOneController@courseImage')->name('course_image');
    Route::delete('level/{id}/delete_course_image', 'OneOnOneController@deleteCourseImage')->name('delete_course_image');
    Route::post('{id}/upload_promo', 'OneOnOneController@uploadPromo')->name('upload_promo');
    Route::get('level/{id}/promo', 'OneOnOneController@promo')->name('promo');
    Route::delete('level/{id}/delete_promo', 'OneOnOneController@deletePromo')->name('delete_promo');
    Route::get('{id}/edit', 'OneOnOneController@edit')->name('edit');
    Route::patch('{id}/update', 'OneOnOneController@update')->name('update');
    Route::patch('{id}/publish', 'OneOnOneController@publish')->name('publish');
});

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/blended", 'as' => "admin.blended."], function () {
    Route::get('/', 'BlendedController@index')->name('index');
    Route::post('init', 'BlendedController@init')->name('init');
    Route::get('{id}/edit', 'BlendedController@edit')->name('edit');
    Route::delete('{id}/delete', 'BlendedController@delete')->name('delete');
    Route::post('{id}/upload_section_image', 'BlendedController@uploadSectionImage')->name('upload_section_image');
    Route::post('{id}/upload_course_image', 'BlendedController@uploadCourseImage')->name('upload_course_image');
    Route::post('{id}/upload_course_material', 'BlendedController@uploadCourseMaterial')->name('upload_course_material');
    Route::post('{id}/upload_promo', 'BlendedController@uploadPromo')->name('upload_promo');
    Route::get('{id}/section_image', 'BlendedController@sectionImage')->name('section_image');
    Route::get('{id}/course_image', 'BlendedController@courseImage')->name('course_image');
    Route::get('{id}/course_material', 'BlendedController@courseMaterial')->name('course_material');
    Route::get('{id}/promo', 'BlendedController@promo')->name('promo');
    Route::delete('{id}/delete_section_image', 'BlendedController@deleteSectionImage')->name('delete_section_image');
    Route::delete('{id}/delete_course_image', 'BlendedController@deleteCourseImage')->name('delete_course_image');
    Route::delete('{id}/delete_course_material', 'BlendedController@deleteCourseMaterial')->name('delete_course_material');
    Route::delete('{id}/delete_promo', 'BlendedController@deletePromo')->name('delete_promo');
    Route::patch('{id}/update', 'BlendedController@update')->name('update');
    Route::patch('{id}/publish', 'BlendedController@publish')->name('publish');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/blended_template", 'as' => "admin.blended_template."], function () {
    Route::get('/', 'BlendedTemplateController@index')->name('index');
    Route::post('init', 'BlendedTemplateController@init')->name('init');
    Route::get('{id}/edit', 'BlendedTemplateController@edit')->name('edit');
    Route::post('{id}/upload_ig', 'BlendedTemplateController@uploadIg')->name('upload_ig');
    Route::get('{id}/ig', 'BlendedTemplateController@ig')->name('ig');
    Route::delete('{id}/delete_ig', 'BlendedTemplateController@deleteIg')->name('delete_ig');
    Route::patch('{id}/update', 'BlendedTemplateController@update')->name('update');
    Route::patch('{id}/publish', 'BlendedTemplateController@publish')->name('publish');
    Route::post('{id}/init_session', 'BlendedTemplateController@initSession')->name('init_session');
    Route::delete('{id}/delete_session', 'BlendedTemplateController@deleteSession')->name('delete_session');
    Route::post('{id}/upload_session_material', 'BlendedTemplateController@uploadSessionMaterial')->name('upload_session_material');
    Route::get('{id}/session_material', 'BlendedTemplateController@session_material')->name('session_material');
    Route::delete('{id}/delete_session_material', 'BlendedTemplateController@deleteSessionMaterial')->name('delete_session_material');
    Route::post('{id}/upload_session_content', 'BlendedTemplateController@uploadSessionContent')->name('upload_session_content');
    Route::get('{id}/session_content', 'BlendedTemplateController@session_content')->name('session_content');
    Route::delete('{id}/delete_session_content', 'BlendedTemplateController@deleteSessionContent')->name('delete_session_content');
    Route::delete('{id}/delete', 'BlendedTemplateController@delete')->name('delete');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/blended_schedule", 'as' => "admin.blended_schedule."], function () {
    Route::get('/', 'BlendedScheduleController@index')->name('index');
    Route::post('init', 'BlendedScheduleController@init')->name('init');
    Route::get('{id}/edit', 'BlendedScheduleController@edit')->name('edit');
    Route::delete('{id}/delete', 'BlendedScheduleController@delete')->name('delete');
    Route::patch('{id}/update', 'BlendedScheduleController@update')->name('update');
    Route::patch('{id}/publish', 'BlendedScheduleController@publish')->name('publish');
    Route::get('{id}/students', 'BlendedScheduleController@students')->name('students');
    Route::post('{id}/add_student', 'BlendedScheduleController@addStudent')->name('add_student');
    Route::delete('{id}/delete_student', 'BlendedScheduleController@deleteStudent')->name('delete_student');
    Route::get('templates', 'BlendedScheduleController@templates')->name('templates');
    Route::get('sessions', 'BlendedScheduleController@sessions')->name('sessions');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/one_schedule", 'as' => "admin.one_schedule."], function () {
    Route::get('/', 'OneScheduleController@index')->name('index');
    Route::post('init', 'OneScheduleController@init')->name('init');
    Route::get('{id}/edit', 'OneScheduleController@edit')->name('edit');
    Route::delete('{id}/delete', 'OneScheduleController@delete')->name('delete');
    Route::get('students', 'OneScheduleController@students')->name('students');
    Route::get('teachers', 'OneScheduleController@teachers')->name('teachers');
    Route::patch('{id}/update', 'OneScheduleController@update')->name('update');
    Route::patch('{id}/publish', 'OneScheduleController@publish')->name('publish');
    Route::delete('{id}/delete', 'OneScheduleController@delete')->name('delete');
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/elearning", 'as' => "admin.elearning.", 'namespace' => 'Elearning'], function () {
    Route::group(['prefix' => 'course', 'as' => 'course.'], function () {
        Route::get('/', 'CourseController@index')->name('index');
        Route::post('init', 'CourseController@init')->name('init');
        Route::get('{id}/edit', 'CourseController@edit')->name('edit');
        Route::patch('{id}/update', 'CourseController@update')->name('update');
        Route::patch('{id}/publish', 'CourseController@publish')->name('publish');
    });
    Route::group(['prefix' => 'module', 'as' => 'module.'], function () {
        Route::post('{course_id}/init', 'ModuleController@init')->name('init');
        Route::delete('{id}/delete', 'ModuleController@delete')->name('delete');
        Route::post('{id}/upload_content', 'ModuleController@uploadContent')->name('upload_content');
        Route::post('{id}/upload_material', 'ModuleController@uploadMaterial')->name('upload_material');
        Route::delete('{id}/delete_content', 'ModuleController@deleteContent')->name('delete_content');
        Route::delete('{id}/delete_material', 'ModuleController@deleteMaterial')->name('delete_material');
    });
    Route::group(['prefix' => 'lesson', 'as' => 'lesson.'], function () {
        Route::post('{course_id}/init', 'LessonController@init')->name('init');
        Route::delete('{id}/delete', 'LessonController@delete')->name('delete');
        Route::post('{id}/upload_content', 'LessonController@uploadContent')->name('upload_content');
        Route::post('{id}/upload_material', 'LessonController@uploadMaterial')->name('upload_material');
        Route::delete('{id}/delete_content', 'LessonController@deleteContent')->name('delete_content');
        Route::delete('{id}/delete_material', 'LessonController@deleteMaterial')->name('delete_material');
    });
    Route::group(['prefix' => 'topic', 'as' => 'topic.'], function () {
        Route::post('{course_id}/init', 'TopicController@init')->name('init');
        Route::delete('{id}/delete', 'TopicController@delete')->name('delete');
        Route::post('{id}/upload_content', 'TopicController@uploadContent')->name('upload_content');
        Route::post('{id}/upload_material', 'TopicController@uploadMaterial')->name('upload_material');
        Route::delete('{id}/delete_content', 'TopicController@deleteContent')->name('delete_content');
        Route::delete('{id}/delete_material', 'TopicController@deleteMaterial')->name('delete_material');
    });
});
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/quizzes", 'as' => "admin.quizzes."], function () {
    Route::get('/', 'quizController@index')->name('index');
    Route::post('init', 'quizController@init')->name('init');
    Route::get('{id}/edit', 'quizController@edit')->name('edit');
    Route::post('question_add/{quiz_id}', 'quizController@addQuestion')->name('question_add');
    Route::post('answer_add/{quiz_id}', 'quizController@addAnswer')->name('answer_add');
    Route::get('answers/{question_id}', 'quizController@answers')->name('answers');
    Route::delete('{id}/delete', 'quizController@delete')->name('delete');
    Route::post('quizzes/{id}/init_question', 'quizController@initQuestion')->name('init_question');
    Route::post('{id}/upload_question_file', 'quizController@uploadQuestionFile')->name('upload_question_file');
    Route::post('{id}/upload_answer_file', 'quizController@uploadAnswerFile')->name('upload_answer_file');
    Route::get('{id}/question_file', 'quizController@questionFile')->name('question_file');
    Route::get('{id}/answer_file', 'quizController@answerFile')->name('answer_file');
    Route::delete('{id}/delete_question_file', 'quizController@deleteQuestionFile')->name('delete_question_file');
    Route::delete('{id}/delete_answer_file', 'quizController@deleteAnswerFile')->name('delete_answer_file');
    Route::delete('{id}/delete_course_image', 'quizController@deleteCourseImage')->name('delete_course_image');
    Route::delete('{id}/delete_promo', 'quizController@deletePromo')->name('delete_promo');
    Route::patch('{id}/update', 'quizController@update')->name('update');
    Route::patch('{id}/update_answers', 'quizController@updateAnswers')->name('update_answers');
});

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => "admin/students", 'as' => "admin.students.", 'namespace' => 'students'], function () {
    Route::controller('allStudents', 'studentController', [
        'getAllStudents'  => 'datatables.data',
        'getIndex' => 'datatables',
    ]);
    Route::get('getAllStudents', 'studentController@getAllStudents');
});
    
//Route::get('{module}/{state}/js_validator.js', ['as' => 'js_validator', function ($module, $state) {
//        app('debugbar')->disable();
//        $module = app('moduleResolver')->make($module);
//        $module->validator()->setState($state);
//        $module->validator()->makeValidations($state);
//        return $module->validator()->jsValidator();
//    }
//]);
//Route::get('admin/one_on_one/create', 'OneOnOneController@create');
Route::auth();
Route::get('/home', 'HomeController@index');
