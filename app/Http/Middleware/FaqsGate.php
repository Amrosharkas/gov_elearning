<?php

namespace App\Http\Middleware;

use Closure;
use LoggedAccount;

class FaqsGate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!LoggedAccount::get()->isCRM() && !LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
        return $next($request);
    }

}
