<?php

namespace App\Http\Middleware\Elearning;

use Closure;
use Route;

class ModuleMaterial {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        app('debugbar')->disable();
        $path = '';
        for ($i = 4; $i <= count($request->segments()); $i++) {
            $path .= '/' . $request->segment($i);
        }
        $fullPath = storage_path('uploads/elearning/module/' . $path);
        if (!file_exists($fullPath)) {
            abort(404);
        }
        return response(file_get_contents($fullPath));
    }

}
