<?php

namespace App\Http\Middleware;

use Closure;
use LoggedAccount;
use Route;

class AdminGate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!LoggedAccount::get()->isSuperAdmin()) {
            abort(404);
        }
//        $parts = explode('.', Route::currentRouteName());
//        $ability = $parts[1] . '.' . $parts[2];
//        if (Auth::user()->cannot($ability)) {
//            abort(404);
//        }

        return $next($request);
    }

}
