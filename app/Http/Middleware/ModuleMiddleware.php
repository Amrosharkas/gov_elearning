<?php

namespace App\Http\Middleware;

use Closure;
use App\Support\ModuleResolver;

class ModuleMiddleware {

    public function handle($request, Closure $next) {
        $moduleName = $request->segment(2);
        $module = app('moduleResolver')->make($moduleName);
        $repoName = $module->getRepositroyName();
        $requestForm = $module->getFormRequestName();
        app()->instance('currentModule', $module);
        app()->bind('App\\Repositories\\Repository', $repoName);
        app()->bind('Illuminate\\Foundation\\Http\\FormRequest', $requestForm);
        return $next($request);
    }

}
