<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model {

    use Traits\TrimScalarValues;

    protected $fillable = ['name'];

    public static function boot() {
        parent::boot();
        static::deleting(function ($category) {
            $category->faqs()->update(['category_id' => null]);
        });
        static::creating(function ($faqCategory) {
            $faqCategory->stuff_order = static::query()->max('stuff_order') + 1;
        });
    }

    public function faqs() {
        return $this->hasMany('App\Faq', 'category_id');
    }

}
