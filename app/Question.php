<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

	private $questionFile = null;

	public static function boot() {
        parent::boot();
        static::deleting(function ($course) {
            
            $course->deleteQuestionFile();
            
            
            
            
        });
    }
    public function questionFile() {
        if (is_null($this->file)) {
            return null;
        }
        if (is_null($this->questionFile)) {
            $this->questionFile = File::find($this->file);
            return $this->questionFile;
        }
        return $this->questionFile;
    }

    public function deleteQuestionFile() {
        if ($this->questionFile()) {
            $this->questionFile()->delete();
            $this->file = null;
            $this->save();
        }
    }


    public function getAnswers(){
    	return $this->hasMany('App\Answer' ,'question_id');
    }
}
