<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElearningLesson extends Model {

    private $material;

    public function module() {
        return $this->belongsTo('App\ElearningModule', 'module_id');
    }

    public function topics() {
        return $this->hasMany('App\ElearningTopic', 'lesson_id');
    }

    public function material() {
        if (is_null($this->material_id)) {
            return null;
        }
        if (is_null($this->material)) {
            $this->material = File::find($this->material_id);
            return $this->material;
        }
        return $this->material;
    }

    public function deleteMaterial() {
        if ($this->material()) {
            $this->material()->delete();
            $this->material_id = null;
            $this->save();
        }
    }

}
