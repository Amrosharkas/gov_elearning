<?php

namespace App;

class LoggedAccount {

    private $account;

    public function set($account) {
        $this->account = $account;
    }

    public function get($attribute = null) {
        if (is_null($attribute)) {
            return $this->account;
        }
        return $this->account->{$attribute};
    }

}
