<?php

use App\Support\RoleHelper;

// Path related helper functions
function plugin($path) {
    
}

function page_script($path) {
    
}

function page_style($path) {
    
}

function global_script($path) {
    
}

function global_style($path) {
    
}

// Modules related helper functions
function modules() {
    return app('moduleResolver')->moduleNames();
}

function module($moduleName) {
    return app('moduleResolver')->module($moduleName);
}

function module_actions($module) {
    return config("modules.{$module}.actions");
}

function custom_action($action) {
    $actions = config('actions');
    return $actions[$action]();
}

function current_module() {
    return app('currentModule');
}
function jsvalidator_route(){
    return current_module()->validator()->jsValidatorRoute();
}
// Authorization related hlper functions
function roles() {
    return array_keys(config('roles'));
}

function predefined_role($role) {
    return app('roleResolver')->make($role);
}

function role_ability() {
    
}
