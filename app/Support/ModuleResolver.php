<?php

namespace App\Support;

class ModuleResolver {

    private $module;
    private $modules;

    public function __construct() {
        $modules = config('modules');
        foreach ($modules as $name => $module) {
            $nameHelper = new ModuleHelper($name, $module);
            $modules[$name] = $nameHelper->getModuleNames();
        }
        $this->modules = $modules;
    }

    public function all() {
        return $this->modules;
    }

    public function make($module) {
        if ($this->moduleExists($module)) {
            return new Module($module, $this->modules[$module]);
        }
        abort(404);
    }

    public function moduleNames() {
        return array_keys($this->modules);
    }

    public function module($moduleName) {
        return new Module($moduleName, $this->modules[$moduleName]);
    }

    private function moduleExists($module) {
        return isset($this->modules[$module]);
    }

}
