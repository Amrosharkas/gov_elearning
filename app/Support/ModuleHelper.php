<?php

namespace App\Support;

class ModuleHelper {

    private $moduleName;
    private $module;
    private $repoPrefix = '\\App\\Repositories\\';
    private $validatorPrefix = '\\App\\Validators\\';
    private $viewPrefix = '';
    private $formPrefix = '\\App\\Http\\Requests\\';

    public function __construct($moduleName, $module) {
        $this->module = $module;
        $this->moduleName = $moduleName;
        $this->buildRepoName();
        $this->buildViewDirName();
        $this->buildFormRequestName();
        $this->buildActionsList();
        $this->buildValidatorName();
        if (isset($this->module['approvable'])) {
            $this->module['approvable'] = true;
        } else {
            $this->module['approvable'] = false;
        }
    }

    public function buildRepoName() {
        if (isset($this->module['repository'])) {
            $this->module['repository'] = $this->repoPrefix . $this->module['repository'];
            return;
        }
        $this->module['repository'] = $this->repoPrefix . $this->defaultRepoName();
    }

    public function buildActionsList() {
        if (!isset($this->module['actions'])) {
            $this->module['actions'] = $this->defaultActions();
        }
    }

    public function buildFormRequestName() {
        if (isset($this->module['form_request'])) {
            $this->module['form_request'] = $this->formPrefix . $this->module['form_request'];
            return;
        }
        $this->module['form_request'] = $this->formPrefix . $this->defaultFormRequestName();
    }

    public function buildViewDirName() {
        if (isset($this->module['view_dir'])) {
            $this->module['view_dir'] = $this->viewPrefix . $this->module['view_dir'];
            return;
        }
        $this->module['view_dir'] = $this->viewPrefix . $this->defaultViewDirName();
    }

    public function buildValidatorName() {
        if (isset($this->module['validator'])) {
            $this->module['validator'] = $this->validatorPrefix . $this->module['validator'];
            return;
        }
        $this->module['validator'] = $this->validatorPrefix . ucfirst(camel_case($this->moduleName)) . 'Validator';
    }

    public function getModuleNames() {
        return $this->module;
    }

    private function defaultRepoName() {
        return ucfirst(camel_case($this->moduleName)) . 'Repository';
    }

    private function defaultActions() {
        return ['index', 'create', 'edit', 'store', 'update', 'delete', 'delete_multiple'];
    }

    private function defaultFormRequestName() {
        return ucfirst(camel_case($this->moduleName)) . 'FormRequest';
    }

    private function defaultViewDirName() {
        return $this->moduleName;
    }

}
