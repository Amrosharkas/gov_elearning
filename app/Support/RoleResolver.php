<?php

namespace App\Support;

class RoleResolver {

    private $roles;

    public function __construct() {
        $roles = config('roles');
        $this->roles = [];
        foreach ($roles as $name => $role) {
            $this->roles[$name] = $role;
        }
    }

    public function make($role) {
        if (!isset($this->roles[$role])) {
            return false;
        }
        return new RoleHelper($role,$this->roles[$role]);
    }

}
