<?php

namespace App\Support;

class Module {

    private $module;
    private $name;
    private $validator;

    public function __construct($name, $module) {
        $this->module = $module;
        $this->name = $name;
        $validatorName = $this->module['validator'];
        $this->validator = new $validatorName();
    }

    public function getViewDirName() {
        return $this->module['view_dir'];
    }

    public function getRepositroyName() {
        return $this->module['repository'];
    }

    public function getFormRequestName() {
        return $this->module['form_request'];
    }

    public function getActionsList() {
        return $this->module['actions'];
    }

    public function getName() {
        return $this->name;
    }

    public function isApprovable() {
        return $this->module['approvable'];
    }

    public function validator() {
        return $this->validator;
    }

}
