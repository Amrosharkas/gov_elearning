<?php

namespace App\Support;

use Route;

class AbilityHelper {

    public static function getEntitiesWithActions() {
        $routes = Route::getRoutes();
        dd($routes->first());
        $entities = [];
        foreach ($routes as $route) {
            if (!preg_match('/$admin\.(.+\..+)\.?/', $route->getName(), $matches)) {
                continue;
            }
            $abilitity = $matches[1];
            list($entity, $action) = explode('.', $abilitity);
            $entity = ucfirst($entity);
            $action = ucfirst($action);
            if (!isset($entities[$entity])) {
                $entities[$entity] = [];
                $entities[$entity][] = $action;
                continue;
            }
            $entities[$entity][] = $action;
        }
        return $entities;
    }

}
