<?php

namespace App\Support;

class UploadUtility {

    public static function upload($uploadedFile) {
        
    }

    public static function createUniqueFilename($filename, $extension, $dirPath) {
        $filePath = $dirPath . $filename . '.' . $extension;

        if (File::exists($filePath)) {
            // Generate token for image
            $fileToken = substr(sha1(mt_rand()), 0, 5);
            return $filename . '-' . $fileToken . '.' . $extension;
        }

        return $filename . '.' . $extension;
    }

}
