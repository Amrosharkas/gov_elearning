<?php

namespace App\Support;

class RoleHelper {

    private $roleName;
    private $role;

    public function __construct($roleName, $role) {
        $this->roleName = $roleName;
        $this->role = $role;
    }

    public function hasAbility($ability) {
        list($module, $action) = explode('.', $ability);
        if (!$this->knowsModule($module)) {
            return false;
        }
        return in_array($action, $this->role[$module]);
    }

    public function knowsModule($module) {
        return isset($this->role[$module]);
    }

}
