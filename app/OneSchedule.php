<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneSchedule extends Model {

    use Traits\TrimScalarValues;

    public static function boot() {
        parent::boot();
        static::deleting(function ($schedule) {
            $schedule->daytimes()->delete();
        });
    }

    public function course() {
        return $this->belongsTo('App\OneCourse', 'course_id');
    }

    public function teacher() {
        return $this->belongsTo('App\Account', 'teacher_id');
    }

    public function student() {
        return $this->belongsTo('App\Account', 'student_id');
    }

    public function daytimes() {
        return $this->hasMany('App\OneScheduleDaytime', 'schedule_id');
    }

}
