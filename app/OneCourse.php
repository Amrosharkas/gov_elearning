<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneCourse extends Model {

    use Traits\TrimScalarValues;

    private $sectionImage = null;
    private $courseImage = null;
    private $promo = null;

    public static function boot() {
        parent::boot();
        static::deleting(function ($course) {
            $course->deleteSectionImage();
            $course->deleteCourseImage();
            $course->deletePromo();
            $course->levels()->delete();
            $course->pricings()->delete();
        });
    }

    public function levels() {
        return $this->hasMany('App\OneLevel', 'course_id');
    }

//    public function objectives() {
//        return $this->morphMany('App\LearningObjective', 'stuff');
//    }

    public function pricings() {
        return $this->hasMany('App\OneSessionPricing', 'course_id');
    }

    public function section() {
        return $this->belongsTo('App\Section', 'section_id');
    }

    public function schedules() {
        return $this->hasMany('App\OneSchedule', 'course_id');
    }

    public function sectionImage() {
        if (is_null($this->section_image_id)) {
            return null;
        }
        if (is_null($this->sectionImage)) {
            $this->sectionImage = File::find($this->section_image_id);
            return $this->sectionImage;
        }
        return $this->sectionImage;
    }

    public function courseImage() {
        if (is_null($this->course_image_id)) {
            return null;
        }
        if (is_null($this->courseImage)) {
            $this->courseImage = File::find($this->course_image_id);
            return $this->courseImage;
        }
        return $this->courseImage;
    }

    public function promo() {
        if (is_null($this->promo_id)) {
            return null;
        }
        if (is_null($this->promo)) {
            $this->promo = File::find($this->promo_id);
            return $this->promo;
        }
        return $this->promo;
    }

    public function deleteCourseImage() {
        if ($this->courseImage()) {
            $this->courseImage()->delete();
            $this->course_image_id = null;
            $this->save();
        }
    }

    public function deleteSectionImage() {
        if ($this->sectionImage()) {
            $this->sectionImage()->delete();
            $this->section_image_id = null;
            $this->save();
        }
    }

    public function deletePromo() {
        if ($this->promo()) {
            $this->promo()->delete();
            $this->promo_id = null;
            $this->save();
        }
    }

    public function shifts() {
        return $this->belongsToMany('App\Shift', 'one_shifts', 'course_id', 'shift_id');
    }

}
