<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quiz extends Model
{
    protected $table = "quizes";

    public function getQuestions(){
    	return $this->hasMany('App\Question' ,'quiz_id');
    }
}
