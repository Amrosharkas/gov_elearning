<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlendedCourse extends Model {

    use Traits\TrimScalarValues;

    private $sectionImage = null;
    private $courseImage = null;
    private $promo = null;
    private $courseMaterial = null;

    public static function boot() {
        parent::boot();
        static::deleting(function ($course) {
            $course->deleteCourseImage();
            $course->deleteSectionImage();
            $course->deletePromo();
            if ($course->courseImage()) {
                $course->courseImage()->delete();
            }
            if ($course->promo()) {
                $course->promo()->delete();
            }
            $course->templates()->update(['course_id' => null]);
        });
    }

    public function section() {
        return $this->belongsTo('App\Section', 'section_id');
    }

    public function templates() {
        return $this->hasMany('App\BlendedTemplate', 'course_id');
    }

    public function sectionImage() {
        if (is_null($this->section_image_id)) {
            return null;
        }
        if (is_null($this->sectionImage)) {
            $this->sectionImage = File::find($this->section_image_id);
            return $this->sectionImage;
        }
        return $this->sectionImage;
    }

    public function courseImage() {
        if (is_null($this->course_image_id)) {
            return null;
        }
        if (is_null($this->courseImage)) {
            $this->courseImage = File::find($this->course_image_id);
            return $this->courseImage;
        }
        return $this->courseImage;
    }
    public function courseMaterial() {
        if (is_null($this->course_material_id)) {
            return null;
        }
        if (is_null($this->courseMaterial)) {
            $this->courseMaterial = File::find($this->course_material_id);
            return $this->courseMaterial;
        }
        return $this->courseMaterial;
    }


    public function promo() {
        if (is_null($this->promo_id)) {
            return null;
        }
        if (is_null($this->promo)) {
            $this->promo = File::find($this->promo_id);
            return $this->promo;
        }
        return $this->promo;
    }

    public function deleteSectionImage() {
        if ($this->sectionImage()) {
            $this->sectionImage()->delete();
            $this->section_image_id = null;
            $this->save();
        }
    }

    public function deleteCourseImage() {
        if ($this->courseImage()) {
            $this->courseImage()->delete();
            $this->course_image_id = null;
            $this->save();
        }
    }
    public function deleteCourseMaterial() {
        if ($this->courseMaterial()) {
            $this->courseMaterial()->delete();
            $this->course_material_id = null;
            $this->save();
        }
    }

    public function deletePromo() {
        if ($this->promo()) {
            $this->promo()->delete();
            $this->promo_id = null;
            $this->save();
        }
    }

}
