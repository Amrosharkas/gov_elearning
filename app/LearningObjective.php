<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningObjective extends Model {

    use Traits\TrimScalarValues;

    public function stuff() {
        return $this->morphTo();
    }

}
