<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElearningCourse extends Model {

    private $sectionImage = null;
    private $courseImage = null;
    private $promo = null;

    public function modules() {
        return $this->hasMany('App\ElearningModule', 'course_id');
    }

    public function lessons() {
        return $this->hasMany('App\ElearningLesson', 'course_id');
    }

    public function topics() {
        return $this->hasMany('App\ElearningTopic', 'course_id');
    }

    public function sectionImage() {
        if (is_null($this->section_image_id)) {
            return null;
        }
        if (is_null($this->sectionImage)) {
            $this->sectionImage = File::find($this->section_image_id);
            return $this->sectionImage;
        }
        return $this->sectionImage;
    }

    public function courseImage() {
        if (is_null($this->course_image_id)) {
            return null;
        }
        if (is_null($this->courseImage)) {
            $this->courseImage = File::find($this->course_image_id);
            return $this->courseImage;
        }
        return $this->courseImage;
    }

    public function promo() {
        if (is_null($this->promo_id)) {
            return null;
        }
        if (is_null($this->promo)) {
            $this->promo = File::find($this->promo_id);
            return $this->promo;
        }
        return $this->promo;
    }

    public function deleteCourseImage() {
        if ($this->courseImage()) {
            $this->courseImage()->delete();
            $this->course_image_id = null;
            $this->save();
        }
    }

    public function deleteSectionImage() {
        if ($this->sectionImage()) {
            $this->sectionImage()->delete();
            $this->section_image_id = null;
            $this->save();
        }
    }

    public function deletePromo() {
        if ($this->promo()) {
            $this->promo()->delete();
            $this->promo_id = null;
            $this->save();
        }
    }

}
