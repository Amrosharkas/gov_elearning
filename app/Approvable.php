<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Approvable extends Creatable {

    public static function boot() {
        parent::boot();
        static::creating(function ($approvable) {
            if (Auth::user()->isSuperAdmin()) {
                $approvable->approved_by = Auth::user()->id;
                $approvable->approved = true;
            }
        });
        static::updating(function ($approvable) {
            if (!Auth::user()->isSuperAdmin()) {
                $approvable->approved_by = null;
                $approvable->approved = false;
            }
        });
    }

    public function approvedBy() {
        return $this->belongsTo('App\User', 'approved_by');
    }

}
