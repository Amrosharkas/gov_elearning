<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Shift extends Model {

    use Traits\TrimScalarValues;

    protected $fillable = ['name', 'start_time', 'end_time'];

    public static function boot() {
        parent::boot();
        static::deleting(function ($shift) {
            $shift->oneCourses()->detach();
            $shift->blendedSchedules()->update(['shift_id' => null]);
            $shift->oneSchedules()->update(['shift_id' => null]);
        });
    }

    public function oneCourses() {
        return $this->belongsToMany('App\OneShift', 'one_shifts');
    }

    public function blendedSchedules() {
        return $this->hasMany('App\BlendedSchedule');
    }

    public function oneSchedules() {
        return $this->hasMany('App\OneSchedule', 'shift_id');
    }

    // Attribute Accessors 
    public function getStartTimeAttribute($time) {

        return $time ? $this->getTimeWithoutSeconds($time) : null;
    }

    public function getEndTimeAttribute($time) {
        return $time ? $this->getTimeWithoutSeconds($time) : null;
    }

    // Helper Functions
    private function getTimeWithoutSeconds($time) {
        list($hour, $minute) = explode(':', $time);
        return Carbon::createFromTime($hour, $minute)->format('H:i');
    }

}
