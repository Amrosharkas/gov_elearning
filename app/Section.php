<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model {

    use Traits\TrimScalarValues;

    public function oneCourses() {
        return $this->hasMany('App\OneCourse', 'section_id');
    }

    public function blendedCourses() {
        return $this->hasMany('App\BlendedCourse', 'section_id');
    }

}
