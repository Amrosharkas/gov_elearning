<?php

namespace App\Repositories;

use App\FaqCategory;
use stdClass;
use App\Faq;

class FaqRepository extends OrderableRepository implements FaqRepositoryInterface {

    protected $model = '\App\\Faq';

    public function __construct() {
        
    }

    public function getFormData() {
        $formData = new stdClass();
        $formData->categories = FaqCategory::all();
        return $formData;
    }

    public function hasRelatedData() {
        return false;
    }

    public function haveRelatedData() {
        return false;
    }

    public function report($id) {
        $faq = Faq::find($id);
        $faq->reported = true;
        $faq->save();
    }

}
