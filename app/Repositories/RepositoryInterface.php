<?php

namespace App\Repositories;

interface RepositoryInterface {

    public function find($id);

    public function findBy($attribute, $value);

    public function findOneBy($attribute, $value);

    public function getAll();

    public function create($data);

    public function update($data, $id);

    public function getFormData();

}
