<?php

namespace App\Repositories;

use stdClass;
use DB;

class RoleRepository extends Repository implements BackendUsersRepositoryInterface {

    protected $model = '\\App\\Role';

    public function create($data) {
        $model = $this->model;
        $role = new $model();
        return $this->save($role, $data['name'], $data['abilities']);
    }

    public function update($data, $id) {
        $model = $this->model;
        DB::table('role_abilities')->where('role_id', '=', $id)->delete();
        $role = $model::find($id);
        return $this->save($role, $data['name'], $data['abilities']);
    }

    public function getFormData() {
        $formData = new stdClass();
        $formData->modules = module();
        return $formData;
    }

    private function save($role, $roleName, $abilityNames) {
        $role->name = $roleName;
        $role->save();
        $abilities = [];
        foreach ($abilityNames as $abilityName) {
            $abilities[] = ['name' => $abilityName, 'role_id' => $role->id];
        }
        DB::table('role_abilities')->insert($abilities);
        return ['status' => 1];
    }

}
