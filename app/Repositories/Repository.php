<?php

namespace App\Repositories;

use stdClass;

class Repository implements RepositoryInterface {

    protected $model;
    protected $messages = [
        'create' => 'Created successfully',
        'update' => 'Updated successfully',
        'delete' => 'Item successfully deleted',
        'delete-multiple' => 'Items successfully deleted',
        'delete-error' => 'Please unlink all related data first'
    ];

    public function find($id) {
        $model = $this->model;
        return $model::find($id);
    }

    public function findOrFail($id) {
        $model = $this->model;
        return $model::findOrFail($id);
    }

    public function findBy($attribute, $value) {
        $model = $this->model();
        return $model::where($attribute, $value)->get();
    }

    public function findOneBy($attribute, $value) {
        $model = $this->model();
        return $model::where($attribute, $value)->first();
    }

    public function getAll() {
        $model = $this->model;
        return $model::all();;
    }

    public function create($data) {
        $model = $this->model;
        $model::create($data);
        return ['status' => 1, 'message' => $this->messages['create']];
    }

    public function update($data, $id) {
        $model = $this->model;
        $model::find($id)->update($data);
        return ['status' => 1, 'message' => $this->messages['update']];
    }

    public function delete($id) {
        $model = $this->model;
        $item = $model::findOrFail($id);
        if ($this->hasRelatedData($item)) {
            return ['status' => -1, 'message' => $this->messages['delete-error']];
        }
        $this->deleteItemWithSubData($item);
        return ['status' => 1, $this->messages['delete']];
    }

    public function deleteMultiple($idList) {
        $model = $this->model;
        $query = $model::query()->whereIn('id', $idList);
        if ($query->count() == 0) {
            abort(404);
        }
        if ($this->haveRelatedData($idList)) {
            return ['status' => -1, 'message' => $this->messages['delete-error']];
        }
        $this->deleteItemsWithSubData($query);
        return ['status' => 1, $this->messages['delete-multiple']];
    }

    public function getFormData() {
        return null;
    }

    protected function hasRelatedData() {
        return true;
    }

    protected function haveRelatedData() {
        return true;
    }

    protected function deleteItemWithSubData($item) {
        $item->delete();
    }

    protected function deleteItemsWithSubData($query) {
        $query->delete();
    }

}
