<?php

namespace App\Repositories;

use stdClass;
use App\User;
use App\Account;

class BackendUsersRepository extends Repository implements BackendUsersRepositoryInterface {

    protected $model = '\\App\\Account';

    public function getAll() {
        $superAdmins = User::where('type', '=', 'superAdmin')->get();
        $superAdminIds = $superAdmins->pluck('id');
        return Account::whereNotIn('user_id', $superAdminIds)->with('user')->get();
    }

    public function find($id) {
        return Account::with('user')->find($id);
    }

    public function findOrFail($id) {
        return Account::with('user')->findOrFail($id);
    }

    public function create($data) {
        $user = new User();
        $account = new Account();
        if (env('APP_ENV') == 'local') {
            $user->password_dec = '123456';
            $user->password = bcrypt($user->password_dec);
        } else {
            $user->password_dec = str_random(6);
            $user->password = bcrypt($user->password_dec);
        }
        $this->save($data, $user, $account);
        return ['status' => 1, 'message' => 'Backend user created successfully'];
    }

    public function update($data, $id) {
        $account = Account::findOrFail($id);
        $this->save($data, $account->user, $account);
        return ['status' => 1, 'message' => 'Backend user updated successfully'];
    }

    public function getFormData() {
        $formData = new stdClass();
        $formData->userTypes = User::getBackendUserTypes();
        return $formData;
    }

    protected function hasRelatedData() {
        return false;
    }

    protected function haveRelatedData() {
        return false;
    }

    private function save($data, $user, $account) {
        $user->email = $data['email'];
        $user->type = $data['type'];
        $account->name = $data['name'];
        $user->save();
        $user->accounts()->save($account);
    }

}
