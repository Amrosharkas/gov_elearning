<?php

namespace App\Repositories;

class ApprovableRepository extends CreatableRepository implements ApprovableRepositoryInterface {

    protected $model = '\App\\Approvable';

    public function __construct() {
        $this->messages['approved'] = 'Entry approved successfully';
    }

    public function approve($id, $approvedById) {
        $model = $this->model;
        $model::where('id', '=', $id)->update(['approved_by' => $approvedById]);
        return ['status' => 1, 'message' => $this->messages['approved']];
    }

}
