<?php

namespace App\Repositories;

use Carbon\Carbon;

class ShiftRepository extends Repository implements ShiftRepositoryInterface {

    protected $model = '\\App\\Shift';

    public function __construct() {
        $this->messages['invalid-range'] = 'End time is before start time';
        $this->messages['interference'] = 'This shift interferes with other shifts';
    }

    public function create($data) {
//        if (!$this->isValidTimeRange($data['start_time'], $data['end_time'])) {
//            return ['status' => -1, 'message' => $this->messages['invalid-range']];
//        }
//        if ($this->causesInterference($data['start_time'], $data['end_time'])) {
//            return ['status' => -1, 'message' => $this->messages['interference']];
//        }
        $model = $this->model;
        $model::create($data);
        return ['status' => 1, 'message' => 'Shift created successfully'];
    }

    public function update($data, $id) {
//        if (!$this->isValidTimeRange($data['start_time'], $data['end_time'])) {
//            return ['status' => -1, 'message' => $this->messages['invalid-range']];
//        }
//        if ($this->causesInterference($data['start_time'], $data['end_time'])) {
//            return ['status' => -1, 'message' => $this->messages['interference']];
//        }
        $model = $this->model;
        $model::find($id)->update($data);
        return ['status' => 1, 'message' => 'Shift updated successfully'];
    }

    //Helper functions
//    private function isValidTimeRange($start, $end) {
//        list($hoursBefore, $minutesBefore) = explode(':', $start);
//        list($hours, $minutes) = explode(':', $end);
//        if ($hours < $hoursBefore || ($hours == $hoursBefore && $minutes < $minutesBefore)) {
//            return false;
//        }
//        return true;
//    }

    protected function hasRelatedData() {
        return false;
    }

    protected function haveRelatedData() {
        return false;
    }

    private function causesInterference($start, $end) {
        $model = $this->model;
        list($startHour, $startMinute) = explode(':', $start);
        list($endHour, $endMinute) = explode(':', $end);
        $startTime = Carbon::createFromTime($startHour, $startMinute);
        $endTime = Carbon::createFromTime($endHour, $endMinute);
        $shifts = $model::all();
        foreach ($shifts as $shift) {
            list($sHour, $sMinute) = explode(':', $shift->start_time);
            list($eHour, $eMinute) = explode(':', $shift->end_time);
            $sTime = Carbon::createFromTime($sHour, $sMinute);
            $eTime = Carbon::createFromTime($eHour, $eMinute);
            if (($eTime > $sTime && $startTime >= $sTime && $startTime < $eTime) ||
                    ($eTime > $sTime && $endTime > $sTime && $endTime <= $eTime) ||
                    ($eTime < $sTime && ($startTime >= $sTime || $startTime < $eTime)) ||
                    ($eTime < $sTime && ($endTime > $sTime || $endTime <= $eTime)) ||
                    ($eTime > $sTime && $endTime > $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime < $sTime && $endTime < $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime > $sTime && $endTime < $startTime && (($sTime >= $startTime && $eTime > $startTime) || ($sTime < $endTime && $eTime <= $endTime)))) {
                return true;
            }
        }
        return false;
    }

}
