<?php

namespace App\Repositories;

use stdClass;

class OrderableRepository extends Repository implements OrderableRepositoryInterface {

    protected $model;

    public function __construct() {
        $this->messages['reorder-error'] = 'Something went wrong';
        $this->messages['reorder-done'] = 'Records reordered successfully';
    }

    public function getAll() {
        $model = $this->model;
        return $model::query()->orderBy('stuff_order')->get();;
    }

    public function reOrder($newSequence) {
        $model = $this->model;
        $allCount = $model::query()->count();
        $foundCount = $model::whereIn('id', $newSequence)->count();
        $newSequenceCount = count($newSequence);
        if ($newSequenceCount != $allCount || $newSequenceCount != $foundCount) {
            return ['status' => -1, 'message' => $this->messages['reorder-error']];
        }
        $order = 1;
        $response = [];
        foreach ($newSequence as $id) {
            $response[] = $model::where('id', '=', $id)->update(['stuff_order' => $order++]);
        }
        return ['status' => 1, 'message' => $this->messages['reorder-done']];
    }

}
