<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Traits\TrimScalarValues;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isFamily() {
        return $this->type == 'family';
    }

    public function isSingular() {
        return !$this->isFamily();
    }

    public function accounts() {
        return $this->hasMany('App\Account');
    }

    public static function getBackendUserTypes() {
        return [
            'TCM', 'Coordinator', 'CRM', 'QM', 'Content Creator', 'HR'
        ];
    }

}
