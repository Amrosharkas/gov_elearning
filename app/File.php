<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    use Traits\TrimScalarValues;

    public static function boot() {
        parent::boot();
        static::deleting(function ($file) {
            $file->removeFile();
        });
    }

    public function removeFile() {
        if (file_exists($this->path)) {
            unlink($this->path);
        }
    }

}
