<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneSessionPricing extends Model {

    use Traits\TrimScalarValues;

    protected $guarded = ['id', 'course_id'];

    public function course() {
        return $this->belongsTo('App\OneCourse', 'course_id');
    }

}
