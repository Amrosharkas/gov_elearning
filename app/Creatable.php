<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Creatable extends Model {

    public static function boot() {
        parent::boot();
        static::creating(function ($creatable) {
            if (Auth::check()) {
                $creatable->created_by = Auth::user()->id;
                $creatable->updated_by = Auth::user()->id;
            }
        });
        static::updating(function ($creatable) {
            $creatable->updated_by = Auth::user()->id;
        });
    }

    public function creator() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updator() {
        return $this->belongsTo('App\User', 'updated_by');
    }

}
