<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    use Traits\TrimScalarValues;

    public function user() {
        return $this->belongsToMany('App\User', 'user_roles');
    }

    public function abilities() {
        return $this->hasMany('App\RoleAbility');
    }

    public function entitiesWithActions() {
        $abilities = $this->abilities;
        $entities = [];
        foreach ($abilities as $ability) {
            list($entity, $action) = explode('.', $ability->name);
            $entity = ucfirst($entity);
            $action = ucfirst($action);
            if (!isset($entities[$entity])) {
                $entities[$entity] = [];
                $entities[$entity][] = $action;
                continue;
            }
            $entities[$entity][] = $action;
        }
        return $entities;
    }

    public function getAbilityNames() {
        $abilityNames = [];
        foreach ($this->abilities as $ability) {
            $abilityNames[] = $ability->name;
        }
        return $abilityNames;
    }

}
