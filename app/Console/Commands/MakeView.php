<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeView extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:view {dirName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dirPath = resource_path('views/' . $this->argument('dirName'));
        if (is_dir($dirPath)) {
            $this->error('Error: View already exists');
            exit;
        }
        mkdir($dirPath);
        $tabTitle = $this->ask('Tab/Windows Title: ');
        $pageTitle = $this->ask('Page title: ', $tabTitle);
        $formRequest = $this->ask('Form Request: ');
        $routeNamePrefix = $this->ask('Route Name Prefix: ');
        $templateDirPath = storage_path('make-templates/view/');
        $baseTemplateFileContent = file_get_contents($templateDirPath . 'base.template.php');
        $search = ['%%%%FormRequest%%%%', '%%%%tabTitle%%%%', '%%%%pageTitle%%%%', '%%%%routeNamePrefix%%%%'];
        $replace = [$formRequest, $tabTitle, $pageTitle, $routeNamePrefix];
        $baseFileContent = str_replace($search, $replace, $baseTemplateFileContent);
        file_put_contents($dirPath . '/base.blade.php', $baseFileContent);
        $listTemplateFileContent = file_get_contents($templateDirPath . 'list.template.php');
        $listFileContent = str_replace($search, $replace, $listTemplateFileContent);
        file_put_contents($dirPath . '/list.blade.php', $listFileContent);
        $formTemplateFileContent = file_get_contents($templateDirPath. 'form.template.php');
        $formFileContent = str_replace($search, $replace, $formTemplateFileContent);
        file_put_contents($dirPath . '/form.blade.php',$formFileContent);
        $this->info('Done .. :)');
    }

}
