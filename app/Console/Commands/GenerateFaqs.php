<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserQuestion;
use Faker\Factory;

class GenerateFaqs extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:faqs {count=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $faker = Factory::create();
        $count = $this->argument('count');
        for ($i = 1; $i <= $count; $i++) {
            $faq = new UserQuestion();
            $faq->question = $faker->text;
            $faq->save();
        }
    }

}
