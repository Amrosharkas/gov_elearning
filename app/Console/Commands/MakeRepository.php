<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeRepository extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name} {--extends=Repository}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $name = ucfirst($this->argument('name'));
        $extends = $this->option('extends');
        $model = $this->ask('Model Name:', $name);
        $repo = $this->ask('Repository Name:', $name . 'Repository');
        $repoInterface = $this->ask('Repository Interface Name:', $repo . 'Interface');
        $search = ['repo', 'repoInterface', '%model%', 'base'];
        $replace = [$repo, $repoInterface, $model, $extends];
        $templatesDir = storage_path('make-templates/repo');
        $repoTemplateContent = file_get_contents($templatesDir . '/repo.template.php');
        $repoInterfaceTemplateContent = file_get_contents($templatesDir . '/repoInterface.template.php');
        $repoDir = app_path('repositories');
        $repoContent = str_replace($search, $replace, $repoTemplateContent);
        $repoInterfaceContent = str_replace($search, $replace, $repoInterfaceTemplateContent);
        file_put_contents($repoDir .'\\'. $repo . '.php', $repoContent);
        file_put_contents($repoDir .'\\'. $repoInterface . '.php', $repoInterfaceContent);
        $this->info('Done :)');
    }

}
