<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Support\ModuleResolver;
use App\Support\RoleResolver;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        if ($this->app->environment() == 'local') {
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
        }
//        $this->app->bind('App\Repositories\RepositoryInterface', 'App\Repositories\Repository');
//        $moduleResolver = new ModuleResolver();
//        $this->app->instance('moduleResolver', $moduleResolver);
//        $roleResolver = new RoleResolver();
//        $this->app->instance('roleResolver', $roleResolver);
    }

}
