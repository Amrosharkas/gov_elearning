<?php

namespace App\Validators;

abstract class BaseValidator {

    protected $messages = [];
    protected $rules = [];
    protected $attributes = [];
    protected $state = 0;

    public function makeValidations() {
        $this->makeRules();
        $this->makeMessages();
        $this->makeAttributes();
    }

    public function inValid($data) {
        return false;
    }

//    abstract public function buildValidations($item = null);

    public function getRules() {
        return $this->rules;
    }

    public function getMessages() {
        return $this->messages;
    }

    public function getAttributes() {
        return $this->attributes;
    }

}
