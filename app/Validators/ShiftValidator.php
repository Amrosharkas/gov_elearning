<?php

namespace App\Validators;

use App\Repositories\ShiftRepository;
use Carbon\Carbon;

class ShiftValidator extends BaseValidator {


    public function inValid($data) {
        $start = $data['start_time'];
        $end = $data['end_time'];
        list($startHour, $startMinute) = explode(':', $start);
        list($endHour, $endMinute) = explode(':', $end);
        $startTime = Carbon::createFromTime($startHour, $startMinute);
        $endTime = Carbon::createFromTime($endHour, $endMinute);
        $repo = new ShiftRepository();
        $shifts = $repo->getAll();
        foreach ($shifts as $shift) {
            list($sHour, $sMinute) = explode(':', $shift->start_time);
            list($eHour, $eMinute) = explode(':', $shift->end_time);
            $sTime = Carbon::createFromTime($sHour, $sMinute);
            $eTime = Carbon::createFromTime($eHour, $eMinute);
            if (($eTime > $sTime && $startTime >= $sTime && $startTime < $eTime) ||
                    ($eTime > $sTime && $endTime > $sTime && $endTime <= $eTime) ||
                    ($eTime < $sTime && ($startTime >= $sTime || $startTime < $eTime)) ||
                    ($eTime < $sTime && ($endTime > $sTime || $endTime <= $eTime)) ||
                    ($eTime > $sTime && $endTime > $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime < $sTime && $endTime < $startTime && $startTime <= $sTime && $endTime >= $eTime) ||
                    ($eTime > $sTime && $endTime < $startTime && (($sTime >= $startTime && $eTime > $startTime) || ($sTime < $endTime && $eTime <= $endTime)))) {
                return ['status' => -1, 'message' => 'This shift interferes with other shifts'];
            }
        }
        return false;
    }

}
