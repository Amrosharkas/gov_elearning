<?php

namespace App\Validators;

use JsValidator;

class FaqValidator extends BaseValidator {

    public function makeRules() {
        $this->rules = [
            'question' => 'required',
            'published' => 'boolean',
            'category_id' => 'required_with:published'
        ];
        if ($this->state) {
            $this->rules['answer'] = 'required';
        } else {
            $this->rules['answer'] = 'required_with:published';
        }
    }

    public function makeMessages() {
        $this->messages['category_id.required_with'] = 'You can\'t publish without a category';
        $this->messages['answer.required_with'] = 'You can\'t publish without an answer';
    }

    public function makeAttributes() {
        $this->attributes['category_id'] = 'category';
    }

    public function detectState($faq) {
        if ($faq && $faq->isFirstTimeResponse()) {
            $this->state = 1;
        }
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function jsValidator() {
        return JsValidator::make($this->getRules(), $this->getMessages(), $this->getAttributes());
    }

    public function jsValidatorRoute() {
        return route('js_validator', ['module' => current_module()->getName(), 'state' => $this->state]);
    }

}
