<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElearningModule extends Model {

    private $material;

    public function lessons() {
        return $this->hasMany('App\ElearningLesson', 'module_id');
    }

    public function course() {
        return $this->belongsTo('App\ElearningCourse', 'course_id');
    }

    public function material() {
        if (is_null($this->material_id)) {
            return null;
        }
        if (is_null($this->material)) {
            $this->material = File::find($this->material_id);
            return $this->material;
        }
        return $this->material;
    }

    public function deleteMaterial() {
        if ($this->material()) {
            $this->material()->delete();
            $this->material_id = null;
            $this->save();
        }
    }

}
