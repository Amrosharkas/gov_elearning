<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Account extends Model {

    use SearchableTrait,
        Traits\TrimScalarValues;

    protected $searchable = [
        'columns' => [
            'users.email' => 20,
            'accounts.name' => 20,
        ],
        'joins' => [
            'users' => ['users.id', 'accounts.user_id'],
        ],
    ];

    public static function boot() {
        parent::boot();
        static::deleting(function ($account) {
            $account->user()->delete();
        });
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function isSuperAdmin() {
        return $this->type == 'superAdmin';
    }

    public function isCRM() {
        return $this->type == 'CRM';
    }

    public function isFrontEnd() {
        return $this->isFamily() && $this->isStudent();
    }

    public function isStudent() {
        return $this->type == 'student';
    }

    public function isBackend() {
        return $this->type == 'backend';
    }

    // Authorization related methods

    public function can($ability, $arguments = []) {
        if ($role = predefined_role($this->type)) {
            return $role->hasAbility($ability);
        }
        $abilities = $this->roles->pluck('abilities')->flatten()->pluck('name');
        return in_array($ability, $abilities->all());
    }

    public function cannot($ability, $arguments = []) {
        return !$this->can($ability, $arguments);
    }

    public function roles() {
        return $this->belongsToMany('App\Role', 'user_roles');
    }


}
