<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class LoggedAccount extends Facade {

    protected static function getFacadeAccessor() {
        return 'App\LoggedAccount';
    }

}
