<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class OneLevel extends Model {

    use Traits\TrimScalarValues;

    private $ig = null;
    private $material = null;

    public static function boot() {
        parent::boot();
        static::deleting(function ($level) {
            $level->deleteIg();
            $level->deleteMaterial();
        });
    }

    public function course() {
        return $this->belongsTo('App\\OneCourse', 'course_id');
    }

    public function ig() {
        if (is_null($this->ig_id)) {
            return null;
        }
        if (is_null($this->ig)) {
            $this->ig = File::find($this->ig_id);
            return $this->ig;
        }
        return $this->ig;
    }

    public function material() {
        if (is_null($this->material_id)) {
            return null;
        }
        if (is_null($this->material)) {
            $this->material = File::find($this->material_id);
            return $this->material;
        }
        return $this->material;
    }

    public function deleteIg() {
        if ($this->ig()) {
            $this->ig()->delete();
            $this->ig_id = null;
            $this->save();
        }
    }

    public function deleteMaterial() {
        if ($this->material()) {
            $this->material()->delete();
            $this->material_id = null;
            $this->save();
        }
    }

}
