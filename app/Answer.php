<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "question_choices";

    private $answerFile = null;

	public static function boot() {
        parent::boot();
        static::deleting(function ($course) {
            
            $course->deleteAnswerFile();
            
            
            
            
        });
    }
    public function answerFile() {
        if (is_null($this->file)) {
            return null;
        }
        if (is_null($this->answerFile)) {
            $this->answerFile = File::find($this->file);
            return $this->answerFile;
        }
        return $this->answerFile;
    }

    public function deleteAnswerFile() {
        if ($this->answerFile()) {
            $this->answerFile()->delete();
            $this->file = null;
            $this->save();
        }
    }


    

}
