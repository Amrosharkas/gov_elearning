<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAbility extends Model {

    use Traits\TrimScalarValues;

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function setNameAttribute($value) {
        list($segment1, $segment2) = explode('.', $value);
        return lcfirst($segment1) . '.' . lcfirst($segment2);
    }

}
