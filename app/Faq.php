<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Faq extends Model {

    use Traits\TrimScalarValues;

    public static function boot() {
        parent::boot();
        static::updating(function ($faq) {
            $faq->created_by = Auth::user()->id;
            $faq->updated_by = Auth::user()->id;
        });
        static::creating(function ($faq) {
            $faq->updated_by = Auth::user()->id;
        });
    }

    public function category() {
        return $this->belongsTo('App\FaqCategory', 'category_id');
    }

    public function original() {
        return $this->belongsTo('App\Faq', 'modification_to');
    }

    public function modification() {
        return $this->hasOne('App\Faq', 'modification_to');
    }

    public function setAnswerAttribute($value) {
        if (!trim($value)) {
            $this->attributes['answer'] = null;
        }
        $this->attributes['answer'] = $value;
    }

//    public function isFirstTimeResponse() {
//        return $this->updated_at == $this->created_at && $this->response;
//    }
}
