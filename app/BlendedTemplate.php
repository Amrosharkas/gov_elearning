<?php

namespace App;

use App\File;
use Illuminate\Database\Eloquent\Model;

class BlendedTemplate extends Model {

    use Traits\TrimScalarValues;

    private $ig = null;

    public static function boot() {
        parent::boot();
        static::deleting(function ($template) {
            $template->deleteIg();
            $template->sessions()->delete();
        });
    }

    public function course() {
        return $this->belongsTo('App\BlendedCourse', 'course_id');
    }

    public function schedules() {
        return $this->hasMany('App\BlendedSchedule', 'template_id');
    }

    public function sessions() {
        return $this->hasMany('App\BlendedSession', 'template_id');
    }

    public function ig() {
        if (is_null($this->ig_id)) {
            return null;
        }
        if (is_null($this->ig)) {
            $this->ig = File::find($this->ig_id);
            return $this->ig;
        }
        return $this->ig;
    }

    public function deleteIg() {
        if ($this->ig()) {
            $this->ig()->delete();
            $this->ig_id = null;
            $this->save();
        }
    }

}
