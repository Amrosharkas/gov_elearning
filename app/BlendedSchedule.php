<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlendedSchedule extends Model {

    use Traits\TrimScalarValues;

    public static function boot() {
        parent::boot();
        static::deleting(function ($schedule) {
            $schedule->sessions()->detach();
        });
    }

    public function template() {
        return $this->belongsTo('App\BlendedTemplate', 'template_id');
    }

    public function course() {
        return $this->belongsTo('App\BlendedCourse', 'course_id');
    }

    public function shift() {
        return $this->belongsTo('App\Shift', 'shift_id');
    }

    public function teacher() {
        return $this->belongsTo('App\Account', 'teacher_id');
    }

    public function sessions() {
        return $this->belongsToMany('App\BlendedSession', 'blended_schedule_sessions', 'schedule_id', 'session_id')
                        ->withPivot('start_time', 'created_at', 'updated_at');
    }

    public function students() {
        return $this->belongsToMany('App\Account', 'blended_schedule_students', 'course_id', 'student_id');
    }

    public function updateTemplate($template) {
        $this->template_id = $template->id;
        $this->save();
        $this->sessions()->detach();
        $this->sessions()->attach($template->sessions);
    }

    public function clearTemplate() {
        $this->template_id = null;
        $this->save();
        $this->sessions()->detach();
    }

    public function clearCourse() {
        $this->course_id = null;
        $this->template_id = null;
        $this->save();
        $this->sessions()->detach();
    }

}
