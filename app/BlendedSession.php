<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlendedSession extends Model {

    use Traits\TrimScalarValues;

    private $material = null;
    private $content = null;
    protected $fillable = ['title', 'duration'];

    public static function boot() {
        parent::boot();
        static::deleting(function ($session) {
            if ($session->content()) {
                $session->content()->delete();
            }
            if ($session->material()) {
                $session->material()->delete();
            }
        });
    }

    public function template() {
        return $this->belongsTo('App\BlendedTemplate', 'template_id');
    }

    public function material() {
        if (is_null($this->material_id)) {
            return null;
        }
        if (is_null($this->material)) {
            $this->material = File::find($this->material_id);
            return $this->material;
        }
        return $this->material;
    }

    public function content() {
        if (is_null($this->content_id)) {
            return null;
        }
        if (is_null($this->content)) {
            $this->content = File::find($this->content_id);
            return $this->content;
        }
        return $this->content;
    }

    public function deleteContent() {
        if ($this->content()) {
            $this->content()->delete();
            $this->content_id = null;
            $this->save();
        }
    }

    public function deleteMaterial() {
        if ($this->material()) {
            $this->material()->delete();
            $this->material_id = null;
            $this->save();
        }
    }

}
