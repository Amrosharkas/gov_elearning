<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderable extends Creatable {

    use Traits\TrimScalarValues;

    public static function boot() {
        parent::boot();
        static::creating(function ($orderable) {
            $orderable->stuff_order = static::query()->max('stuff_order') + 1;
        });
    }

}
