var table = null;
function pjaxPage(url) {
    var link = document.getElementById('pjax-goto-link');
    link.href = url;
    link.click();
}
function scrollToTop() {
    var aTag = $('#page-top');
    $('html,body').animate({scrollTop: aTag.offset().top}, 'slow');
}

function customFileInput($elements) {
    $elements.each(function (index, el) {
        var data = {id: $(el).closest('.level').find('input[name=id]').val()};
        var url = $(el).data('delete');
        $(el).fileinput({
            uploadUrl: $(el).data('action'),
            showPreview: false,
            uploadExtraData: function (previewId, index) {
                return data;
            },
            allowedPreviewTypes: ['image'],
            initialPreviewConfig: [
                {
                    caption: 'desert.jpg',
                    width: '120px',
                    url: url, // server delete action 
                    key: 100,
                    extra: {id: 100}
                }
            ]
        });
        $(el).on('fileclear', function () {
            console.log("fileclear");
        });
        $(el).on('fileloaded', function (event, file, previewId, index, reader) {
            console.log("fileloaded");
        });
        $(el).on('filedeleted', function (event, key) {
            console.log('Key = ' + key);
        });

    });
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on('ready pjax:success', function () {
    $('.timepicker').datetimepicker({
        format: 'hh:ii',
        weekStart: 1,
        todayBtn: 0,
        autoclose: 1,
        todayHighlight: 0,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        showMeridian: true,
    });
    $(".datePicker").datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-caret-left"></i>',
        nextText: '<i class="fa fa-caret-right"></i>'

    });

    if ($('#table-dt').length) {
        table = $('#table-dt').DataTable({
            columnDefs: [
                {
                    targets: 'no-sorting',
                    sortable: false,
                    searchable: false,
                    orderable: false
                },
                {
                    targets: 'table-checkbox-col',
                    width: 10
                }
            ]
        });
    }
    $('.no-sorting').removeClass('sorting sorting_asc sorting_desc');
});
$(document).on('change', '.group-checkable', function () {
    if ($(this).is(':checked')) {
        $('.table-checkbox').prop('checked', true);
    } else {
        $('.table-checkbox').prop('checked', false);
    }
    $('.table-checkbox').each(function () {
        if ($(this).is(':checked')) {
            $(this).parents('tr').addClass("active to-remove");
        } else {
            $(this).parents('tr').removeClass("active to-remove");
        }
    });
    var formId = $(this).closest('form').attr('id');
    var $button = $('[data-form=' + formId + ']');
    if ($('.table-checkbox:checked').length) {
        $button.prop('disabled', false);
    } else {
        $button.prop('disabled', true);
    }
});
$(document).on('change', '.table-checkbox', function () {
    var formId = $(this).closest('form').attr('id');
    var $button = $('[data-form=' + formId + ']');
    if ($(this).is(':checked')) {
        $(this).parents('tr').addClass("active to-remove");
    } else {
        $(this).parents('tr').removeClass("active to-remove");
    }
    if ($('.table-checkbox:checked').length) {
        $button.prop('disabled', false);
    } else {
        $('.group-checkable').prop('checked', false);
        $button.prop('disabled', true);
    }
});
$(document).on('click', '.no-sorting', function () {
    $(this).removeClass('sorting sorting_asc sorting_desc');
});
$(document).on('click', '.delete_single', function () {
    var url = $(this).data('action');
    var $tr = $(this).closest('tr');
    var redirect = $(this).data('redirect');
    swal({title: "Are you sure?", text: "", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", closeOnConfirm: false}, function () {
        $.ajax({
            url: url,
            method: 'delete',
            success: function (data) {
                if (data.status == 1) {
                    if (redirect) {
                        ajaxPage(pageAttributes.indexUrl);
                    } else {
                        table.row($tr).remove().draw();
                    }
                    swal("Deleted!", data.message, "success");
                    return false;
                }
                if (data.status == -1) {
                    swal("Error", data.message, "error");
                    return false;
                }
            }
        });
    });
})
$(document).on('click', '.delete_multiple', function () {
    var $form = $('#' + $(this).data('form'));
    var formData = $form.serialize();
    var url = $form.attr('action');
    swal({title: "Are you sure?", text: "", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", closeOnConfirm: false}, function () {
        $.ajax({
            url: url,
            method: 'delete',
            data: formData,
            success: function (data) {
                table.rows('tr.to-remove').remove().draw();
                $('.group-checkable').prop('checked', false);
                swal("Deleted!", data.message, "success");
                return false;
            },
            error: function (data) {
                $('tr.to-remove').removeClass('to-remove');
            }
        });
    });
})
